/* A simple first C program. */

/* Import definitions of standard library functions. */
#include <stdlib.h>

/* Import definitions of standard input and output functions. */
#include <stdio.h>

/**
 * The main function is called when the program is executed.
 * Its return value is the exit status of the program:
 * 0 means success; anything else means an error occurred.
 *
 * Parameters
 *   argc: number of command line arguments
 *   argv: array of command line arguments as strings, including the
 *         name of the executable as it was invoked.
 */
int main(int argc, char** argv) {
  // Print "Hello, world!" to standard output.
  printf("Hello, world!\n");
  // Exit with success.
  return 0;
}
