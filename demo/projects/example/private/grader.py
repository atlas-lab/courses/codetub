#!/usr/bin/env python3

from codetub.grader.driver import *
from codetub.grader.gradetree import *

def auto(sandbox):
    def boxed(log, command):
        return sandbox.boxed_run(command, logfile = log)
    ok = seq([
        lambda: boxed('compile.out', ['pwd']),
        lambda: boxed('compile.out', ['ls', '-l']),
        lambda: boxed('compile.out', ['make']),
        lambda: boxed('execute.out', ['./hello'])
    ])
    return make('Correctness', points = (2 if all(ok) else 0, 2))
            
def manual(sandbox):
    return make('Style', points = (1, 1))

if __name__ == '__main__':
    main([auto, manual], rootlinks = ['compile.out', 'execute.out'])
