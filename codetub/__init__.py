import os
import pwd

# Name of this tool
NAME = "CodeTub"

# Default Git branch
DEFAULT_BRANCH = "main"

# Home directory of effective user.
EFFECTIVE_HOME = pwd.getpwuid(os.geteuid()).pw_dir

# Get a path in our effective home directory
def homepath(*subpaths):
    return os.path.join(EFFECTIVE_HOME, *subpaths)

# Get a path in our effective dot-directory
def dotpath(*subpaths):
    return homepath('.codetub', *subpaths)

# Get a URL for a repo on the host.
def repourl(*path, user = None, host = None, port = None):
    assert(user)
    assert(host)
    assert(path)
    return 'ssh://{user}@{host}{port}/{path}'.format(
        user = user,
        host = host,
        port = ':{}'.format(port) if port and str(port) != '22' else '',
        path = os.path.join(*path)
    )
