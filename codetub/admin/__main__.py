# CodeTub Admin Tools
# Copyright (c) 2017-2018, Benjamin P. Wood

import codetub
from codetub import admin
from codetub.cli import main
from codetub.admin.local import (Init, Clone)
from codetub.admin.keydrop import KeydropSetup
from codetub.admin.course import CourseSetup
from codetub.util import UMASK_U_RWX
        
TASKS = [
    (['init'], Init,
     'Create configuration and keys for new codetub repository host installation.'),
    (['clone'], Clone,
     "Clone the repository host's admin repo."),
    (['keydrop'], KeydropSetup,
     'Install a keydrop bot.'),
    (['course'], CourseSetup,
     'Create keys, access control, and tools for a new course.'),
]

if __name__ == '__main__':
    main('tubadmin', "{} host administration tasks".format(codetub.NAME), TASKS,
         config = admin.DEFAULT,
         load_config = lambda opts: admin.load_config(opts.config),
         umask = UMASK_U_RWX)
