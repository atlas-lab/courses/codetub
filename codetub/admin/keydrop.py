# CodeTub keydrop admin tasks
# Copyright (c) 2017-2018, Benjamin P. Wood

import codetub
from codetub import admin
from codetub import keydrop
from codetub.cli import *
from codetub.util import UMASK_U_RWX

import os
import pwd
import shutil
import socket

class KeydropSetup(Task):
    def __init__(self, parser):
        super().__init__(parser)
        default_user = codetub.NAME.lower() + '-keydrop'
        parser.add_argument("--coderoot", required = True,
                            help = "CodeTub installation root directory")
        parser.add_argument("--dropuser", help = "keydrop bot user [%(default)s]",
                            default = default_user)
        parser.add_argument("--drophost", help = "keydrop bot hostname [from config]")
        parser.add_argument("--dropport", help = "keydrop bot SSH port [fron config]")
        parser.add_argument("--drophome", help = "keydrop bot home directory")
        parser.add_argument("--dropcc",
                            help = "cc executable on remote")
        parser.add_argument("--dropgit",
                            help = "git executable on remote")
        parser.add_argument("--droppython",
                            help = "python executable on remote")
        parser.add_argument("--force", action='store_true')
        pass

    def execute(self, conf, opts):
        old_umask = os.umask(UMASK_U_RWX)

        if getattr(conf, 'dropuser', None) and not opts.force:
            self.error('Keydrop is already configured ({user}@{host}).'.format(
                user = conf.dropuser,
                host = conf.drophost
            ))
            pass

        drophome = opts.drophome or os.path.join('/home', opts.dropuser)
        conf = conf.shadow(
            coderoot = opts.coderoot,
            dropuser = opts.dropuser,
            drophost = opts.drophost or conf.repohost,
            dropport = opts.dropport or conf.repoport,
            dropbot = os.path.join(opts.coderoot, keydrop.DROP_BOT),
            dropbot_setuid = os.path.join(drophome, keydrop.DROP_BOT_SETUID),
            dropdir  = os.path.join(drophome, keydrop.DROP_DIR),
            dropcc = opts.dropcc,
            dropgit = opts.dropgit,
            droppython = opts.droppython,
        )
        conf.store()

        # sanity: use conf
        opts = None

        #### Configure Gitolite for keydrop ###################################

        # Create the bot key dir if needed.
        bot_keys = os.path.join(conf.repo, admin.PUBKEYS)
        os.makedirs(bot_keys, exist_ok = True)

        # Create a keypair for the keydrop user.
        keyname = keydrop.ACL_KEYNAME
        privkey = os.path.join(bot_keys, keyname)
        pubkey = admin.pubkey(privkey)
        keygen = not os.path.exists(privkey) or not os.path.exists(pubkey)
        if keygen:
            self.run([
                'ssh-keygen', '-q', '-t', 'rsa', '-b', '4096',
                '-N', '', '-C', os.path.basename(privkey),
                '-f', privkey
            ])
            
            # Add the public key to the admin repo.
            self.run(['git', '-C', conf.repo, 'add', pubkey])
            pass

        # Install the private key in the keydrop user account.
        self.run([
            'scp', '-F', conf.ssh_config, '-p',
            privkey, '{user}@{host}:{path}'.format(
                user = conf.dropuser,
                host = conf.drophost,
                path = keydrop.PRIVKEY
            )
        ])
        
        # Delete the private key from the local machine.
        os.remove(privkey)

        # Grant the keydrop user restricted gitolite permissions.
        acl = admin.KEYDROP_CONF_TEMPLATE.format(keydrop_keyname = keyname)
        with open(os.path.join(conf.repo, admin.MAIN_CONF)) as gitolite_conf:
            writeacl = acl not in gitolite_conf.read()
            pass
        if writeacl:
            with open(os.path.join(conf.repo, admin.MAIN_CONF),
                      'a') as gitolite_conf:
                gitolite_conf.write(acl)
                pass
            self.run(['git', '-C', conf.repo, 'add', admin.MAIN_CONF])
            pass

        # Commit and push changes in admin repo.
        if keygen or writeacl:
            self.run([
                'git', '-C', conf.repo, 'commit', '-m',
                'key and permissions for keydrop bot ({user}@{host})'.format(
                    user = conf.dropuser,
                    host = conf.drophost
                )
            ])
            pass
        self.run(['git', '-C', conf.repo, 'push'])

        #### Install keydrop bot on host ###################################

        # Run setup in keydrop user.
        self.run([
            'ssh', '-F', conf.ssh_config,
            '{user}@{host}'.format(
                user = conf.dropuser,
                host = conf.drophost
            ),
            'env', 'PYTHONPATH=' + conf.coderoot,
            (conf.droppython or 'python3'), '-B',
            os.path.join(conf.coderoot, 'codetub/keydrop/install.py'),
            '--coderoot', conf.coderoot,
            '--repouser', conf.repouser,
        ] + [
            '--{var}={val}'.format(var = var, val = val)
            for var, val in [
                    ('cc', conf.dropcc),
                    ('git', conf.dropgit),
                    ('python', conf.droppython),
            ] if val
        ] + (
            # specify host/port only if not loopback to same host
            ['--repohost', conf.repohost, '--repoport', conf.repoport]
            if conf.repohost != conf.drophost else []
        ))
        pass
    pass
