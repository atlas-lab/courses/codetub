import codetub
from codetub.util import Config
import hashlib
import os

REPO = 'gitolite-admin'

CONF = 'conf'
MAIN_CONF = os.path.join(CONF, 'gitolite.conf')
_COURSE_CONF_DIR = 'courses'
COURSE_CONFS = os.path.join(CONF, 'courses')

USER_PREFIX = 'user'
MANAGER_PREFIX = 'manager'
SYSTEM_PREFIX = codetub.NAME.lower()

PUBKEYS = 'keydir'
USER_PUBKEYS = os.path.join(PUBKEYS, '{}s'.format(USER_PREFIX))
MANAGER_PUBKEYS = os.path.join(PUBKEYS, '{}s'.format(MANAGER_PREFIX))
# BOT_PUBKEYS = os.path.join(PUBKEYS, 'bots')

# All usernames in gitolite conf will be classified by a prefix to
# determine basic privilege level.  Currently no access control rules
# grant permissions based on this distinction (and probably none
# should).  Rather, the distinction determines what kinds of keys can
# be uploaded by the keydrop user, as an extra level of protection to
# ensure that the keydrop user can never push a key that grants
# someone access as a more privileged user.  At worst, an unprivileged
# user that has successfully attached the dropbot can upload a key to
# gain access as another unprivileged user.
def unprivileged_name(k):
    return k
def manager_name(k):
    return '{}-{}'.format(MANAGER_PREFIX, k)
def system_name(k):
    return '{}-{}'.format(SYSTEM_PREFIX, k)
LEVEL_NAMES = [unprivileged_name, manager_name, system_name]
assert len(LEVEL_NAMES) == len(set(f('') for f in LEVEL_NAMES))

def pubkey(privkey):
    return privkey + '.pub'

def privkey(pubkey):
    assert pubkey.endswith('.pub')
    return pubkey[:-4]

def pubkey_name(owner = None, sha = None):
    return '{owner}@{sha}.pub'.format(owner = owner, sha = sha)

def raw_pubkey_canonical_name(owner = None, pubkey = None):
    # Hash the key to get a probably unique ID for the key when it is
    # added the key set stored in userid's account on repo host.  If
    # students want to launch collision attacks between their own keys
    # to self-deny auth, more power to them.
    assert(owner)
    assert(pubkey)
    return pubkey_name(owner = owner,
                       sha = hashlib.sha1(pubkey).hexdigest())
    pass
def pubkey_canonical_name(owner = None, pubkey = None):
    # Hash the key to get a probably unique ID for the key when it is
    # added the key set stored in userid's account on repo host.  If
    # students want to launch collision attacks between their own keys
    # to self-deny auth, more power to them.
    assert(pubkey)
    assert(pubkey.endswith('.pub'))
    with open(pubkey, 'rb') as fd:
        return raw_pubkey_canonical_name(owner = owner,
                                         pubkey = fd.read())
    pass

KEYDROP_CONF_TEMPLATE = """
# Allow {name} keydrop bot to add user pubkeys.
repo gitolite-admin
    RW      = {{keydrop_keyname}}
    # Disallow any placement of system or manager keys in user keydir.
    -  VREF/NAME/{user_pubkeys}/{system_pattern} = @all
    -  VREF/NAME/{user_pubkeys}/{manager_pattern} = @all
    # Allow dropbot access to any other pubkey in user keydir.
    RW VREF/NAME/{user_pubkeys}/{unprivileged_pattern} = {{keydrop_keyname}}
    # Disallow dropbot access to everything else (including subdirs of user keydir).
    -  VREF/NAME/ = {{keydrop_keyname}}
""".format(name = codetub.NAME.lower(),
           user_pubkeys = USER_PUBKEYS,
           system_pattern = system_name('.*'),
           manager_pattern = manager_name('.*'),
           unprivileged_pattern = unprivileged_name(pubkey('[^/]*')))
           

MANAGER_ID_TEMPLATE = manager_name("courses-{course}-{instance}")

COURSE_CONF_PATH_TEMPLATE = "{course_conf_dir}/{{course}}{{instance}}.conf".format(course_conf_dir = _COURSE_CONF_DIR)

COURSE_INCLUDE_TEMPLATE = """
# Configuration for {{course}} {{instance}}
include "{course_conf_dir}/{{course}}{{instance}}.conf"
""".format(course_conf_dir = _COURSE_CONF_DIR)

STARTERS = 'starters'
SUBMISSIONS = 'submissions'

# Unfortunately, indirection of user permissions through groups
# appears to hide repos from the gitolite info command, so we inline
# all memberships.
COURSE_CONF_TEMPLATE = """# Configuration for {{course}}{{instance}}

# {{course}}{{instance}} students
@courses-{{course}}-{{instance}}-students = {{students}}

# {{course}}{{instance}} instructors
@courses-{{course}}-{{instance}}-managers = {{manager}} {{instructors}}

# {{course}}{{instance}} starter repos
@courses-{{course}}-{{instance}}-starter-repos = courses/{{course}}/{{instance}}/{starters}/[a-zA-Z0-9].*

# {{course}}{{instance}} solo submission repos
@courses-{{course}}-{{instance}}-solo-submission-repos = courses/{{course}}/{{instance}}/{submissions}/CREATOR/[a-zA-Z0-9].*

# {{course}}{{instance}} team submission repos
@courses-{{course}}-{{instance}}-team-submission-repos = courses/{{course}}/{{instance}}/{submissions}/CREATOR-[a-zA-Z0-9].*/[a-zA-Z0-9].* courses/{{course}}/{{instance}}/{submissions}/[a-zA-Z0-9].*-CREATOR/[a-zA-Z0-9].* courses/{{course}}/{{instance}}/{submissions}/[a-zA-Z0-9].*-CREATOR-[a-zA-Z0-9].*/[a-zA-Z0-9].*

# {{course}}{{instance}} tub repo
@courses-{{course}}-{{instance}}-tub-repo = courses/{{course}}/{{instance}}/tub

# {{course}}{{instance}} starter repo ACL
repo @courses-{{course}}-{{instance}}-starter-repos
    # Instructors in the course can create and access {starters}
    C       =   {{manager}} {{instructors}}
    RW      =   {{manager}} {{instructors}}
    # Students in the course can read {starters}
    R       =   {{students}}

# {{course}}{{instance}} solo submission repo ACL
repo @courses-{{course}}-{{instance}}-solo-submission-repos
    # Students in the course can create and access submissions
    C       =   {{students}}
    RW      =   CREATOR
    # Instructors in the course can access submissions
    RW      =   {{manager}} {{instructors}}

# {{course}}{{instance}} team submission repo ACL
repo @courses-{{course}}-{{instance}}-team-submission-repos
    # Students in the course can create and access submissions
    # Permission can be granted to teammates.
    C       =   {{students}}
    RW      =   CREATOR
    RW      =   WRITERS
    # Instructors in the course can access submissions
    RW      =   {{manager}} {{instructors}}

# {{course}}{{instance}} tub repo ACL
repo @courses-{{course}}-{{instance}}-tub-repo
    # Instructors in the course can create and access the tub repo
    C       =   {{manager}} {{instructors}}
    RW      =   {{manager}} {{instructors}}
""".format(starters = STARTERS, submissions = SUBMISSIONS)

LOCAL_CONTEXT = codetub.dotpath('admin')
LOCAL_KEYNAME = system_name('admin')
LOCAL_CONFFILE = 'config.json'

LOCAL_SSH_CONFIG_TEMPLATE = '''
Host {host}
  Port {port}
  UserKnownHostsFile {known_hosts}
  PasswordAuthentication no
  IdentityFile {key}
  IdentitiesOnly yes
  HostKeyAlgorithms ssh-ed25519,ssh-rsa
'''
LOCAL_SSH_CONFIG_FILE = 'ssh_config'
LOCAL_SSH_KNOWN_HOSTS_FILE = 'ssh_known_hosts'

CONFIG_VERSION = 0

DEFAULT = 'default'

def load_config(cid):
    config = os.path.join(LOCAL_CONTEXT, cid, LOCAL_CONFFILE)
    if os.path.exists(config):
        return Config().load(config)
    else:
        return None


