import codetub
from codetub import admin
from codetub.cli import *
import socket
        
class Init(Task):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("--private-known-hosts",
                            dest = 'privhosts', action = 'store_true',
                            help = "use a private (non global) known hosts file")
        parser.add_argument("--repouser", help = "repository host user [%(default)s]",
                            default = 'git')
        parser.add_argument("--repohost", help = "repository host hostname [%(default)s]",
                            default = socket.gethostname())
        parser.add_argument("--repoport", help = "repository host SSH port [%(default)s]",
                            default = "22")

    def execute(self, oldconf, opts):
        old_umask = os.umask(UMASK_U_RWX)

        # if oldconf:
        #     self.error('{} is already configured.'.format(opts.config))
        #     pass

        # Canonical ID of host
        hostid = '{user}@{host}'.format(user = opts.repouser,
                                     host = opts.repohost)
        # Base for local context.
        base = os.path.join(admin.LOCAL_CONTEXT, hostid)
        os.makedirs(base, mode = MODE_U_RWX, exist_ok = True)

        conf = Config(
            version = admin.CONFIG_VERSION,
            repo = os.path.join(base, admin.REPO),
            key  = os.path.join(base, admin.LOCAL_KEYNAME),
            known_hosts = os.path.join(base,
                                       admin.LOCAL_SSH_KNOWN_HOSTS_FILE
                                       if opts.privhosts
                                       else os.path.expanduser('~/.ssh/known_hosts')),
            ssh_config = os.path.join(base, admin.LOCAL_SSH_CONFIG_FILE),
            hostid = hostid,
            repouser = opts.repouser,
            repohost = opts.repohost,
            repoport = opts.repoport,

            coderoot = None,
            dropuser = None,
            drophost = None,
            dropport = None,
            dropbot = None,
            dropbot_setuid = None,
            dropdir = None,
            dropgit = None,
            droppython = None
        )
        conf.update(configpath = os.path.join(base, admin.LOCAL_CONFFILE))

        # Save config by canonical name.
        conf.store()

        # If given name was not canonical, alias it.
        if opts.config != hostid:
            link = os.path.join(admin.LOCAL_CONTEXT, opts.config)
            if os.path.exists(link):
                if not os.readlink(link) == hostid:
                    self.error('{} already aliases {}.'.format(opts.config,
                                                               hostid))
                    pass
                pass
            else:
                os.symlink(hostid, link)
                pass
            pass

        # Generate key.
        if not os.path.exists(conf.key):
            self.run([
                'ssh-keygen', '-t', 'rsa', '-b', '4096', '-N', '',
                '-C', os.path.basename(conf.key), '-f', conf.key
            ])
            pass

        # Generate SSH config.
        with open(os.path.join(base, admin.LOCAL_SSH_CONFIG_FILE), 'w') as ssh_config:
            ssh_config.write(admin.LOCAL_SSH_CONFIG_TEMPLATE.format(
                host = conf.repohost,
                port = conf.repoport,
                known_hosts = conf.known_hosts,
                key = conf.key
            ))
            pass

        self.showln('Private key: {}'.format(conf.key))
        self.showln('Public key:  {}'.format(admin.pubkey(conf.key)))
        pass
    pass

class Clone(Task):
    def execute(self, conf, opts):
        old_umask = os.umask(UMASK_U_RWX)

        try:
            # Scan the repohost keys and persist to known_hosts.
            with open(conf.known_hosts, 'a') as known_hosts:
                known_hosts.write(self.run(
                    ['ssh-keyscan', '-p', conf.repoport, conf.repohost],
                    output = True
                ))
        except:
            self.error('{host} lookup failed.'.format(conf.repohost))
            pass

        ssh = 'ssh -F {ssh_config}'.format(ssh_config = conf.ssh_config)
        if not os.path.exists(conf.repo):
            self.run([
                'env',
                'GIT_SSH_COMMAND=' + ssh,
                'git', 'clone', codetub.repourl(admin.REPO,
                                                user = conf.repouser,
                                                host = conf.repohost,
                                                port = conf.repoport),
                conf.repo
            ])
            self.run(['git', '-C', conf.repo, 'config', 'core.sshCommand', ssh])
            pass
        else:
            self.showln('repo exists')
        pass
    pass

