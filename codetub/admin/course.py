# CodeTub course hosting admin tasks
# Copyright (c) 2017-2018, Benjamin P. Wood

import codetub
from codetub import admin
from codetub.cli import *
from codetub.util import (UMASK_U_RWX, MODE_U_RWX)

import os
import pwd
import shutil
import socket

COURSE_CLIENT_TEMPLATE = """#!/bin/bash
exec /usr/bin/env python3 -B -c "
import os
import sys
sys.argv[0] = '$0'
sys.path.insert(0, '{code_dir}')
import codetub.client.tool
codetub.client.tool.main(os.path.basename('$0'), desc = {desc})
" "$@"
"""

class CourseSetup(Task):
    def __init__(self, parser):
        super().__init__(parser)
        ids = parser.add_argument_group('required course identifiers')
        ids.add_argument("-c", "--course", required = True,
                         metavar = 'CID',
                         help = "course ID (no spaces, e.g., cs240)")
        ids.add_argument("-i", "--inst", required = True,
                         metavar = 'SID', dest = 'instance',
                         help = "course instance/semester ID (no spaces, e.g., f18)")

        products = parser.add_argument_group('required paths for generated course tools')
        # products.add_argument("-d", "--desc", required = True,
        #                       help = "create course descriptor file for clients here")
        # FIXME: take only pubkey instead.  Model: manager sends pubkey to admin.
        products.add_argument("-k", "--key", default = None,
                              metavar = 'KEY', dest = 'privkey',
                              help = "use or create a private key for the manager bot here")
        products.add_argument("-t", "--tool", required = True,
                              help = "create a course-instance-specific tubclient here")

        acls = parser.add_argument_group('optional access control refinements')
        acls.add_argument("-m", "--managers", nargs = '+', default = [],
                          metavar = 'USER',
                          help = "grant manager access to these users [+ manager bot]")
        acls.add_argument("-s", "--students", nargs = '+', default = [],
                          metavar = 'USER',
                          help = "constrain student access to these users only [@all]")
        parser.add_argument("--repoport", help = "override repository host SSH port for tool")
        parser.add_argument("--dropport", help = "override keydrop host SSH port for tool")
        pass

    def execute(self, conf, opts):
        old_umask = os.umask(UMASK_U_RWX)

        if not opts.privkey:
            opts.privkey = '.codetub_ssh_key_' + opts.course + opts.instance
            pass

        # Make sure no keys are accidentally lost.
        # Either both exist or neither.
        if os.path.exists(opts.privkey) ^ os.path.exists(admin.pubkey(opts.privkey)):
            self.error("Only one of the private/public keys exists. Please restore or delete the other to be sure.")
            pass

        # Pull admin repo.
        self.run(['git', '-C', conf.repo, 'pull'])

        # Generate course descriptor.
        cid = '{course}{instance}'.format(course = opts.course,
                                          instance = opts.instance)
        desc = Config(
            version  = admin.CONFIG_VERSION,

            confid   = opts.course,

            # Course
            course   = opts.course,
            instance = opts.instance,

            # Repo host
            hostid   = conf.hostid,
            repobase = 'courses/{course}/{instance}'.format(
                course = opts.course,
                instance = opts.instance
            ),
            repouser = conf.repouser,
            repohost = conf.repohost,
            repoport = opts.repoport or conf.repoport,

            # Keydrop
            coderoot = conf.coderoot,
            dropuser = conf.dropuser,
            drophost = conf.drophost,
            dropport = opts.dropport or conf.dropport,
            dropbot  = conf.dropbot,
            dropbot_setuid  = conf.dropbot_setuid,
            dropdir  = conf.dropdir,

            droppython = conf.droppython
        )
        # desc.store(opts.desc)

        # Generate course tool.
        with open(opts.tool, 'w') as tool:
            tool.write(COURSE_CLIENT_TEMPLATE.format(
                code_dir = conf.coderoot,
                toolname = opts.course,
                desc = repr(desc.to_dict())
            ))
            pass
        os.chmod(opts.tool, 0o0755)

        # Create the course key dir if needed.
        course_keys = os.path.join(conf.repo, admin.MANAGER_PUBKEYS)
        os.makedirs(course_keys, exist_ok = True)

        # Create the manager keys.
        manager = admin.MANAGER_ID_TEMPLATE.format(course = opts.course,
                                                   instance = opts.instance)
        if not os.path.exists(opts.privkey):
            self.run(['ssh-keygen', '-t', 'rsa', '-b', '4096', '-N', '',
                      '-C', manager, '-f', opts.privkey])
            pass
        manager_pubkey = os.path.join(
            course_keys,
            admin.pubkey_canonical_name(owner = manager,
                                        pubkey = admin.pubkey(opts.privkey))
        )

        # Install the manager public key.
        shutil.copy2(admin.pubkey(opts.privkey), manager_pubkey)
        self.run(['git', '-C', conf.repo, 'add', manager_pubkey])
        
        # Create course configuration.
        main_conf = os.path.join(conf.repo, admin.MAIN_CONF)
        incl_ref = admin.COURSE_CONF_PATH_TEMPLATE.format(
            course = opts.course,
            instance = opts.instance
        )
        course_conf = os.path.join(conf.repo, admin.CONF, incl_ref)

        # If the course conf file does not exist, this is the first
        # attempt to add this course. Add an include in the main conf.
        if not os.path.exists(course_conf):
            with open(main_conf, 'a') as c:
                c.write(admin.COURSE_INCLUDE_TEMPLATE.format(
                    course = opts.course,
                    instance = opts.instance
                ))
                pass
            self.run(['git', '-C', conf.repo, 'add', main_conf])
            pass
        
        # Create or overwrite the course conf file.
        os.makedirs(os.path.dirname(course_conf), exist_ok = True)
        with open(course_conf, 'w') as c:
            c.write(admin.COURSE_CONF_TEMPLATE.format(
                course = opts.course,
                instance = opts.instance,
                instructors = ' '.join(admin.unprivileged_name(u)
                                       for u in opts.managers),
                students = ' '.join((admin.unprivileged_name(u)
                                     for u in opts.students)
                                    if opts.students else ['@all']),
                manager = manager
            ))
            pass
        self.run(['git', '-C', conf.repo, 'add', course_conf])

        # Commit and push the updates.
        if 0 < len(self.run(['git', '-C', conf.repo, 'diff', '--cached'],
                            output = True)):
            self.run([
                'git', '-C', conf.repo,
                'commit', '-m', 'new course {cid}'.format(cid = cid)
            ])
            self.run(['git', '-C', conf.repo, 'push'])
            pass

        pass
    pass
