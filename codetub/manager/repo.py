import codetub
from codetub.util import *
from codetub.manager.roster import *

from itertools import *
import re

def by(string, ids):
    """Does string contain a commit message attributed to any author in ids?"""
    return any(map(lambda x: x in string, ids))

class VCS(object):
    def __init__(self, vcs, run):
        self.vcstool = vcs
        self.run = run
    def vcs(self, *cmd, cwd = None, output = False):
        """Run a command in the local repo working copy using
        check_call (output=False) or check_output (output=True)."""
        if cwd:
            return self.run(self.vcstool + list(cmd),
                            cwd = cwd, output = output)
        else:
            return self.run(self.vcstool + list(cmd), output = output)
        pass
    pass

def extend_env(**extend):
    env = os.environ.copy()
    env.update(extend)
    return env

class Git(VCS):
    def __init__(self, run, ssh = 'ssh'):
        super().__init__(['git'],
                         lambda cmd, **kwargs:
                         run(cmd, env = extend_env(GIT_SSH_COMMAND = ssh),
                             **kwargs))
    def clone(self, src, dest = None):
        """Clone the remote repo to create the local repo."""
        if dest:
            self.run(self.vcstool + ['clone', src, dest], cwd = None)
        else:
            self.run(self.vcstool + ['clone', src], cwd = None)
            pass
        pass
    def pull(self, src = None):
        """Pull from the remote repo to the local repo and
        update/fast-foward/merge."""
        if src:
            self.vcs('pull', src)
        else:
            self.vcs('pull')
            pass
        pass
    def push(self, *args):
        """Push from the local repo to the remote repo."""
        self.vcs('push', *args)
        pass
    def log(self, *args):
        """Get the log."""
        return self.vcs('log', *args, output = True)
    def status(self, *paths, **kwargs):
        """Get the status"""
        if kwargs.get('noignore'):
            return self.vcs('status', '-s', '--ignored', *paths, output = True)
        else:
            return self.vcs('status', '-s', *paths, output = True)
    def diff(self, *paths):
        """Get the diff."""
        return self.vcs('diff', *paths, output = True)
    def latest(self):
        """Get the latest commit."""
        return self.vcs('show', '--no-patch', 'HEAD', output = True)
    def latest_id(self):
        """Get the latest commid ID."""
        return self.vcs('rev-parse', output = True)
    def archive(self, dest):
        """Archive the local repo to the given destination."""
        self.vcs('checkout-index', '-a', '-f',
                 '--prefix={dest}/'.format(dest = dest))
        pass
    def discard(self):
        """Discard uncommitted working copy/index changes."""
        self.vcs('reset', '--hard')
        pass
    def stage(self, *path):
        self.vcs('add', *path)
    def commit(self, message):
        self.vcs('commit', '-m', message)
        pass
    def commit_files(self, message, files, noignore = False):
        if noignore:
            self.vcs('add', '-f', *files)
        else:
            self.vcs('add', *files)
            pass
        self.vcs('commit', '-m', message, *files)
        pass
    def commit_tracked(self, message):
        self.vcs('commit', '-m', '-a', message)
        pass
    def branch_name(self):
        return self.vcs('branch', '--show-current', output = True).strip()
    def branch_rename(self, name):
        if self.branch_name() != name:
            self.vcs('branch', '-M', name)
            pass
        pass
    pass

class Repo(object):
    """Abstract base for hosted repos."""
    def __init__(self, make_vcs, localpath, hosturl, ssh = None):
        self.vcs = make_vcs(make_run(cwd = localpath), ssh = ssh)
        self.localpath = localpath
        self.hosturl = hosturl
        pass
    def __str__(self):
        return self.hosturl
    def clone(self, dest = None):
        """Clone the remote repo to create the local repo."""
        self.vcs.clone(self.hosturl, dest = self.localpath)
        pass
    def pull(self, src = None):
        """Pull from the remote repo to the local repo and
        update/fast-foward/merge."""
        self.vcs.pull(src = src)
        pass
    def get(self, src = None):
        if os.path.exists(self.localpath):
            self.pull()
        else:
            self.clone()
            pass
        pass
    def push(self, *args):
        """Push from the local repo to the remote repo."""
        self.vcs.push(*args)
        pass
    def log(self, *args):
        """Get the log."""
        return self.vcs.log(*args)
    def status(self, *paths, **kwargs):
        """Get the status"""
        return self.vcs.status(*paths, **kwargs)
    def changed(self, *paths):
        return self.status(*paths) != ''
    def diff(self, *paths):
        """Get the diff."""
        return self.vcs.diff(*paths)
    def latest(self):
        """Get the latest commit."""
        return self.vcs.latest()
    def latest_id(self):
        """Get the latest commit ID."""
        return self.vcs.latest_id()
    def archive(self, dest):
        """Archive the local repo to the given destination."""
        self.vcs.archive(dest)
        pass
    def discard(self):
        """Discard uncommitted working copy/index changes."""
        self.vcs.discard()
        pass
    def commit_files(self, message, files, **kwargs):
        self.vcs.commit_files(message, files, **kwargs)
        pass
    def commit_tracked(self, message):
        self.vcs.commit_tracked(message)
        pass
    def branch_name(self):
        return self.vcs.branch_name()
    def branch_rename(self, name):
        self.vcs.branch_rename(name)
        pass
    pass

class ManagedRepo(Repo):
    def __init__(self, project, localpath, hostpath):
        super().__init__(project.tub.vcs, localpath, hostpath,
                         ssh = project.tub.ssh)
        self.localpath = localpath
        self.project = project
        self.tub = project.tub
        pass
    def local_path(self):
        return self.localpath
    def remote_url(self):
        return self.hosturl
    def local_exists(self):
        """Does the local repo exist?"""
        return os.path.exists(self.local_path())
    def remote_exists(self):
        """Does the remote repo exist?"""
        assert False # FIXME use gitolite info
        return True
    pass

class StarterRepo(ManagedRepo):
    """Represents a starter code repository."""
    def __init__(self, project):
        super().__init__(project,
                         os.path.join(project.tub.starters_dir,
                                      project.id),
                         project.tub.host.starter_url(project))
    def local_path(self):
        """Path to local repo: starters/project"""
        return os.path.join(self.tub.starters_dir, self.project.id)
    pass

class StudentRepo(ManagedRepo):
    """Represents a student code repository."""

    SYNC_LOCAL = 'none'
    """Sync level: just use local repo."""

    SYNC_DEMAND = 'missing'
    """Sync level: pull and update only if no local student commits."""
    
    SYNC_REMOTE = 'all'
    """Sync level: always pull and update."""
    
    SYNC_LEVELS = [SYNC_LOCAL, SYNC_DEMAND, SYNC_REMOTE]
    """All sync levels"""

    def __init__(self, project, owners, remote_url = None):
        super().__init__(project,
                         os.path.join(project.tub.submit_dir,
                                      project.id,
                                      teamid_from_students(owners)),
                         remote_url or project.tub.host.submit_url(project, owners))
        self.owners = owners
        self.grade = None
    def __eq__(self, other):
        return self.owners.__eq__(other.owners)
    def __cmp__(self, other):
        return self.owners.__cmp__(other.owners)
    def __lt__(self, other):
        return self.owners.__lt__(other.owners)
    def __hash__(self):
        return hash(self.teamid())
    def team(self):
        return list(self.owners)
    def teamid(self):
        return teamid_from_students(self.owners)
    def owned(self, student):
        return student in self.owners
    def keyowner(self):
        return self.owners[0]
    def has_commits(self):
        """Are there any student commits in the local repo?"""
        return (self.local_exists()
                # FIXME more modular...
                and any((not by(ch, self.tub.instructors)
                         and " started team " not in ch
                         and "signature by " not in ch
                         and "responses by " not in ch)
                        for ch in self.log().split('\n\ncommit ')
                        if ch.strip(' \t\n') != ''))
    def latest_student_commit(self):
        for commit in self.log()[len('commit '):].split('\n\ncommit '):
            if not by(commit, self.tub.instructors):
                return re.search(r'([0-9a-f]+)(\s|$)', commit).group(1)
            pass
        return None
    def has_grade(self):
        """Do grader commits follow student commits in the local repo?"""
        return (self.has_commits()
                and by(self.latest(), self.tub.instructors))
    def has_file(self, path):
        """Does the repo contain this file?"""
        return os.path.exists(os.path.join(self.local_path(), path))
    def has_sig(self):
        """Does the repo contain a signature file?"""
        return any(
            self.has_file(codetub.client.SIGN_PATH_TEMPLATE.format(u.vcid))
            for u in self.team()
        )
    def sync(self, level = SYNC_DEMAND):
        """Sync as needed. Do student commits exist locally?"""
        try:
            sank = False
            # Sync to desired level.
            if level == StudentRepo.SYNC_DEMAND:
                # If no local student commits, sync.
                if self.has_commits():
                    return (True, True)
                else:
                    self.get()
                    return (self.has_commits(), True)
                pass
            elif level == StudentRepo.SYNC_REMOTE:
                self.get()
                return (self.has_commits(), True)
            elif level == StudentRepo.SYNC_LOCAL:
                return (self.has_commits(), False)
            else:
                assert False
        except subprocess.CalledProcessError:
            return (False, False)
