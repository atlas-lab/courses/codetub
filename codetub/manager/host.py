import codetub
from codetub import admin as admin
from functools import reduce
import re

class Host(object):
    def __init__(self, tub):
        self.tub = tub
        pass

    def url(self, *path):
        return codetub.repourl(self.tub.repobase, *path,
                               user = self.tub.repouser,
                               host = self.tub.repohost,
                               port = self.tub.repoport)

    def starter_url(self, project):
        return self.url(admin.STARTERS, project.id)

    def submit_url(self, project, owners):
        # NB: avoid circularity
        return self.submit_url_from_teamid(project, project.tub.roster.teamid_from_students(owners))
    
    def submit_url_from_teamid(self, project, teamid):
        return self.url(admin.SUBMISSIONS, teamid, project.id)

    def acl(self):
        return (
            line.strip() for line in
            self.tub.run([
                'ssh', '{user}@{host}'.format(user = self.tub.repouser,
                                              host = self.tub.repohost),
                '-F', self.tub.ssh_config,
                'info'
            ], output = True).split('\n')[1:]
            if '' != line
        )

    def repo_paths(self):
        return (line.split()[-1] for line in self.acl() if '*' not in line)
    
    def teamids(self, project):
        def teamid_from_repopath(repopath):
            match = re.search('{prefix}/([^/]+)/{project}$'.format(
                prefix = admin.SUBMISSIONS,
                project = project.id
            ), repopath)
            return match.group(1) if match else None
        return sorted(filter(lambda t: t, map(teamid_from_repopath, self.repo_paths())))
