import argparse
import re

from codetub.cli import Task
from codetub.manager.host import *
from codetub.manager.project import load_proj
from codetub.manager.repo import *
from codetub.manager.roster import load_roster
from codetub.util import *
from codetub.manager import default_roster_importer

TUB_FILE = '.tub.json'

class Tub(Config):
    def __init__(self, file, run, shows, **params):
        super().__init__(
            verbose      = PROGRESS,

            course       = None,
            instance     = None,
            title        = None,
            instructors  = [],

            envhosts     = [],
            envpath      = 'environment',
            gradebook    = 'gradebook.csv',
            gradepath    = None,
            grademessage = 'Feedback by course staff',
            managedpath  = 'tub',
            maxteam      = 1,
            minteam      = 1,
            privhosts    = False,
            privkeypath  = '.codetub_ssh_key',
            projectpath  = 'projects',
            remote       = None,
            rosterimport = None,
            rosterpath   = 'roster',
            survey       = [],
            signed       = True, # require signing submissions
            timeout      = None, # no timeout
            **params
        )

        self.run = run
        show, showln = shows
        self.show = show
        self.showln = showln
        self.show_timed = make_timer(show, showln)

        self.load(file)
        pass

    def load(self, file, **kwargs):
        self.exists = os.path.exists(file)
        if self.exists:
            super().load(file)
            pass
        self.base = os.path.realpath(os.path.dirname(file))
        self.reload(**kwargs)

    def reload(self, **kwargs):
        self.update(**kwargs)
        # Course Instance ID.
        self.identifier = (self.course or 'course') + (self.instance or '-instance')

        # Base directories.
        self.managed = os.path.join(self.base, self.managedpath,
                                    self.instance or 'new')

        # SSH configuration and keys
        self.ssh_config = os.path.join(self.base, '.codetub_ssh_config_' + (self.course or 'course') + (self.instance or 'instance'))
        self.privkey = os.path.join(self.base, self.privkeypath + '_' + (self.course or 'course') + (self.instance or 'instance'))
        self.known_hosts = (
            os.path.expanduser('~/.ssh/known_hosts') if not self.privhosts
            else os.path.join(self.base, '.codetub_ssh_known_hosts')
        )

        # User-provided directories.
        self.roster_dir   = os.path.join(self.managed, self.rosterpath)
        self.projects_dir = os.path.join(self.base, self.projectpath)
        self.env_dir      = os.path.join(self.base, self.envpath)

        # Managed directories.
        self.grades_dir = (
            os.path.join(self.base, self.gradepath) if self.gradepath
            else os.path.join(self.managed, 'grades')
        )
        self.sandbox_dir  = os.path.join(self.managed, 'sandboxes')
        self.starters_dir = os.path.join(self.managed, 'starters')
        self.submit_dir   = os.path.join(self.managed, 'submissions')

        # Repository host.
        self.remote = self.remote or 'git@localhost'
        m = re.match(r'([a-zA-Z][\d\w_\-]*)@([\d\w_\-\.]+)(?:\:(\d+))?(?:(/[^/])+)?',
                     self.remote)
        if m:
            self.repouser = m.group(1) or 'git'
            self.repohost = m.group(2) or 'localhost'
            self.repoport = m.group(3) or '22'
            self.repobase = (m.group(4)[1:] if m.group(4)
                             else os.path.join('courses',
                                               self.course,
                                               self.instance)
                             if self.course and self.instance
                             else None)
            pass

        self.ssh = 'ssh -F {ssh_config}'.format(ssh_config = self.ssh_config)
        self.vcs = Git
        
        # Derived
        self.host = Host(self)
        self.roster = load_roster(
            self.rosterimport or default_roster_importer.__file__,
            self.roster_dir
        )

        # Directory types
        self.create_dirs = [
            self.base,
            self.managed,
            os.path.dirname(self.privkey),
            
            self.env_dir,
            self.roster_dir,
            self.projects_dir,

            self.grades_dir,
            self.sandbox_dir,
            self.starters_dir,
            self.submit_dir,
        ]
        self.managed_paths = [
            self.sandbox_dir,
            self.starters_dir,
            self.submit_dir,
        ]
        self.snapshot_paths = [
            self.projects_dir,
            self.env_dir,
            self.privkey,
            self.privkey + '.pub',
        ]
        pass
    pass

def load_tub(opts):
    """Load the Tub for the given options."""
    path = findup(opts.config)
    if path:
        os.chdir(os.path.dirname(path))
        pass
    return Tub(opts.config,
               make_run(pretend = opts.pretend, verbose = opts.verbose),
               make_show(opts.verbose))

## TASKS ###################


class InitTub(Task):
    """Task to initialize a new tub in the current directory."""

    def __init__(self, parser):
        super().__init__(parser)
        # Tub parameters.
        params = parser.add_argument_group('configuration arguments')
        params.add_argument('-c', '--course', metavar = 'CID',
                            help = 'course ID (no spaces, e.g., cs240)')
        params.add_argument('-i', '--inst', metavar = 'SID', dest = 'instance',
                            help = 'course instance/semester ID (no spaces, e.g., f18)')
        params.add_argument('-k', '--key', metavar = 'KEY', dest = 'privkey',
                            help = 'private key for manager access to repo host')
        params.add_argument('-r', '--remote',
                            help = 'remote repository host (repouser@host[:port])')
        params.add_argument('-t', '--title',
                            help = 'course title')
        params.add_argument('-e', '--envpath',
                            help = 'directory for course environment definitions')
        params.add_argument('-w', '--workstations', nargs = '+', default = [],
                            help = 'physical workstation environment hosts for testing')
        # Optional archiving.
        optional = parser.add_argument_group('optional arguments')
        optional.add_argument('--no-archive', dest = 'archive', action = 'store_false',
                              default = True,
                              help = 'do not archive projects and environment with starters and submissions')
        # Pretending and forcing.
        advanced = parser.add_argument_group('advanced arguments')
        advanced.add_argument("--overwrite", action = "store_true",
                              help = "force replacement of existing tub")
        advanced.add_argument("--private-known-hosts",
                              dest = 'privhosts', action = 'store_true',
                              help = "use a private (non global) known hosts file")
        pass
    
    def execute(self, tub, opts):
        os.umask(UMASK_U_RWX)

        self.showln("{act} tub file {file} exists.".format(
            act = 'Updating' if tub.exists else 'Creating',
            file = TUB_FILE
        ), PROGRESS)

        # BEFORE making changes to a new instance, snapshot relevant
        # unmanaged items into the outgoing instance's managed directory.
        if (tub.exists and opts.archive and tub.instance and opts.instance
            and tub.instance != opts.instance):
            for path in tub.snapshot_paths:
                if os.path.exists(os.path.join(tub.base, path)):
                    relcopy(tub.base, tub.managed, path)
                    pass
                pass
            self.showln('Archived {paths} to {archive}.'.format(
                paths = ', '.join(os.path.relpath(p, tub.base)
                                  for p in tub.snapshot_paths),
                archive = os.path.relpath(tub.managed, tub.base)
            ), PROGRESS)
            pass

        # Configure tub.
        tub.course = opts.course or tub.course
        tub.instance = opts.instance or tub.instance
        tub.privkeypath = opts.privkey
        tub.remote = opts.remote or tub.remote
        tub.privhosts = opts.privhosts if type(opts.privhosts) != bool else tub.privhosts
        tub.envhosts = opts.workstations or tub.envhosts
        tub.envpath = opts.envpath or tub.envpath
        tub.load(opts.config)
        tub.course = opts.course or tub.course
        tub.instance = opts.instance or tub.instance
        tub.privkeypath = opts.privkey
        tub.remote = opts.remote or tub.remote
        tub.privhosts = opts.privhosts if type(opts.privhosts) != bool else tub.privhosts
        tub.envhosts = opts.workstations or tub.envhosts
        tub.envpath = opts.envpath or tub.envpath
        # Many attributes now initialized.

        # Check for required parts.
        if not tub.course:
            self.usage_error("Course not specified.")
            pass

        # Create directory structure.
        for d in tub.create_dirs:
            os.makedirs(d, exist_ok = True)
            pass

        # Create key if needed.
        if not os.path.exists(tub.privkey):
            manager = tub.course + tub.instance
            run = make_run(pretend = opts.pretend, verbose = opts.verbose)
            run(['ssh-keygen', '-t', 'rsa', '-b', '4096', '-N', '',
                 '-C', manager, '-f', tub.privkey])
            pass

        # Generate SSH config.
        with open(tub.ssh_config, 'w') as ssh_config:
            ssh_config.write(admin.LOCAL_SSH_CONFIG_TEMPLATE.format(
                host = tub.repohost,
                port = tub.repoport,
                known_hosts = tub.known_hosts,
                key = tub.privkey
            ))
            pass

        # Scan the repohost keys and persist to known_hosts.
        # with open(tub.known_hosts, 'a') as known_hosts:
        #     known_hosts.write(self.run(
        #         ['ssh-keyscan', '-p', tub.repoport, tub.repohost],
        #         output = True
        #     ))

        # Save the tub config.
        tub.store(opts.config)

        # Done.
        self.showln("Initialized {}.".format(opts.config))

class RunGitAsManager(Task):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("args", nargs="*",
                            help = argparse.SUPPRESS)
        self.real_cwd = os.getcwd()
        pass
    def execute(self, tub, opts):
        exit(subprocess.run(['git'] + opts.args,
                            cwd = self.real_cwd,
                            env = extend_env(GIT_SSH_COMMAND = tub.ssh)).returncode)
    pass

