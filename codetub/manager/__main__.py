# CodeTub Manager
# Copyright (c) 2017-2018, Benjamin P. Wood

import sys
assert (3, 5) <= sys.version_info

from codetub.cli import main
from codetub.manager.tub import (
    TUB_FILE,
    load_tub,
    InitTub,
    RunGitAsManager
)
from codetub.manager.project import (
    DefineProject,
    TestProject,
    PublishProject,
    CollectProject,
    EvalProject,
    ShowProjectStatus,
    RecordProjectGrades,
    IgnoreRepo,
    ReattributeRepo,
    SubmitRepo,
    SurveyProject,
    ShutdownSandbox
)
from codetub.manager.roster import CompileGradebook
from codetub.util import UMASK_U_RWX

TASKS = [
    # Tub initialization
    (['init'], InitTub,
     'initialize or update a code tub in the current directory'),
    (['define', 'spec'], DefineProject,
     'generate a new project spec'),

    # Project tasks
    (['test'], TestProject,
     'test project starter and solution code in course environment'),
    (['publish'], PublishProject,
     'publish project starter code'),
    (['collect'], CollectProject,
     'collect project work from student repos'),
    (['survey'], SurveyProject,
     'display collected survey results'),
    (['eval'], EvalProject,
     'evaluate collected project work in student repos'),
    (['status'], ShowProjectStatus,
     'show collection and evaluation status'),
    (['record', 'report'], RecordProjectGrades,
     'record grades, optionally commit and push to student repos'),

    # Student repo tasks
    (['ignore'], IgnoreRepo,
     'mark student repo as ignored'),
    (['reattribute', 'reattr'], ReattributeRepo,
     'mark student repo as reattributed'),
    (['submit'], SubmitRepo,
     'mark student repo as submitted'),
    (['git'], RunGitAsManager,
     'run git as manager, e.g., tub git -- log --pretty=oneline'),

    # Tub tasks
    (['gradebook'], CompileGradebook,
     'compile gradebook CSV'),
    (['stop'], ShutdownSandbox,
     'shutdown sandbox providers'),
]

if __name__ == '__main__':
    main('tub', "CodeTub Course Manager", TASKS,
         config = TUB_FILE, load_config = load_tub, umask = UMASK_U_RWX)
