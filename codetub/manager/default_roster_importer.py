#!/usr/bin/env python3

import csv
import glob
import json
import sys

def student_from_row(row):
    return {
        'vcid':  row[0],
        'first': row[1],
        'last':  row[2],
        'email': row[3],
    }

def students_from_files(files):
    students = []
    for f in files:
        with open(f, newline = '') as f:
            rows = csv.reader(f)
            # Discard the header
            next(rows)
            students.extend(student_from_row(r) for r in rows)
        pass
    return students

if __name__ == '__main__':
    print(json.dumps(students_from_files(glob.glob(sys.argv[1] + '/*.csv'))))
