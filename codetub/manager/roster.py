from codetub.util import *
from codetub.cli import (Task, show_summary)
from codetub.grader import gradetree
from codetub.manager.task import TubBaseTask

import csv
import glob

class Student(Record):
    """Represents a student"""
    def __init__(self, attrs):
        super().__init__(**attrs)
        self.roster_index = None
        pass
    def name(self):
        return "{first} {last}".format(first = self.first,
                                       last  = self.last)

    def address(self):
        return "{first} {last} <{email}>".format(first = self.first,
                                                 last  = self.last,
                                                 email = self.email)
    def __eq__(self, other):
        assert type(other) == type(self)
        return self.vcid == other.vcid
    def __cmp__(self, other):
        return (self.last + self.first + self.vcid).__cmp__(other.last + other.first + other.vcid)
    def __lt__(self, other):
        return (self.last + self.first + self.vcid) < (other.last + other.first + other.vcid)        
    def __str__(self):
        return "{vcid}: {addr}".format(vcid = self.vcid,
                                       addr = self.address())
    def __hash__(self):
        return self.vcid.__hash__()

    def id(self):
        return self.vcid
    
    def update(self, new):
        self.email = new.email
        self.first = new.first
        self.last  = new.last
        if self.vcid != new.vcid:
            self.vcid  = new.vcid
            return True
        return False

def teamid_from_vcids(vcids):
    return '-'.join(sorted(vcids))

def teamid_from_students(students):
    return teamid_from_vcids(s.vcid for s in students)

def vcids_from_teamid(teamid):
    return teamid.split('-')

class Roster(object):
    """Represents the student roster."""

    def __init__(self, students):
        # NB: Aliases to avoid circularity
        self.teamid_from_vcids = teamid_from_vcids
        self.teamid_from_students = teamid_from_students
        self.vcids_from_teamid = vcids_from_teamid
        # Index
        self.ordered = sorted(map(Student, students),
                              key = lambda s: s.last + s.first + s.vcid)
        self.by_vcid = {}
        self.by_email = {}
        for (i, s) in enumerate(self.ordered):
            self.index(s, i)
            pass
        pass

    def __iter__(self):
        return self.ordered.__iter__()

    def empty(self):
        return len(self.ordered) == 0
            
    def index(self, s, i):
        """Build by_vcid and by_email from ordered."""
        assert s.vcid not in self.by_vcid
        self.by_vcid[s.vcid] = s
        assert s.email not in self.by_email
        self.by_email[s.email] = s
        s.roster_index = i
        return i

    def store(self, file = None):
        if not file:
            file = self.file
            pass
        store_json(json.dumps([s.to_dict() for s in self.ordered]), file)
        pass

    def vcid_enrolled(self, vcid):
        return vcid in self.by_vcid

    def student_from_vcid(self, vcid):
        return self.by_vcid[vcid]
    
    def students_from_vcids(self, vcids):
        return [self.by_vcid[vcid] for vcid in vcids if self.vcid_enrolled(vcid)]
    
    def students_from_teamid(self, teamid):
        return self.students_from_vcids(vcids_from_teamid(teamid))

    def teamid_valid(self, teamid):
        return all(self.vcid_enrolled(vcid) for vcid in vcids_from_teamid(teamid))
        
    pass # end Roster

def load_roster(importer, rosterpath):
    if os.path.exists(rosterpath):
        return Roster(json.loads(subprocess.check_output([importer,
                                                          rosterpath])))
    else:
        return Roster([])

class CompileGradebook(TubBaseTask):
    def execute(self, tub, opts):
        # Select team grade reports.
        reports = [
            r.shadow(possible = max(gradetree.total(g)[1] for g in r.grades))
            for r in (
                    Record().load(report)
                    for report in sorted(glob.glob(os.path.join(tub.grades_dir,
                                                                '*.json')))
            )
        ]
        total_possible = sum(r.possible for r in reports)

        # Compile student grades.
        students = [
            Record(
                student = s,
                projects = [
                    Record(
                        project = r.project,
                        possible = r.possible,
                        submissions = [
                            Record(grade =  grade, team = grade['members'])
                            for grade in r.grades
                            if s.vcid in grade['members']
                        ]
                    ) for r in reports
                ]
            )
            for s in tub.roster
        ]

        # Report missing and duplicate grades.
        missing = [miss for miss in (Record(student = s.student,
                                           projects = [r for r in s.projects
                                                       if 0 == len(r.submissions)])
                                     for s in students)
                   if miss.projects]
        dups = [dup for dup in (Record(student = s.student,
                                       projects = [r for r in s.projects
                                                   if 1 < len(r.submissions)])
                                for s in students)
                if dup.projects]

        # Write gradebook as CSV.
        csvpath = os.path.join(tub.grades_dir, tub.gradebook)
        with open(csvpath, 'w', newline = '') as f:
            gradebook = csv.writer(f)
            # Header row
            header = [
                'vcid', 'email',
                'first', 'last',
            ] + [
                'all awarded', 'all possible', 'all ratio',
                'dropworst awarded', 'dropworst possible', 'dropworst ratio',
            ] + [
                col for r in reports for col in [r.project + ' awarded',
                                                 r.project + ' possible']
            ]
            gradebook.writerow(header)

            # Row per student
            student_rows = []
            invalid_grades = []
            for s in students:
                grades = [(gradetree.total(project.submissions[0].grade)[0] if len(project.submissions) == 1 else 0, project.possible)
                          for project in s.projects]
                total_awarded = sum(a for a, p in grades)
#                total_possible = sum(p for a, p in grades)
                worst_awarded, worst_possible = max(
                    grades,
                    key = lambda g: ((total_awarded - g[0]) / (total_possible - g[1]))
                )
                dropworst_awarded = total_awarded - worst_awarded
                dropworst_possible = total_possible - worst_possible
                row = [
                    s.student.vcid, s.student.email,
                    s.student.first, s.student.last,
                ] + [
                    total_awarded,
                    total_possible,
                    total_awarded / total_possible
                ] + [
                    dropworst_awarded,
                    dropworst_possible,
                    dropworst_awarded / dropworst_possible
                ] + [
                    col for g in grades for col in g  # flatten(grades)
                ]
                gradebook.writerow(row)
                student_rows.append(row)
            pass

        def show_row(row):
            data = dict(zip(header, row))
            return '{:.<30} {:>5.1f} all    {}'.format(
                # data['vcid'],
                '{} {}'.format(data ['first'], data['last']),
                data['all ratio'] * 100,
                '    '.join('{points:>3d} {project}'.format(project = r.project, points = int(data[r.project + ' awarded']))
                          for r in reports)
            )
        show_summary(tub.showln, [
            ('Students with Grades', sorted(student_rows, key = lambda row: row[2]), show_row),
        ], title = 'Gradebook')

        sums = [0.0 for x in header]
        for s in student_rows:
            for i, n in enumerate(s):
                if type(n) == int or type(n) == float:
                    sums[i] += n
                    pass
                pass
            pass
        avg_row = [sum / float(len(student_rows)) for sum in sums]
        avg_row[2] = 'Arithmetic'
        avg_row[3] = 'Mean'
        tub.show('    ')
        tub.showln(show_row(avg_row))
        tub.showln()
        
        project_format = '{:.<' + str(max(len(r.project) for r in reports) + 2) + '} {:>5.1f}'
        show_summary(tub.showln, [
            ('Projects with Grades', reports, lambda r: project_format.format(r.project, r.possible)),
        ])

        tub.showln(('    ' + project_format).format("total", sum(r.possible for r in reports)))
        tub.showln()
        
        show_summary(tub.showln, [
            ('Gradebook CSV File', [csvpath], lambda f: f),
        ])

        show_summary(tub.showln, [
            ('Students with Missing Grades', missing,
             lambda r: '{:.<8}  {:.<30}  {}'.format(
                 r.student.vcid,
                 r.student.name(),
                 ', '.join(p.project for p in r.projects)
             )),
            ('Students with Duplicate Grades', dups,
             lambda r: '{:.<8}  {:.<30}  {}'.format(
                 r.student.vcid,
                 r.student.name(),
                 ', '.join(
                     '{project}:{{{teams}}}'.format(
                         project = p.project,
                         teams = teamid_from_vcids(s.team)
                     ) for p in r.projects for s in p.submissions
                 )
             )),
        ], title = 'Issues')

        pass
    pass
