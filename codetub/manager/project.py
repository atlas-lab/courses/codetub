import codetub
from codetub import admin
from codetub.cli import (Task, show_summary)
from codetub.client import QUESTION_FILE as SURVEY_FILE
from codetub.client import (check_survey, SURVEY_PROMPT_KEY, SURVEY_TYPE_KEY)
from codetub.grader import driver
from codetub.grader import gradetree
from codetub.grader import sample_grader
from codetub.manager.repo import *
from codetub.manager.task import TubBaseTask
import codetub.sandbox.strong as strong
import codetub.sandbox.weak as weak
import codetub.sandbox.unsafe as unsafe
from codetub.util import *

import argparse
import fnmatch
import importlib.util
import os
import shlex
import shutil
import socket
import sys
import time
from tqdm import tqdm

EVAL_PRODUCTS = [
    gradetree.GRADE_TEXT,
    gradetree.GRADE_DIR,
    gradetree.GRADE_JSON,
    gradetree.GRADE_OUT,
]

PROJECT_FILE = 'tubproject.json'

class Project(Record):
    """Represents a project."""
    def __init__(self, tub, projfile = None, **attrs):
        super().__init__(
            id = None,
            title = None,
            minteam = tub.minteam,
            maxteam = tub.maxteam,
            includepath = 'private',
            starterpath = 'public/starter',
            solnpath = 'private/solution',
            pubhook = [],
            eval = 'grader.py',
            targets = [],
            includes = [],
            products = [],
            survey = [],
            docker_opts = [],
            appliance_opts = [],
            host_opts = [],
            timeout = tub.timeout,
        )
        self.update(**attrs)
        self.tub = tub

        # paths
        if projfile:
            self.load(projfile)
            self.file = os.path.realpath(projfile)
            self.base = os.path.realpath(os.path.dirname(self.file))
            self.include_dir = os.path.join(self.base, self.includepath)
            self.eval_script = os.path.join(self.include_dir, self.eval)
            self.starter_template = os.path.join(self.base, self.starterpath)
            self.solution = os.path.join(self.base, self.solnpath)
        else:
            self.file = None
            self.base = None
            self.include_dir = None
            self.eval_script = None
            self.starter_template = None
            self.solution = None
            pass
        # Directory for student submission repos.
        self.submit_dir = os.path.join(tub.submit_dir, self.id)
        # Starter repository.
        self.starter_repo = StarterRepo(self)

        # Create context for sandbox provider for this project.
        self.docker_provider = strong.DockerSandboxProvider(
            image = tub.identifier,
            imagedir = tub.env_dir,
            stagebase = os.path.join(tub.sandbox_dir, self.id),
            logfile = gradetree.GRADE_OUT,
            boxopts = self.docker_opts,
            onbuild = lambda p:
            tub.showln('Building sandbox image (may take a moment)...'),
            onstart = lambda p:
            self.tub.showln('Starting sandbox provider (may take a moment)...'),
            oncreate = lambda p:
            self.tub.showln('Creating sandbox provider (may take a few minutes)...'),
            timeout = self.timeout
        ) if tub.providertype == "docker" else None
        self.appliance_provider = weak.SharedVagrantSandboxProvider(
            defdir = tub.env_dir,
            stagebase = os.path.join(tub.sandbox_dir, self.id),
            logfile = gradetree.GRADE_OUT,
            boxopts = self.appliance_opts,
            onstart = lambda p:
            self.tub.showln('Starting sandbox provider (may take a moment)...'),
            oncreate = lambda p:
            self.tub.showln('Creating sandbox provider (may take a few minutes)...'),
            timeout = self.timeout
        ) if tub.providertype == "vm" else None
        self.host_providers = [
            unsafe.SharedRemoteSandboxProvider(
                ssh_host = h,
                stagebase = os.path.join(tub.sandbox_dir, self.id),
                logfile = gradetree.GRADE_OUT,
                boxopts = self.host_opts,
                timeout = self.timeout
            ) for h in tub.envhosts
        ]

        try:
            self.provider = (
                self.docker_provider if tub.providertype == "docker"
                else self.appliance_provider if tub.providertype == "vm"
                else self.host_providers[0]
            )
        except:
            raise Exception("No viable provider: updated .tub.json to specify one.")

        hooks_file = os.path.join(self.include_dir, 'hooks.py') if self.include_dir else None
        if hooks_file and os.path.exists(hooks_file):
            hooks_spec = importlib.util.spec_from_file_location('projects.{}.hooks'.format(self.id), hooks_file)
            self.hooks = importlib.util.module_from_spec(hooks_spec)
            hooks_spec.loader.exec_module(self.hooks)
            for h in ['pre_publish']:
                setattr(self.hooks, h, getattr(self.hooks, h, None))
                pass
            pass
        else:
            self.hooks = None
            pass

        self.evaluation_phases = []
        try:
            self.evaluation_phases = json.loads(subprocess.run([self.eval_script, '--dump-phases'],
                                                               check = True, capture_output = True).stdout)
            os.makedirs(self.submit_dir, exist_ok = True)
            store_json(self.evaluation_phases, phase_file)
        except:
            pass

        pass
    
    def __str__(self):
        return self.id
    
    def exists(self):
        return self.file and os.path.exists(self.file)

    pass


def load_proj(tub, project, task, require = True):
    """Load the project by ID or project file path."""
    if '/' in project:
        if os.path.exists(project):
            return Project(tub, projfile = project)
        else:
            task.error("Project file {} does not exist.".format(project))
            pass
    elif tub.projects_dir:
        pf = os.path.join(tub.projects_dir, project, PROJECT_FILE)
        if os.path.exists(pf):
            return Project(tub, projfile = pf, id = project)
        pass
    if not require:
        return Project(tub, id = project)
    else:
        task.error("Project {} does not exist.".format(project))
        pass
    pass


    
## TASKS ###################################

APPLIANCE_TAG = 'appliance'
SANDBOX_TAG = 'sandbox'
HOST_TAG = 'workstation'

TEST_ENVS = [APPLIANCE_TAG, SANDBOX_TAG, HOST_TAG]

def test(proj, task, phases = [], envs = None, stop = False):
    # Run the project grader:
    # - on:
    #   - starter template
    #   - solution
    # - in:
    #   - docker container (sandboxes named test-starter, test-solution)
    #   - vagrant base image
    #   - any ssh hosts given (should try on both server, client)
    #
    resultpaths = []
    def testenv(provider_name, provider):
        tub.show_timed("Starting provider",
                       lambda: provider.start())
        try:
            for target, target_name in [(proj.starter_template, 'starter'),
                                        (proj.solution, 'solution')]:
                if os.path.exists(target):
                    sandbox = provider.sandbox('test-{}-{}-{}'.format(
                        proj.id,
                        target_name,
                        provider_name
                    ))
                    resultpaths.append(os.path.relpath(sandbox.stagedir))
                    eval_one(proj, target, phases, sandbox, clean = True, published = False)
                    proj.tub.showln('Completed {}'.format(os.path.basename(sandbox.stagedir)))
                    pass
                pass
            pass
        finally:
            if stop:
                tub.show_timed("Stopping provider",
                               lambda: provider.stop())
                pass
            pass
        pass
    envs = set(envs or TEST_ENVS)
    if SANDBOX_TAG in envs:
        testenv(SANDBOX_TAG, proj.docker_provider)
        pass
    if APPLIANCE_TAG in envs:
        testenv(APPLIANCE_TAG, proj.appliance_provider)
        pass
    if HOST_TAG in envs:
        for provider in proj.host_providers:
            testenv(HOST_TAG + '-' + provider.ssh_host, provider)
            pass
        pass
    proj.tub.showln('Results are in:\n{}'.format(
        '\n'.join('  - {}'.format(p) for p in resultpaths)
    ))
    pass

# WARNING: ignores are top-level only.
def copy_template(source, dest, ignore = [], underwrite = True):
    nodes = []
    for node in os.listdir(source):
        if not any(fnmatch.fnmatch(node, pat) for pat in ignore):
            path = os.path.join(source, node)
            relcopy(source, dest, path, underwrite = underwrite)
            nodes.append(os.path.relpath(path, source))
            pass
        pass
    return nodes

def publish(proj, task, clear = False, copy = True,
            ignore = ['.git', '.hg', '*~', '.*~', '*#*', '.*#*'],
            yes = False, message = None):
    # Clear starter repo.
    if clear and os.path.exists(proj.starter_repo.local_path()):
        shutil.rmtree(proj.starter_repo.local_path())
        pass

    # Clone or pull the remote starter repo.
    repo_existed = os.path.exists(proj.starter_repo.local_path())
    proj.starter_repo.get()
    build_complete = False

    try:
    
        # Copy template into starter repo.
        sources = (
            copy_template(proj.starter_template, proj.starter_repo.local_path(),
                          ignore, underwrite = False)
            if copy and proj.starter_template else []
        )
        
        # Generate survey.
        if proj.survey and 0 < len(proj.survey):
            survey_file = os.path.join(proj.starter_repo.local_path(),
                                       SURVEY_FILE)
            check_survey(proj.survey)
            store_json(proj.survey, survey_file)
            sources.append(survey_file)
            pass
        
        # Run project pre-publish hooks
        if proj.hooks and proj.hooks.pre_publish:
            sources.extend(proj.hooks.pre_publish(proj) or [])
            pass
        
        # Confirm.
        proj.tub.show(proj.starter_repo.status(noignore = True))
        proj.tub.showln('All changes (including [un]staged/[un]tracked) shown above will be committed.')
        if not yes:
            proj.tub.show('OK [y/n]? ')
            if input().strip().lower() not in ['y', 'yes']:
                # NB: any necessary removal is handled by `finally`.
                task.error('Aborting publication.')
                pass
            pass
        
        # Commit.
        proj.starter_repo.commit_files(
            message or 'Starter code for {}'.format(proj.title or proj.id),
            sources,
            noignore = True
        )

        build_complete = True
    finally:
        if not repo_existed and not build_complete:
            shutil.rmtree(proj.starter_repo.local_path())
            pass
        pass
        
    # Push
    proj.starter_repo.branch_rename(codetub.DEFAULT_BRANCH)
    proj.starter_repo.push('--set-upstream', 'origin', codetub.DEFAULT_BRANCH)
    proj.tub.run(proj.tub.ssh.split() + ['-p', proj.tub.repoport, '{}@{}'.format(proj.tub.repouser, proj.tub.repohost),
                                         'symbolic-ref',
                                         '/'.join([proj.tub.repobase, admin.STARTERS, proj.id]),
                                         'HEAD',
                                         'refs/heads/{}'.format(codetub.DEFAULT_BRANCH)])
    pass

def progress_bar(total, complete, step = '*'):
    fmt = '[{:<' + str(int(total)) + '}]'
    return fmt.format(step * int(complete))

def collect(proj, sync, task, only = [], signed = True):
    """Collect student repos for the given project."""

    tub = proj.tub

    # Get the latest starter repo, to make sure it gets timestamped
    # before collected files.
    if sync != StudentRepo.SYNC_LOCAL:
        tub.show_timed("Syncing starter repo",
                       lambda: proj.starter_repo.get())
        pass
    
    # Create the submit dir for this project.
    os.makedirs(proj.submit_dir, exist_ok = True)

    # Gather repos.
    reattributed_marks = load_marks(proj, MARK_REATTRIBUTE)
    ignored_marks = load_marks(proj, MARK_IGNORE)
    submitted_marks = load_marks(proj, MARK_SUBMIT)
    
    if sync == StudentRepo.SYNC_REMOTE:
        remote_teamids = tub.show_timed("Fetching submission list from remote repo host",
                                        lambda: sorted(tub.host.teamids(proj)))
        store_marks(proj, MARK_REMOTES, remote_teamids)
        pass
    else:
        remote_teamids = load_marks(proj, MARK_REMOTES)
        pass
    all_repos = []
    ignored_teamids = []
    invalid_teamids = []
    for teamid in remote_teamids:
        if teamid in ignored_marks:
            # Ignored teamid
            ignored_teamids.append(teamid)
            pass
        elif teamid in reattributed_marks:
            # Reattributed teamid
            all_repos.append(StudentRepo(proj, tub.roster.students_from_teamid(reattributed_marks[teamid]),
                                         remote_url = tub.host.submit_url_from_teamid(proj, teamid)))
            pass
        elif tub.roster.teamid_valid(teamid):
            # Good teamid.
            all_repos.append(StudentRepo(proj, tub.roster.students_from_teamid(teamid)))
            pass
        else:
            # Unignored/unattributed bad teamid
            invalid_teamids.append(teamid)
            pass
        pass
    all_repos = sorted(all_repos, key = lambda r: r.teamid())

    marks = Record(
        ignored = sorted(ignored_teamids),
        reattributed = reattributed_marks,
        submitted = sorted(submitted_marks.keys()),
    )

    # Invalid team size.
    invalid_size_team_repos = [
        r for r in all_repos
        if not (proj.minteam <= len(r.team()) <= proj.maxteam)
    ]

    # Get repos.
    def sync_repo(r):
        # Sync the repo to desired level.
        has_commits, remote_sync = r.sync(level = sync)
        try:
            has_sig = r.has_sig()
        except:
            # temporary fix
            has_sig = False
            pass
        has_sig = has_sig or (r.teamid() in submitted_marks)
        return (has_sig, has_commits, r)
    repos_with_status = [sync_repo(r) for r in (all_repos if sync == StudentRepo.SYNC_LOCAL
                                                else tqdm(all_repos, desc = "Collecting submissions"))]
    signed_repos = [r for s, c, r in repos_with_status if s and c]
    unsigned_repos = [r for s, c, r in repos_with_status if c and not s]
    committed_repos = [r for s, c, r in repos_with_status if c]
    uncommitted_repos = [r for s, c, r in repos_with_status if not c]

    submitted_repos = signed_repos if signed else committed_repos
    unsubmitted_repos = [r for r in committed_repos if r not in submitted_repos]
    
    # Map student to list of repos where they are owners.
    all_repos_by_student = { s: [r for r in all_repos if s in r.owners]
                             for s in tub.roster }
    signed_repos_by_student = { s: [r for r in signed_repos if s in r.owners]
                                for s in tub.roster }
    unsigned_repos_by_student = { s: [r for r in unsigned_repos if s in r.owners]
                                  for s in tub.roster }
    committed_repos_by_student = { s: [r for r in committed_repos if s in r.owners]
                                   for s in tub.roster }
    uncommitted_repos_by_student = { s: [r for r in uncommitted_repos if s in r.owners]
                                     for s in tub.roster }

    submitted_repos_by_student = signed_repos_by_student if signed else committed_repos_by_student
    unsubmitted_repos_by_student = { s: [r for r in unsubmitted_repos if s in r.owners]
                                     for s in tub.roster }

    # Students with submitted repos.
    submitted_students = list(sorted(
        (s, rs) for s, rs in submitted_repos_by_student.items() if 1 == len(rs)
    ))

    # Students with multiple submitted repos.
    oversubmitted_students = list(sorted(
        (s, rs) for s, rs in submitted_repos_by_student.items() if 1 < len(rs)
    ))

    # Students with no submitted repo.
    unsubmitted_students = list(sorted(
        (s, rs) for s, rs in submitted_repos_by_student.items() if 0 == len(rs) and 0 < len(all_repos_by_student[s])
    ))

    # Students with started but uncommitted repos.
    uncommitted_students = list(sorted(
        (s, rs) for s, rs in uncommitted_repos_by_student.items() if 0 < len(rs)
    ))

    # Students with no repo started at all.
    unstarted_students = list(sorted(
        s for s in tub.roster if 0 == len(all_repos_by_student[s])
    ))

    # Try to select a single repo for each student.
    repo_by_student = { s: rs[0] for s, rs in submitted_repos_by_student.items()
                        if len(rs) == 1 }

    # Select students.
    selected_students = (
        [tub.roster.student_from_vcid(s) for s in only] if only
        else list(tub.roster)
    )

    # Select repos.
    selected_repos = set(repo_by_student[s] for s in selected_students
                         if s in repo_by_student)

    total_phases = len(proj.evaluation_phases)
    def graded_phases(r):
        complete = [
            p for p in proj.evaluation_phases
            if r.has_file(gradetree.path(gradetree.SUBGRADE_JSON_TEMPLATE.format(p)))
        ]
        complete_phases = len(complete)
        bar = progress_bar(total_phases, complete_phases)
        # tub.show('  {}: {} {}\n'.format(r.teamid(), bar,
        #                                 'fully graded' if complete_phases == total_phases
        #                                 else 'partly graded' if 0 < complete_phases
        #                                 else 'not graded'), PROGRESS, alt = '.')
        return complete_phases
    grading_status = [(r, graded_phases(r)) for r in sorted(selected_repos)]

    graded_repos = [r for r, c in grading_status if c == total_phases or (total_phases == 0 and r.has_grade())]
    grading_repos = [r for r, c in grading_status if 0 < c and c < total_phases]
    ungraded_repos = [r for r, c in grading_status if 0 == c]
    
    return Record(
        marks = marks,
        repos = Record(
            all = all_repos,
            graded = graded_repos,
            grading = grading_repos,
            ungraded = ungraded_repos,
            submitted = submitted_repos,
            signed = signed_repos,
            unsigned = unsigned_repos,
            unsubmitted = unsubmitted_repos,
            committed = committed_repos,
            uncommitted = uncommitted_repos,
            invalid_size_teams = invalid_size_team_repos,
            invalid_teamids = sorted(invalid_teamids),
            selected = list(sorted(selected_repos)),
        ),
        students = Record(
            all = list(tub.roster),
            selected = list(sorted(selected_students)),
            submitted = submitted_students,
            oversubmitted = oversubmitted_students,
            unsubmitted = unsubmitted_students,
            uncommitted = uncommitted_students,
            unstarted = unstarted_students
        ),
        issues = sum(map(len, [ungraded_repos, unsigned_repos, uncommitted_repos, invalid_size_team_repos,
                               oversubmitted_students, unsubmitted_students, uncommitted_students, unstarted_students]))
    )

def showas_collected(proj, teamid):
    return '{teamid:.>17}    {team}'.format(
        teamid = teamid,
        team = ', '.join(s.name() for s in proj.tub.roster.students_from_teamid(teamid))
    )

def show_proj_summary(proj, parts, **kwargs):
    return show_summary(proj.tub.showln, parts,
                        prefix = proj.id,
                        **kwargs)
def show_problem_summary(proj, harvest):
    tub = proj.tub
    ok = True
    parts = []
    if harvest.repos.invalid_teamids:
        parts.append(
            ('Repos with Invalid Team IDs',
             harvest.repos.invalid_teamids, lambda r: r),
        )
        pass
    if harvest.repos.invalid_size_teams:
        parts.append(
            ('Repos with Oversize Teams',
             harvest.repos.invalid_size_teams, lambda r: r.teamid()),
        )
        pass
    if harvest.repos.unsubmitted:
        parts.append(
            ('Repos with Unsubmitted Commits',
             harvest.repos.unsubmitted, lambda r: r.teamid()),
        )
        pass
    if harvest.repos.uncommitted:
        parts.append(
            ('Repos with No Commits',
             harvest.repos.uncommitted, lambda r: r.teamid()),
        )
        pass
    if parts:
        show_proj_summary(
            proj,
            parts,
            title = 'Repo Issues',
            showas = lambda teamid: showas_collected(proj, teamid)
        )
        ok = False
        tub.showln('  Use `tub {ignore,reattribute,submit}` to manage repo issues.\n')
        pass

    parts = []
    if harvest.students.oversubmitted:
        parts.append(
            ('Students with Multiple Submitted Repos',
             harvest.students.oversubmitted, lambda s: s[0].id()),
        )
        pass
    if harvest.students.unsubmitted:
        parts.append(
            ('Students with No Submitted Repo',
             harvest.students.unsubmitted, lambda s: s[0].id()),
        )
        pass
    if harvest.students.unstarted:
        parts.append(
            ('Students with No Started Repo',
             harvest.students.unstarted, lambda s: s.id()),
        )
        pass
    if parts:
        show_proj_summary(
            proj,
            parts,
            title = 'Student Issues',
            showas = lambda teamid: showas_collected(proj, teamid)
        )
        ok = False
        tub.showln('  Use `tub {ignore,reattribute,submit}` to manage repo issues.\n')
        pass
    return ok

def show_collect_summary(proj, harvest, show_problems = True):
    tub = proj.tub
    tub.showln()
    show_proj_summary(
        proj,
        [
            # Explicit marks.
            ('Repos Marked Ignored',
             harvest.marks.ignored, lambda teamid: teamid),
        ],
        title = 'Repo Marks',
        showas = lambda teamid: showas_collected(proj, teamid)
    )
    def showas_reattributed(proj, teamid, reattr):
        return '{teamid:.>17}    ->  {reattr}    {team}'.format(
            teamid = teamid,
            reattr = reattr,
            team = ', '.join(s.name() for s in proj.tub.roster.students_from_teamid(reattr))
        )
    show_proj_summary(
        proj,
        [
            ('Repos Marked Reattributed',
             sorted(harvest.marks.reattributed.keys()), lambda teamid: teamid),
        ],
        showas = lambda teamid: showas_reattributed(proj, teamid, harvest.marks.reattributed[teamid])
    )
    show_proj_summary(
        proj,
        [
            ('Repos Marked Submitted',
             harvest.marks.submitted, lambda teamid: teamid),
        ],
        showas = lambda teamid: showas_collected(proj, teamid)
    )
    tub.showln('  Use `tub {ignore,reattribute,submit}` to manage repo issues.\n')
    
    show_proj_summary(
        proj,
        [
            # Submitted repos (after marking).
            ('Repos Submitted',
             harvest.repos.submitted, lambda r: r.teamid()),
            ('Students with Submitted Repos',
             harvest.students.submitted, lambda s: s[0].id()),
        ],
        title = 'Collection Summary',
        showas = lambda teamid: showas_collected(proj, teamid)
    )
    if len(harvest.repos.selected) < len(harvest.repos.submitted):
        show_proj_summary(
            proj,
            [
                ('Repos Selected',
                 harvest.repos.selected, lambda r: r.teamid()),
            ],
             showas = lambda teamid: showas_collected(proj, teamid)
        )
        pass
    if show_problems:
        show_problem_summary(proj, harvest)
        pass
    pass

def show_grade_summary(proj, results):
    tub = proj.tub
    grade_format = '{teamid:.>17}{middle}    {team}'
    def fully_graded(g):
        graded_keys = set(gradetree.key(s) for s in gradetree.subgrades(g))
        return all(p in graded_keys for p in proj.evaluation_phases)
    show_proj_summary(proj, [
        ('Repos Fully Graded',
         list(filter(fully_graded, results.grades)),
         lambda g: grade_format.format(
             teamid = tub.roster.teamid_from_vcids(g['members']),
             team = ', '.join(tub.roster.student_from_vcid(vcid).name() for vcid in g['members']),
             middle = '    {award[0]:>5.1f} / {award[1]:>5.1f}'.format(award = gradetree.total(g))
         )),
        ('Repos Partly Graded',
         list(filter(lambda x: not fully_graded(x), results.grades)),
         lambda g: grade_format.format(
             teamid = tub.roster.teamid_from_vcids(g['members']),
             team = ', '.join(tub.roster.student_from_vcid(vcid).name() for vcid in g['members']),
             middle = '    {award[0]:>5.1f} / {award[1]:>5.1f}'.format(award = gradetree.total(g))
         )),
    ], title = "Grading Status")
    if len(results.ungraded) > 0:
        show_proj_summary(proj, [
            ('Repos Not Graded',
             results.ungraded,
             lambda r: grade_format.format(
                 teamid = r.teamid(),
                 middle = '',
                 team = ', '.join(s.name() for s in r.team()),
             ))
        ])
        pass
    pass

def invoke_grader_script(proj, phases, sandbox, flags = []):
    sandbox.dump_state()
    sys.stderr.flush()
    sys.stdout.flush()
    try:
        return sandbox.stage_run(
            [proj.eval_script] + phases + flags + [
                # These arguments defined by codetub.grader.driver.main.
                '--title',
                '{course}{instance} {title} ({id}) Grade Summary'.format(
                    course = proj.tub.course,
                    instance = proj.tub.instance,
                    title = proj.title,
                    id = proj.id
                ),
                '--sandbox-boxtype',  sandbox.__class__.__name__,
                '--sandbox-boxarg',   sandbox.arg,
                '--sandbox-boxid',    sandbox.boxid,
                '--sandbox-boxcwd',   sandbox.boxcwd,
                '--sandbox-stagedir', sandbox.stagedir,
            #     '--dep', proj.eval_script,
            # ] + [
            #     '--dep={}'.format(os.path.join(sandbox.updir, f))
            #     for f in (proj.includes + proj.targets)
            ],
            stdout = None,
            # Note: current environment contains PYTHONPATH where
            # CodeTub libs (for grading) are importable.
            # Grading scripts need this if importing codetub.grader...
        )
    finally:
        sandbox.load_state()
        pass
    pass

def eval_one(proj, submitdir, phases, sandbox, published = True,
             clean = False, pretend = False, checksign = True,
             force = False):
    """Run tests on one student repo.
       clean: remove products before eval and sandbox after eval
    """

    # Optionally clean the staging area.
    if clean:
        sandbox.stage_delete()
        pass
    
    fresh = not sandbox.stage_exists()
    sandbox.create()

    
    # Prepare the staging area only if not already prepared (to avoid
    # clobbering manual edits of staged files, e.g., to fix small syntax
    # errors so partial student code parses).
    if fresh:
        # Initialize staging area with the starter code from the
        # starter repository on which the submission is based.
        #
        # Note: we use the starter repo, not the current project
        # starter template, because the template may have changed
        # since publication, while the starter repo should still have
        # the version students used.
        if published:
            proj.starter_repo.archive(sandbox.updir)
        else:
            copy_template(proj.starter_template, sandbox.updir)
            pass

        # Now, remove anything that will get shadowed by targets or includes.
        for node in expand(sandbox.updir, proj.targets + proj.includes):
            if os.path.exists(node):
                rm_r(node)
                pass
            pass
        pass
    # Overlay the staging area's starter code base with evaluation
    # targets from the submission repo.
    for node in expand(submitdir, proj.targets):
        relcopy(submitdir, sandbox.updir, node)
        pass

    # Stage the (latest) grader script and supporting files.
    if proj.include_dir:
        for node in expand(proj.include_dir, proj.includes):
            relcopy(proj.include_dir, sandbox.updir, node)
            pass
        pass

    # Trim phases to those that need work.
    deps = [proj.eval_script] + [os.path.join(sandbox.updir, f) for f in proj.includes + proj.targets]
    def needs_eval(phase):
        if force:
            return True
        phase_grade_file = os.path.join(sandbox.workdir,
                                        gradetree.SUBGRADE_JSON_TEMPLATE.format(phase))
        return True or (
            not os.path.exists(phase_grade_file)
            # FIXME: decide whether to keep change/time-based auto-regardes
            # or any(os.path.getmtime(phase_grade_file) < os.path.getmtime(dep)
            #        for dep in deps if os.path.exists(d))
        )
    phases = list(filter(needs_eval, phases))
    # Avoid any more work if no evaluation is needed on this submission.
    if not phases:
        return True

    # Upload files from sandbox staging area to sandbox container.
    sandbox.upload()

    # Start the sandbox container.
    sandbox.start()

    try:
        # Invoke the grader script to run evaluation phases using the sandbox.
        result = invoke_grader_script(proj, phases, sandbox, flags = ['--overwrite'] if force else [])
        if not result:
            if result.expired():
                proj.tub.showln('<< GRADER TIMEOUT {} sec >>'.format(result.timeout))
            else:
                proj.tub.showln('<< GRADER FAILED >>')
            pass
    finally:
        
        try:
            # Download files from sandbox container to staging area.
            sandbox.download()
        finally:
            # Stop the sandbox container.
            sandbox.stop()
            # Delete the container.
            sandbox.box_delete()
            pass

        pass
    
    return result

def evaluate(proj, repos, sync, phases,
             clean = False, pretend = False, stop_provider = False,
             force = False):
    """Evaluate the given student repos for the given project."""
    tub = proj.tub

    assert proj.starter_repo.local_exists()

    # Ensure sandbox provider is ready.
    tub.show_timed("Starting sandbox provider",
                   lambda: proj.provider.start())
    
    try:
        # Check.
        results = Record(
            finished = [],
            aborted = []
        )
        for i, repo in tqdm(list(enumerate(repos)), desc = "Evaluating submissions", miniters = 1):
            sandbox = proj.provider.sandbox(repo.teamid()) # "{}-{}".format(proj.id, repo.teamid())
            # assert sandbox.isolated()
            if eval_one(proj, repo.local_path(), phases, sandbox,
                        clean = clean, pretend = pretend, force = force):
                results.finished.append(repo)
            else:
                results.aborted.append(repo)
                pass
            pass
    finally:
        pass
    
    if stop_provider:
        tub.show_timed("Stopping sandbox provider",
                       lambda: proj.provider.stop())
        pass

    return results

def erase(proj, repos, phases, pretend = False):
    """Erase evaluation metadata for the given phases in the given student repos for the given project."""
    tub = proj.tub

    if pretend:
        return

    for repo in repos:
        sandbox = proj.provider.sandbox(repo.teamid())
        invoke_grader_script(proj, phases, sandbox, flags = ['--erase'])
        pass
    pass

def status(proj, repos):
    """Report collection and evaluation status for the given project."""
    tub = proj.tub
    
    results = Record(
        graded = [],
        grades = [],
        ungraded = []
    )

    # Compile grade reports.
    for r in repos:
        sandbox = proj.provider.sandbox(r.teamid())
    
        # Build grade report
        gradejson = os.path.join(sandbox.workdir, gradetree.GRADE_JSON)
        if os.path.exists(gradejson):
            grade = gradetree.load(gradejson)
            grade['members'] = [s.vcid for s in r.owners]
            results.graded.append(r)
            results.grades.append(grade)
            pass
        else:
            results.ungraded.append(r)
            pass

    return results

def record(proj, repos, push = False, pretend = False, stop_provider = True):
    """Report evaluation results, record grades, push grades for the given project."""
    tub = proj.tub
    results = status(proj, repos)
    
    if pretend:
        return results

    # Compile and commit grade reports.
    for r, grade in tqdm(list(zip(results.graded, results.grades)), desc = "Formatting feedback"):
        repodir = r.local_path()
        sandbox = proj.provider.sandbox(r.teamid())

        # Create log.
        latest = r.latest_student_commit()
        meta = '''{title} ({id}) submission by:
{team}

Commit log as of this report:

{log}
'''.format(title = proj.title,
           id = proj.id,
           team = '\n'.join('  {}'.format(s) for s in r.team()),
           log = r.log(latest) if latest else r.log())

        # Save log to be scraped by grade report.
        os.makedirs(os.path.join(sandbox.workdir, gradetree.GRADE_DIR), exist_ok = True)
        metafile = os.path.join(gradetree.GRADE_DIR, 'meta.out')
        metafile_path = os.path.join(sandbox.workdir, metafile)
        with open(metafile_path, 'w') as log:
            log.write(meta)
            pass
        pass

        # Log changes necessary to compile/run.
        fixed_diff = ''
        for target in expand(repodir, proj.targets):
            # Check the diff if file is under 1 MB.
            if os.path.getsize(target) < (1<<20):
                diff = r.diff(target)
                diff_size = len(diff)
                # Log the diff if it is nonempty.
                if 0 < diff_size:
                    fixed_diff  += '''
Additional changes in {file} required to compile or run:

{diff}
'''.format(file = os.path.basename(target),
           diff = diff if diff_size < (1<<12) else '<< Large diff omitted >>')
                    pass
                pass
            pass

        if fixed_diff:
            # Save log and changes to be scraped by grade report.
            diff_file = os.path.join(gradetree.GRADE_DIR, 'fixed.diff')
            diff_path = os.path.join(sandbox.workdir, diff_file)
            with open(diff_path, 'w') as log:
                log.write(fixed_diff)
                pass
            pass

        # Gather evaluation products from sandbox download and working
        # directories back into repo. workdir overrides downdir.
        for d in [sandbox.downdir, sandbox.workdir]:
            for product in expand(d, proj.products + EVAL_PRODUCTS):
                relcopy(d, repodir, product, overwrite = True)
            pass
        pass
    
        # Build grade report
        gradejson = os.path.join(r.local_path(), gradetree.GRADE_JSON)
        if metafile not in gradetree.links(grade):
            grade['links'].insert(0, metafile)
            pass
        if fixed_diff and diff_file not in gradetree.links(grade):
            grade['links'].insert(0, diff_file)
            pass
        textpath = os.path.join(sandbox.workdir, gradetree.GRADE_TEXT)
        with open(textpath, 'w') as textf:
            gradetree.text(grade, output = textf, root = sandbox.workdir)
            pass
        relcopy(sandbox.workdir, repodir, textpath, overwrite = True)

    # Dump all per-team grades as json.
    os.makedirs(tub.grades_dir, exist_ok = True)
    Record(
        project = proj.id,
        grades = results.grades
    ).store(os.path.join(tub.grades_dir, proj.id + '.json'))

    # Push grades to remote student repos.
    if push:
        for r in tqdm(results.graded, desc = "Committing/pushing grades"):
            # Commit and push grades.
            products = [p for p in proj.products + EVAL_PRODUCTS
                        if os.path.exists(os.path.join(r.local_path(), p))
                        and r.changed(p)]
            if products: # NB: and implicitly, only if has student commits
                r.commit_files(tub.grademessage, products)
                pass
            # NB: always push, even if no changes, in case a crash
            # previously occurred between committing and pushing
            r.push()
            pass
        if stop_provider and False:
            proj.provider.stop()
            pass
        pass
    
    return results

#### Shared Task Behavior #####################################################

class ProjectBaseTask(TubBaseTask):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("project", metavar = 'PROJECT',
                            help = "project ID or description file")
        pass
    def prep(self, tub, opts):
        return load_proj(super().prep(tub, opts), opts.project, self)
    pass

class RepoBaseTask(ProjectBaseTask):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("-o", "-u", "--only", "--user",
                            metavar="ID", action = 'append',
                            default = [],
                            help = "only repos of selected student usernames")
        pass
    pass

class RepoSyncTask(RepoBaseTask):
    def __init__(self, parser, sync_default = StudentRepo.SYNC_DEMAND):
        super().__init__(parser)
        sync_levels = ','.join(
            map(lambda x:
                "[{}]".format(x) if x == sync_default else x,
                StudentRepo.SYNC_LEVELS)
        )
        parser.add_argument("-s", "--sync", metavar = "COND",
                            default = sync_default,
                            choices = StudentRepo.SYNC_LEVELS,
                            help = "repos to sync {{{}}}".format(sync_levels))
        parser.add_argument("--require-signed-DEPRECATED", dest = 'signed',
                            action = 'store_true', default = None,
                            help = argparse.SUPPRESS) #'require signed submissions')
        parser.add_argument("--allow-unsigned-DEPRECATED", dest = 'signed',
                            action = 'store_false', default = None,
                            help = argparse.SUPPRESS) # 'allow unsigned submissions')
        pass
    pass

#### Project Tasks #####################################################

class DefineProject(TubBaseTask):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("project", metavar = 'PROJECT',
                            help = "project ID or description file")
        parser.add_argument('--title', required = True,
                            help = 'full project title')
        parser.add_argument('--minteam', type = int,
                            help = 'minimum team size')
        parser.add_argument('--maxteam', type = int,
                            help = 'maximum team size')
        parser.add_argument('--target', default = [], action = 'append',
                            dest = 'targets',
                            help = 'files to be graded')
        parser.add_argument('--include', default = [], action = 'append',
                            dest = 'includes',
                            help = 'files to be graded')
        parser.add_argument('--product', default = [], action = 'append',
                            dest = 'products',
                            help = 'files to be graded')
    def execute(self, tub, opts):
        pdir = os.path.join(tub.projects_dir, opts.project)
        # Create project spec.
        spec = Record(
            id = opts.project,
            title = opts.title,
            minteam = opts.minteam if opts.minteam != None else tub.minteam,
            maxteam = opts.maxteam if opts.maxteam != None else tub.maxteam,
            targets = opts.targets,
            includes = opts.includes,
            products = opts.products,
            survey = tub.survey,
        )
        os.makedirs(pdir, exist_ok = True)
        projfile = os.path.join(pdir, PROJECT_FILE)
        if os.path.exists(projfile):
            spec.load(projfile)
            pass
        
        spec.store(projfile)
        
        project = Project(tub, projfile)

        # Create directories.
        os.makedirs(project.include_dir, exist_ok = True)
        os.makedirs(project.starter_template, exist_ok = True)

        # Create a grading script stub.
        if not os.path.exists(project.eval_script):
            shutil.copy2(sample_grader.__file__, project.eval_script)
            pass
        pass
    pass

class TestProject(ProjectBaseTask):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument('-e', '--env', metavar = 'ENV', action = 'append',
                            dest = 'envs', choices = TEST_ENVS,
                            help = 'test only in the selected execution environments')
        parser.add_argument('-p', '--phase', metavar = 'PHASE', action = 'append',
                            dest = 'phases', default = [],
                            help = 'test only the selected grader phases')
        parser.add_argument('-s', '--stop', action = 'store_true',
                            help = 'stop sandbox providers after tests')
    def execute(self, proj, opts):
        return test(proj, self,
                    phases = opts.phases, envs = opts.envs, stop = opts.stop)
    pass

class PublishProject(ProjectBaseTask):
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("-m", "--message",
                            help = "commit message")
        parser.add_argument("-y", "--yes", action = 'store_true',
                            help = "pre-confirm publication")
    def execute(self, proj, opts):
        return publish(proj, self,
                       yes = opts.yes, message = opts.message)
    pass

class CollectProject(RepoSyncTask):
    def __init__(self, parser):
        super().__init__(parser, sync_default = StudentRepo.SYNC_REMOTE)
        pass
    def execute(self, proj, opts):
        harvest = collect(proj, opts.sync, self, only = opts.only,
                          signed = opts.signed if opts.signed != None else proj.tub.signed)
        show_collect_summary(proj, harvest)
        pass
    pass

class SurveyProject(RepoSyncTask):
    def __init__(self, parser):
        super().__init__(parser, sync_default = StudentRepo.SYNC_LOCAL)
        parser.add_argument('-i', '--inst', dest = 'instance',
                            help = 'select specific course instance')
        pass
    def prep(self, tub, opts):
        if opts.instance:
            tub.reload(instance = opts.instance)
            pass
        return super().prep(tub, opts)
    def execute(self, proj, opts):
        harvest = collect(proj, opts.sync, self, only = opts.only)
        show_collect_summary(proj, harvest)
        responses = [(repo, load_json(f)["answers"]) for repo in harvest.repos.selected for f in glob.glob(os.path.join(repo.local_path(), '.*.answers.json'))]
        proj.tub.showln("Survey results:")
        for index, question in enumerate(proj.survey):
            proj.tub.showln("  " + question["prompt"])
            if question["type"] in ["int" , "float"]:
                team_answers = list(sorted(set((ans[index], repo) for repo, ans in responses if ans[index] != None)))
                answers, teams = zip(*team_answers)
                if 1 < len(answers):
                    proj.tub.showln("    min:    {:>2.1f}".format(min(answers)))
                    proj.tub.showln("    mean:   {:>2.1f}".format(float(sum(answers)) / len(answers)))
                    proj.tub.showln("    median: {:>2.1f}".format(answers[int(len(answers) / 2)] if len(answers) % 2 == 1
                                                       else float(answers[int(len(answers) / 2)] + answers[int(len(answers) / 2) + 1]) / 2.0))
                    proj.tub.showln("    max:    {:>2.1f}".format(max(answers)))
                    pass
                proj.tub.showln("    all:")
                for a, r in team_answers:
                    proj.tub.showln("      {:>2.1f}: {}".format(a, ", ".join(s.name() for s in r.team())))
                    pass
                pass
            else:
                team_answers = set((ans[index], repo) for repo, ans in responses)
                for a, r in team_answers:
                    proj.tub.showln("      {}: {}".format(a, ", ".join(s.name() for s in r.team())))
                    pass
                pass
            pass
        pass
    pass

class EvalProject(RepoSyncTask):
    def __init__(self, parser):
        super().__init__(parser, sync_default = StudentRepo.SYNC_LOCAL)
        parser.add_argument('-l', '--list', action = 'store_true',
                            help = 'list available evaluation phases without evaluating')
        parser.add_argument('-a', '--all', action = 'store_true',
                            help = 'also evaluate repos with grades already committed')
        parser.add_argument('-p', '--phase', metavar = 'PHASE', action = 'append',
                            dest = 'phases', default = [],
                            help = 'eval only the selected phases')
        manage = parser.add_mutually_exclusive_group()
        manage.add_argument('--clean', action = 'store_true',
                            help = 'destroy sandbox before evaluating')
        manage.add_argument('--force', action = 'store_true',
                            help = 'replace any existing results when evaluating')
        manage.add_argument('--erase', action = 'store_true',
                            help = 'erase existing results, but do not evaluate')
        pass
    def execute(self, proj, opts):

        if opts.list:
            for p in proj.evaluation_phases:
                print(p)
                pass
            return

        bad_phases = []
        for p in opts.phases:
            if p not in proj.evaluation_phases and p != driver.RAW_ADJUSTMENT_KEY:
                bad_phases.append(p)
                pass
            pass
        if bad_phases:
            self.error("Invalid phases: {}. Available phases are: {}.".format(
                ", ".join(bad_phases),
                ", ".join(proj.evaluation_phases),
            ))
            pass
        if not opts.phases:
            opts.phases = proj.evaluation_phases
            pass

        if opts.clean:
            print("WARNING: --clean is enabled! {}Confirmation will be required."
                  .format("(--clean overrides --force) " if opts.force else ""))
            pass
        elif opts.force:
            print("WARNING: --force is enabled! Confirmation will be required.")
            pass
        elif opts.erase:
            print("WARNING: --erase is enabled! Confirmation will be required.")
            pass

        harvest = collect(proj, opts.sync, self, only = opts.only,
                          signed = opts.signed if opts.signed != None else proj.tub.signed)

        repos = (harvest.repos.selected if opts.all
                 else harvest.repos.ungraded)

        if opts.clean:
            # Warn about cleaning.
            print("""
Please confirm the following effects of --clean,
which removes existing sandboxes:

  For these repositories:
{repos}

  These irreversible steps will precede evaluation:
    - Remove the existing sandbox for this repo, if any,
      destroying all evaluation metadata for all phases for this repo.
    - Create a fresh sandbox from the local copy of this submission repo.

  Sandboxes and evaluation metadata for repos not selected above will
  not be affected.
""".format(
    repos = "\n".join("    - {}".format(r.teamid()) for r in repos)
))
            if input("Proceed with cleaning the selected repos? [y/N]: ").strip() != 'y':
                self.error("Aborting to avoid cleaning.")
                pass
            print()
            pass
        
        elif opts.force:
            # Warn about forcing.
            print("""
Please confirm the following effects of --force,
which overwrites existing evaluation results:

  For these repositories:
{repos}

  For these phases:
{phases}

  Evaluation will run for this phase for this repo even if evaluation
  metadata for this phase already exists in the sandbox for this repo.
  - Evaluation will replace gradetree metadata for each selected phase
    in the sandbox of each selected repo.
  - Behavior of the project grader determines the effect on other
    generated metadata products.

  Evaluation metadata for repos not selected above, and for phases not
  selected above in repos selected above, will not be affected.
""".format(
    phases = "\n".join("    - {}".format(p) for p in opts.phases),
    repos = "\n".join("    - {}".format(r.teamid()) for r in repos)
))

            if input("Proceed with forced evaluation for the selected repos and phases? [y/N]: ").strip() != 'y':
                self.error("Aborting to avoid forced evaluation.")
                pass
            print()
            pass
        elif opts.erase:
            # Warn about erasing.
            print("""
Please confirm the following effects of --erase,
which removes existing evaluation results:

  For these repositories:
{repos}

  For these phases:
{phases}

  Evaluation metadata will be removed,
  but other generated products will remain.

  Metadata for repos not selected above, and for phases not
  selected above in repos selected above, will not be affected.
""".format(
    phases = "\n".join("    - {}".format(p) for p in opts.phases),
    repos = "\n".join("    - {}".format(r.teamid()) for r in repos)
))

            if input("Proceed with removal of evaluate metadata for the selected repos and phases? [y/N]: ").strip() != 'y':
                self.error("Aborting to avoid removal of evaluation metadata.")
                pass
            print()
            erase(proj, repos, opts.phases, pretend = opts.pretend)
            return

        # show_collect_summary(proj, harvest) #, show_problems = False)
        results = evaluate(proj, repos,
                           opts.sync, opts.phases,
                           clean = opts.clean, pretend = opts.pretend,
                           force = opts.force)
        proj.tub.showln()
        show_proj_summary(
            proj,
            [
                ('Repos with Evaluation Complete for {}'.format(', '.join(opts.phases)),
                 results.finished, lambda r: r.teamid()),
            ],
            title = 'Evaluation Summary',
            showas = lambda vcid: showas_collected(proj, vcid)
        )
        if 0 < len(results.aborted):
            show_proj_summary(
                proj,
                [
                    ('Repos with Evaluation Aborted for {}'.format(', '.join(opts.phases)),
                     results.aborted,  lambda r: r.teamid()),
                ],
                showas = lambda vcid: showas_collected(proj, vcid)
            )
            pass
        # show_problem_summary(proj, harvest)
        proj.tub.showln("  Use `tub status {}` for more info.\n".format(proj.id))
        pass
    pass

class ShowProjectStatus(RepoSyncTask):
    def __init__(self, parser):
        super().__init__(parser, sync_default = StudentRepo.SYNC_LOCAL)
        parser.add_argument('-a', '--all', action = 'store_true',
                            help = 'also report repos with grades already committed')
    def execute(self, proj, opts):
        harvest = collect(proj, opts.sync, self, only = opts.only,
                          signed = opts.signed if opts.signed != None else proj.tub.signed)
        results = status(proj, (harvest.repos.selected if opts.all
                                else harvest.repos.ungraded))
        show_collect_summary(proj, harvest)
        show_grade_summary(proj, results)
        proj.tub.showln('  This status report is ephemeral. Use `tub record` to commit/push grades.')
        proj.tub.showln()
    pass

class RecordProjectGrades(RepoSyncTask):
    def __init__(self, parser):
        super().__init__(parser, sync_default = StudentRepo.SYNC_LOCAL)
        parser.add_argument('-a', '--all', action = 'store_true',
                            help = 'also report repos with grades already committed')
        parser.add_argument('-p', '--push', action = 'store_true',
                            help = 'push grades to students')
    def execute(self, proj, opts):
        harvest = collect(proj, opts.sync, self, only = opts.only,
                          signed = opts.signed if opts.signed != None else proj.tub.signed)
        results = record(proj, (harvest.repos.selected if opts.all
                                else harvest.repos.ungraded),
                         push = opts.push,
                         pretend = opts.pretend)
        show_collect_summary(proj, harvest)
        show_grade_summary(proj, results)
        if not opts.push:
            proj.tub.showln('  Use `tub record --push` to send grades to students.')
            proj.tub.showln()
            pass
        pass
    pass

MARK_IGNORE = 'ignored'
MARK_REATTRIBUTE = 'reattributed'
MARK_REMOTES = 'remotes'
MARK_SUBMIT = 'submitted'
def load_marks(proj, key):
    path = os.path.join(proj.submit_dir, '.{}.json'.format(key))
    return load_json(path) if os.path.exists(path) else {}
def store_marks(proj, key, marks):
    os.makedirs(proj.submit_dir, exist_ok = True)
    store_json(marks, os.path.join(proj.submit_dir, '.{}.json'.format(key)))
    pass

class MarkRepoTask(ProjectBaseTask):
    def __init__(self, mark_key, parser):
        super().__init__(parser)
        self.mark_key = mark_key
        parser.add_argument('teamid', metavar = 'TEAMID',
                            help = 'team ID to mark as {}'.format(mark_key))
        parser.add_argument('-u', '--unmark', action = 'store_true',
                            help = 'unmark as {}'.format(mark_key))
    def execute(self, proj, opts):
        marks = load_marks(proj, self.mark_key)
        if opts.unmark:
            proj.tub.showln("Unmarking '{}' as {}: {}".format(opts.teamid, self.mark_key, marks.get(opts.teamid) or 'default'))
            # NB: set.remove(element) requires element in set.
            if opts.teamid in marks:
                del marks[opts.teamid]
                pass
            pass
        else:
            mark = self.mark(proj, opts)
            proj.tub.showln("Marking '{}' as {}: {}".format(opts.teamid, self.mark_key, mark or 'default'))
            marks[opts.teamid] = mark
            pass
        store_marks(proj, self.mark_key, marks)
        pass
    def mark(self, proj, opts):
        return True
    pass

class MarkRepoGroupTask(MarkRepoTask):
    def __init__(self, mark_key, parser):
        super().__init__(mark_key, parser)
        pass
    def execute(self, proj, opts):
        if opts.teamid == "@uncommitted":
            harvest = collect(proj, StudentRepo.SYNC_LOCAL, self, 
                              signed = proj.tub.signed)
            teamids = [r.teamid() for r in harvest.repos.uncommitted]
            pass
        elif opts.teamid == "@unsubmitted":
            harvest = collect(proj, StudentRepo.SYNC_LOCAL, self, 
                              signed = proj.tub.signed)
            teamids = [r.teamid() for r in harvest.repos.unsubmitted]
            pass
        elif opts.teamid.startswith('@'):
            self.error("Invalid teamid spec '{}'".format(opts.teamid))
            pass
        else:
            teamids = [opts.teamid]
            pass
        for teamid in teamids:
            opts.teamid = teamid
            super().execute(proj, opts)
            pass
        pass
class IgnoreRepo(MarkRepoGroupTask):
    def __init__(self, parser):
        super().__init__(MARK_IGNORE, parser)
        pass
    def mark(self, proj, opts):
        if not opts.unmark:
            # remove local copy
            if proj.tub.roster.teamid_valid(opts.teamid):
                repo = StudentRepo(proj, proj.tub.roster.students_from_teamid(opts.teamid))
                if repo.local_exists():
                    shutil.rmtree(repo.local_path())
                    pass
                sandbox = proj.provider.sandbox(repo.teamid())
                sandbox.stage_delete()
                pass
            else:
                assert not os.path.exists(os.path.join(proj.submit_dir, opts.teamid))
                pass
            pass
        return super().mark(proj, opts)
    pass

class ReattributeRepo(MarkRepoTask):
    def __init__(self, parser):
        super().__init__(MARK_REATTRIBUTE, parser)
        parser.add_argument('members', metavar = 'USERID', nargs = '+',
                            help = 'one or more reattributed team member user IDs')
        pass
    def mark(self, proj, opts):
        # Verify that all reattributed team member user IDs are enrolled.
        unenrolled = list(filter(lambda m: not proj.tub.roster.vcid_enrolled(m), opts.members))
        if 0 < len(unenrolled):
            self.error("Unenrolled user IDs: {}".format(' '.join(unenrolled)))
            pass

        # Verify disjointness from ignored set.
        if opts.teamid in load_marks(proj, MARK_IGNORE):
            self.error("Team '{}' is already ignored. Use `tub ignore -u` first.".format(opts.teamid))
            pass
        
        # rename local copy
        repo = StudentRepo(proj, proj.tub.roster.students_from_teamid(opts.teamid))
        repo_reattr = StudentRepo(proj, proj.tub.roster.students_from_vcids(opts.members),
                                  remote_url = proj.tub.host.submit_url_from_teamid(proj, opts.teamid))
        src, dst = (repo, repo_reattr) if not opts.unmark else (repo_reattr, repo)
        assert not dst.local_exists()
        if src.local_exists():
            os.rename(src.local_path(), dst.local_path())
            pass
        src_sandbox = proj.provider.sandbox(src.teamid())
        dst_sandbox = proj.provider.sandbox(dst.teamid())
        assert not dst_sandbox.stage_exists()
        if src_sandbox.stage_exists():
            os.rename(src_sandbox.stagedir, dst_sandbox.stagedir)
            pass

        
        return proj.tub.roster.teamid_from_vcids(opts.members)
    def cleanup(self, proj, opts):
        proj.tub.showln("Run `tub collect` to gather reattributed repos.")
    pass

class SubmitRepo(MarkRepoGroupTask):
    def __init__(self, parser):
        super().__init__(MARK_SUBMIT, parser)
        pass
    pass


class ShutdownSandbox(TubBaseTask):
    def execute(self, tub, opts):
        proj = Project(tub, id = "bogus")
        def stop():
            if proj.appliance_provider:
                proj.appliance_provider.stop()
                pass
            if proj.docker_provider:
                proj.docker_provider.stop()
                pass
            for provider in proj.host_providers:
                provider.stop()
                pass
        tub.show_timed("Shutting down sandbox providers",
                       lambda: stop())

