import argparse
import os
import textwrap
import time

from codetub.util import *

class Task(object):
    def __init__(self, parser):
        self.task_parser = parser
        self.task_start_time = time.time()
    def prep(self, conf, opts):
        return conf
    def execute(self, conf, opts):
        pass
    def do(self, conf, opts):
        self.show, self.showln = make_show(verbose=opts.verbose)
        self.run = make_run(verbose = opts.verbose,
                            pretend = getattr(opts, 'pretend', False))
        conf = self.prep(conf, opts)
        try:
            return self.execute(conf, opts)
        finally:
            self.cleanup(conf, opts)
        pass
    def cleanup(self, conf, opts):
        pass
    def duration(self, since = None, until = None):
        return (until or time.time()) - (since or self.task_start_time)
    def exit(self, status = 0, message = None):
        self.task_parser.exit(
            status = status,
            message = '{name} error: {msg}\n'.format(
                name = self.task_parser.prog,
                msg = message
            )
        )
        pass
    def error(self, message):
        self.exit(status = 1, message = message)
        pass
    def usage_error(self, message):
        self.task_parser.error(message)
        pass
    pass

def show_summary_line(showln, kind, elems, showas = str):
    showln(
        "{count:>3} {kind}\n    {elems}".format(
            kind = kind,
            count = len(elems),
            elems = '\n    '.join(map(showas, elems))
        ) #,
        # PROGRESS,
        # alt ="{kind:<20} ({count:>2}):  {elems}".format(
        #     kind = kind,
        #     count = len(elems),
        #     elems = '; '.join(map(showas, elems))
        # )
    )
    pass

def show_summary(showln, bins, title = None, showas = str, prefix = None):
    if title:
        showln('--- {title:-<76}\n'.format(title = (prefix + ': ' if prefix else '') + title.upper() + ' '))
        pass
    for kind, elems, sh in bins:
        show_summary_line(showln, kind, elems, showas = lambda x: showas(sh(x)))
        if len(elems) != 0:
            showln()
        pass
    pass

def main(prog, progdesc, task_defs, config = None, load_config = None,
         umask = None, **defaults):

    # Apply starter umask.
    if umask:
        os.umask(umask)
        pass
    
    ## Argument parser.
    task_summary = '\n'.join(
        "  {tasks:<14}    {descs}".format(
            tasks = ', '.join(t[0]),
            descs = textwrap.fill(t[2], 80 - 20, subsequent_indent = ' '*20)
        ) for t in task_defs
    )
    parser = argparse.ArgumentParser(
        prog = prog,
        usage = "%(prog)s [-h] [%(prog)s options] TASK [-h] [task options]",
        description = progdesc,
        formatter_class = argparse.RawDescriptionHelpFormatter,
        epilog = task_summary,
    )

    ## Add general options.
    parser.add_argument("-c", "--conf", default = config,
                        dest = 'config', metavar = 'CONF',
                        help = "select named configuration")
    parser.add_argument("-v", "--verbose", action = 'count', default = MODEST,
                        help = "increase verbosity")
    # Extras
    advanced = parser.add_argument_group('testing arguments')
    advanced.add_argument("-n", "--pretend", action = "store_true",
                              help = argparse.SUPPRESS)
    ## Set defaults, overriding any arg-specific defaults.
    parser.set_defaults(**defaults)
    
    ## Add command/task parser.
    subparsers = parser.add_subparsers(
        prog = prog,
        title = 'tasks (help: {} TASK -h)'.format(prog),
        dest = 'task'
    )

    ## Add individual options for each task.
    tasks = {}
    for (keys, task, tdesc) in task_defs:
        task_parser = subparsers.add_parser(keys[0], aliases = keys[1:],
                                            description = tdesc)
        task = task(task_parser)
        for k in keys:
            tasks[k] = task
            pass
        ## Set defaults, overriding any arg-specific defaults.
        task_parser.set_defaults(**defaults)
        pass

    ## Parse. If -h/--help, execution stops here.
    opts = parser.parse_args()
    
    ## Run the task.
    if opts.task in tasks:
        tasks[opts.task].do(load_config(opts) if load_config else None, opts)
    else:
        parser.print_help()
        pass
    pass
