import os
import re
import shlex
import shutil
import subprocess

from codetub.sandbox.base import *

VAGRANT_TARGET = 'sandbox'
SSH_CONFIG = '.vagrant_sandbox_provider_ssh_config'

class SharedVagrantSandbox(Sandbox):
    
    def __init__(self, defdir = None, **opts):
        super().__init__(defdir, **opts)
        if not self.boxcwd:
            self.boxcwd = os.path.join('/tmp/codetub-vagrantbox-' +  self.boxid)
            pass
        self.defdir = defdir
        self.ssh_config = os.path.join(self.defdir, SSH_CONFIG)
        with open(self.ssh_config) as conf:
            line = conf.readline()
            self.ssh_host = re.match(r'^Host\s+([\w\d_\-\.]+)',
                                     line.strip()).group(1)
            pass
        pass

    def box_exists(self):
        return self.stage_run([
            'vagrant', 'ssh', VAGRANT_TARGET, '-c',
            'test -e {}'.format(self.boxcwd),
        ], cwd = self.defdir)
    
    def box_create(self):
        self.stage_run([
            'vagrant', 'ssh', VAGRANT_TARGET, '-c',
            'umask {:04o} && mkdir -p {}'.format(STAGE_UMASK, self.boxcwd),
        ], cwd = self.defdir).check()
        return self

    def box_delete(self):
        self.stage_run([
            'vagrant', 'ssh', VAGRANT_TARGET, '-c',
            'rm -rf {}'.format(self.boxcwd)
        ], cwd = self.defdir).check()
        return super().box_delete()

    def upload(self, path = '.'):
        """Upload from the sandbox staging area to the sandbox container."""
        assert self.box_exists()
        dest = os.path.normpath(os.path.join(self.boxcwd, path))
        self.stage_run([
            'vagrant', 'ssh', VAGRANT_TARGET, '-c',
            'rm -rf {target}'.format(target = dest)
            ], cwd = self.defdir)
        self.stage_run([
            'scp', '-F', self.ssh_config, '-r',
            os.path.normpath(os.path.join(self.updir, path)),
            self.ssh_host + ':' +  dest,
        ]).check()
        return self

    def download(self, path = '.'):
        """Download from the sandbox container to the sandbox staging area."""
        assert self.box_exists()
        dest = os.path.normpath(os.path.join(self.downdir, path))
        if os.path.exists(dest):
            if os.path.isdir(dest):
                shutil.rmtree(dest)
            else:
                os.remove(dest)
                pass
            pass
        self.stage_run([
            'scp', '-F', self.ssh_config, '-r',
            self.ssh_host + ':' + os.path.normpath(os.path.join(self.boxcwd, path)),
            dest
        ], cwd = self.defdir).check()
        return self

    def boxed_run(self, cmd = None, **opts):
        """Run a command in the sandbox container."""
        command = 'cd {boxcwd} && {command}'.format(
            boxcwd = self.boxcwd,
            command = (' '.join(shlex.quote(w) for w in cmd)
                       if type(cmd) == list else cmd)
        )
        return self.stage_run(['vagrant', 'ssh', VAGRANT_TARGET, '-c', command],
                              logcmd = cmd, cwd = self.defdir,
                              **opts)

    pass

class SharedVagrantSandboxProvider(SandboxProvider):
    
    def __init__(self, defdir = './environment',
                 onstart = None, onstop = None, oncreate = None,
                 **opts):
        super().__init__(**opts)
        self.defdir = defdir
        self.ssh_config = os.path.join(self.defdir, SSH_CONFIG)
        self.onstart = onstart
        self.onstop = onstop
        self.oncreate = oncreate
        self.started = False
        pass

    def start(self):
        if subprocess.run(['vagrant', 'ssh', VAGRANT_TARGET, '-c', 'echo'],
                          cwd = self.defdir).returncode != 0:
            self.started = True
            if os.path.exists(os.path.join(self.defdir, '.vagrant')):
                if self.onstart:
                    self.onstart(self)
                    pass
            else:
                if self.oncreate:
                    self.oncreate(self)
                    pass
                pass
            pass
        subprocess.run(
            ['vagrant', 'up', VAGRANT_TARGET],
            cwd = self.defdir,
            check = True
        )
        conf = subprocess.run(
            ['vagrant', 'ssh-config', VAGRANT_TARGET],
            cwd = self.defdir,
            capture_output = True,
            check = True
        ).stdout.decode()
        with open(self.ssh_config, 'w') as f:
            f.write(conf)
            pass
        return self

    def stop(self):
        if self.started:
            if self.onstop:
                self.onstop(self)
                pass
            if os.path.exists(self.ssh_config):
                os.remove(self.ssh_config)
                pass
            self.ssh_host = None
            subprocess.run(
                ['vagrant', 'halt', VAGRANT_TARGET],
                cwd = self.defdir,
                check = True
            )
            pass
        return self

    def sandbox(self, boxid, stagedir = None, boxcwd = None, env = None,
                logfile = None, boxopts = None, timeout = None):
        return SharedVagrantSandbox(
            defdir = self.defdir,
            boxid = boxid,
            stagedir = stagedir or os.path.join(self.stagebase, boxid),
            boxcwd = boxcwd or (os.path.join(self.boxcwd, self.boxid)
                                if self.boxcwd else None),
            env = env if env != None else self.env,
            logfile = logfile or self.logfile,
            boxopts = boxopts or self.boxopts,
            timeout = timeout or self.timeout
        )

    pass
