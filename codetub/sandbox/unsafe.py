import os
import re
import shlex
import shutil
import subprocess

from codetub.sandbox.base import *

class SharedLocalSandbox(Sandbox):
    def __init__(self, none, **opts):
        super().__init__(None, **opts)
        if not self.boxcwd:
            self.boxcwd = self.workdir
            pass
        pass

    def box_exists(self):
        return self.stage_exists()

    def box_create(self):
        self.stage_create()
        return self

    def upload(self, path = '.'):
        for path in os.listdir(self.updir):
            relcopy(self.updir, self.boxcwd, os.path.join(self.updir, path),
                    overwrite = True)
            pass
        return self

    def download(self, path = '.'):
        for path in os.listdir(self.boxcwd):
            relcopy(self.boxcwd, self.downdir, os.path.join(self.boxcwd, path),
                    overwrite = True)
            pass
        return self

    def boxed_run(self, cmd = None, **opts):
        return self.stage_run(cmd = cmd, **opts)

    pass

class SharedLocalSandboxProvider(SandboxProvider):
    def sandbox(self, boxid, stagedir = None, boxcwd = None, env = None,
                logfile = None, timeout = None):
        return SharedLocalSandbox(
            None,
            boxid = boxid,
            stagedir = stagedir or os.path.join(self.stagebase, boxid),
            boxcwd = boxcwd or self.boxcwd,
            env = env if env != None else self.env,
            logfile = logfile or self.logfile,
            timeout = timeout or self.timeout
        )
    pass
    
class SharedRemoteSandbox(Sandbox):
    class State:
        # Remote box does not exist.
        NONEXISTENT = 0

        # Remote box exists, but physical contents are not yet up to
        # date with the latest logical upload action. Physical upload
        # is necessary before a box command can run.
        DEFERRED = 1

        # Remote box exists and physical contents are up to date with
        # the latest logical upload action. Box commands can run
        # without further action.
        UPLOADED = 2

        # Remote box exists and physical contents are at least up to
        # date and possibly changed since the latest logical upload
        # action.
        DIRTY = 3
        pass

    def __init__(self, ssh_host = None, **opts):
        super().__init__(ssh_host, **opts)
        if not self.boxcwd:
            self.boxcwd = os.path.join(SharedRemoteSandboxProvider.PROVIDER_DIR_TEMPLATE.format(ssh_host), self.boxid)
            pass
        self.ssh_host = ssh_host
        self.updir_snapshot = os.path.join(self.stagedir, '.deferred_upload_snapshot')

        # Default state
        self.remote_box_logical_state = self.State.NONEXISTENT
        self.remote_box_physically_exists = False
        self.downloaded = True
        pass

    def set_state(self, state):
        self.remote_box_logical_state = state['logical_state']
        self.remote_box_physically_exists = state['physically_exists']
        self.downloaded = state['downloaded']
        return self

    def get_state(self):
        return {
            'logical_state': self.remote_box_logical_state,
            'physically_exists': self.remote_box_physically_exists,
            'downloaded': self.downloaded,
        }

    def box_exists(self):
        return self.remote_box_logical_state != self.State.NONEXISTENT
        # return self.stage_run([
        #     'ssh', self.ssh_host,
        #     'test -e {}'.format(self.boxcwd),
        # ])

    def box_create(self):
        if self.remote_box_logical_state == self.State.NONEXISTENT:
            self.remote_box_logical_state = self.State.DEFERRED
            os.makedirs(self.updir_snapshot, exist_ok = True)
            pass
        return self
        # self.stage_run([
        #     # Create but do not start a container.
        #     'ssh', self.ssh_host,
        #     'umask {:04o} && mkdir -p {}'.format(STAGE_UMASK, self.boxcwd),
        # ]).check()
        # return self

    def box_delete(self):
        if self.remote_box_physically_exists:
            self.stage_run([
                'ssh', self. ssh_host, 'rm -rf "{}"'.format(self.boxcwd)
            ]).check()
            self.remote_box_logical_state = self.State.NONEXISTENT
            self.remote_box_physically_exists = False
            pass
        return super().box_delete()

    def rsync(self, src_base, dest_base, path, mode = None):
        src = os.path.normpath(os.path.join(src_base, path) if path else src_base)
        dest = os.path.normpath(os.path.join(dest_base, path) if path else dest_base)
        if mode == 'local':
            local_dir = src
            pass
        elif mode == 'upload':
            local_dir = src
            dest = "{}:{}".format(self.ssh_host, dest)
            pass
        elif mode == 'download':
            local_dir = dest
            src = "{}:{}".format(self.ssh_host, src)
            pass
        else:
            assert False
            pass
        if os.path.isdir(local_dir):
            # Trailing slashes so rsync replaces (instead of nesting)
            # destination directory.
            src += '/'
            dest += '/'
            pass
        return self.stage_run([
            'rsync', '-aqz', '--delete', src, dest,
        ]).check()
        

    def upload(self, path = None):
        """Upload from the sandbox staging area to the sandbox container."""
        assert self.box_exists()
        
        # Update the upload snapshot.
        self.defer_upload(path = path)

        # Logical box state is now ahead of download state.
        self.downloaded = False

        # NB: The snapshot is flat, not a full history of per-path
        # deferred uploads. Full uploads can always be deferred, but
        # path-specific uploads can be deferred only if a full upload
        # is already deferred, effectively updating the deferred full
        # upload snapshot with the new per-path upload.

        if self.remote_box_logical_state == self.State.DEFERRED:
            # Was deferred, still deferred.
            pass
        elif self.remote_box_logical_state == self.State.UPLOADED:
            # Was uploaded, now deferred.
            self.remote_box_logical_state = self.State.DEFERRED
            pass
        elif self.remote_box_logical_state == self.State.DIRTY:
            if path:
                # A per-path upload must be forced immediately.
                # We do not model which deferred per-path uploads
                # would overwrite dirty state.
                self.force_upload(path = path)
                # Was dirty, still dirty.
                pass
            else:
                # A full upload can be deferred. It logically wipes
                # out all dirty/downloaded state.
                # Was dirty, now deferred.
                self.remote_box_logical_state = self.State.DEFERRED
                pass
            pass
        else:
            assert False
            pass
        pass
    def defer_upload(self, path = None):
        # rsync updir to snapshot
        self.rsync(self.updir, self.updir_snapshot, path, mode = 'local')
        return self
    def force_upload(self, path = None):
        self.remote_box_physically_exists = True
        self.rsync(self.updir_snapshot, self.boxcwd, path, mode = 'upload')
        return self

    def download(self, path = None):
        """Download from the sandbox container to the sandbox staging area."""
        assert self.box_exists()
        
        if not self.downloaded:
            # Logical box state is ahead of physically downloaded state.
            if self.remote_box_logical_state in [self.State.DEFERRED, self.State.UPLOADED]:
                # Upload snapshot matches logical box state. Copy locally.
                self.rsync(self.updir_snapshot, self.downdir, path, mode = 'local')
                pass
            elif self.remote_box_logical_state == self.State.DIRTY:
                # Physical box state is ahead of upload and download snapshots.
                # Download remote box state.
                self.rsync(self.boxcwd, self.downdir, path, mode = 'download')
                pass
            else:
                assert False
                pass

            # NB: downloading a specific path does not mean the entire
            # download is up to date.
            if not path:
                # Physical download state is up to date with logical box state.
                self.downloaded = True
                pass
            pass
        return self

    def boxed_run(self, cmd = None, **opts):
        """Run a command in the sandbox container."""

        if self.remote_box_logical_state == self.State.DEFERRED:
            # Force deferred full upload.
            self.force_upload()
            pass
        
        # Running a command in the box may update physical box state
        # to be newer than the logical upload and download snapshots.
        self.remote_box_logical_state = self.State.DIRTY
        self.downloaded = False

        assert self.remote_box_physically_exists

        command = 'umask {mask:04o} && cd "{boxcwd}" && {command}'.format(
            mask = STAGE_UMASK,
            boxcwd = self.boxcwd,
            command = (' '.join(shlex.quote(w) for w in cmd)
                       if type(cmd) == list else cmd)
        )
        return self.stage_run(['ssh', self.ssh_host, command],
                              logcmd = opts.get('logcmd', cmd),
                              **{ k: v for k, v in opts.items() if k != 'logcmd'})

    pass

class SharedRemoteSandboxProvider(SandboxProvider):
    PROVIDER_DIR_TEMPLATE = '/tmp/codetub-remotesandbox-{}'
    def __init__(self, ssh_host = None, **opts):
        super().__init__(**opts)
        self.ssh_host = ssh_host
        self.provider_dir = self.PROVIDER_DIR_TEMPLATE.format(ssh_host)
        pass

    def start(self):
        subprocess.run([
            'ssh', self. ssh_host, 'umask {:04o} && mkdir -p "{}"'.format(STAGE_UMASK, self.provider_dir),
        ], check = True)
        return super().start()

    def stop(self):
        subprocess.run([
            'ssh', self. ssh_host, 'rm -rf "{}"'.format(self.provider_dir),
        ], check = True)
        return super().stop()
    
    def sandbox(self, boxid, stagedir = None, boxcwd = None, env = None,
                logfile = None, boxopts = None, timeout = None):
        return SharedRemoteSandbox(
            ssh_host = self.ssh_host,
            boxid = boxid,
            stagedir = stagedir or os.path.join(self.stagebase, boxid),
            boxcwd = boxcwd or (os.path.join(self.boxcwd, self.boxid)
                                if self.boxcwd else None),
            env = env if env != None else self.env,
            logfile = logfile or self.logfile,
            boxopts = boxopts or self.boxopts,
            timeout = timeout or self.timeout
        )
    pass
    

