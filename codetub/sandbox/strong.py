import os
import re
import shlex
import shutil
import subprocess

import codetub
from codetub.sandbox.base import *

class DockerSandbox(Sandbox):
    
    def __init__(self, image = None, **opts):
        super().__init__(image, **opts)
        self.image = image
        self.containerid = None
        self.live = False
        pass

    def isolated(self):
        return True

    def box_exists(self):
        return 0 < len(stage_run([
            'docker', 'ps', '-a', '-q', self.image
        ]).check().decode().split())

    def box_create(self):
        """Create a container named boxid from the given image using the
        given docker command using staging directory stagedir with
        internal working directory boxcwd.

        """
        assert self.containerid == None
        assert not self.live
        self.containerid = self.stage_run([
            # Create but do not start a container.
            'docker', 'create', '--name', self.boxid,
            # Use an init process (pid 1) for normal behavior (reaping, signal).
            '--init',
            # Disable networking to prevent exfiltration.
            '--network', 'none',
            # Leave stdin open.
            '--interactive',
        ] + self.boxopts + [
            # Use the course image with cat as the top-level command
            # in the container.  With stdin open, cat blocks until EOF
            # (which we will never send), keeping the container
            # running.  This is a (fairly standard)? hack, since there
            # appears to be no way to have the (added) init process
            # run without exiting to keep the container live.
            self.image, 'cat'
        ]).check().stdout.decode().strip()
        return self

    def box_delete(self):
        assert self.containerid
        assert not self.live
        self.stage_run(['docker', 'rm', self.boxid]).check()
        self.containerid = None
        return super().box_delete()

    def start(self):
        """Start the sandbox container."""
        assert self.containerid
        assert not self.live
        self.stage_run([
            'docker', 'start', self.boxid
        ]).check().stdout.decode().strip()
        self.live = True
        return self

    def stop(self):
        """Stop the sandbox container."""
        assert self.containerid
        assert self.live
        self.stage_run(['docker', 'stop', self.boxid]).check()
        self.live = False
        return self

    def upload(self, path = '.'):
        """Upload from the sandbox staging area to the sandbox container."""
        # assert self.containerid
        self.stage_run([
            'docker', 'cp', os.path.join(self.updir, path),
            self.boxid + ':' +  os.path.join(self.boxcwd, path),
        ]).check()
        return self

    def download(self, path = '.'):
        """Download from the sandbox container to the sandbox staging area."""
        # assert self.containerid
        self.stage_run([
            'docker', 'cp', self.boxid + ':' +  os.path.join(self.boxcwd, path),
            os.path.join(self.downdir, path),
        ]).check()
        return self

    def boxed_run(self, cmd = None, **opts):
        """Run a command in the sandbox container."""
        command = shlex.split(cmd) if type(cmd) == str else cmd
        logcmd = opts.get('logcmd')
        if logcmd:
            del opts['logcmd']
            pass
        if type(logcmd) == str:
            logcmd = shlex.split(logcmd)
            pass
        return self.stage_run(
            ['docker', 'exec', '--interactive', self.boxid] + command,
            logcmd = logcmd or command,
            **opts
        )

    pass


class DockerMachine(object):
    
    def __init__(self, machine = 'sandbox-provider',
                 enabled = True, show = False,
                 onstart = None, oncreate = None, onstop = None):
        self.machine = machine
        self.enabled = enabled
        self.call = subprocess.check_call if show else subprocess.check_output

        self.onstart = onstart
        self.oncreate = oncreate
        self.onstop = onstop

        pass

    def machine_exists(self):
        return self.machine in subprocess.check_output([
            'docker-machine', 'ls', '-q'
        ]).decode().split()

    def machine_up(self):
        return self.machine_exists() and 'Running' == subprocess.check_output([
            'docker-machine', 'status', self.machine
        ]).decode().strip()

    def configure_environment(self, env = os.environ):
        for line in subprocess.check_output(
                ['docker-machine', 'env', self.machine]
        ).decode().split('\n'):
            match = re.match(r'^export (\w[\w\d_]*)=(.*)$', line.strip())
            if match:
                env[match.group(1)] = shlex.split(match.group(2))[0]
                pass
            pass
        pass

    def start(self):
        if self.enabled:
            # Bring up a docker machine if needed.
            if not self.machine_up():
                if self.machine_exists():
                    if self.onstart:
                        self.onstart(self)
                        pass
                    self.call(['docker-machine', 'start', self.machine])
                else:
                    if self.oncreate:
                        self.oncreate(self)
                        pass
                    self.call([
                        'docker-machine', 'create', '--driver', 'virtualbox',
                        self.machine
                    ])
                    pass
                assert self.machine_up()
                pass
            # Load docker machine environment.
            self.configure_environment()
            pass
        return self

    def stop(self):
        if self.enabled:
            if self.machine_up():
                if self.onstop:
                    self.onstop(self)
                    pass
                self.call(['docker-machine', 'stop', self.machine])
                assert not self.machine_up()
                pass
            self.env = None
            pass
        return self

    pass

class AutoDockerMachine(DockerMachine):
    
    def __init__(self, autostart = False, autostop = False, **opts):
        super().__init__(
            enabled = subprocess.call(
                ['docker', 'info'],
                stdout = subprocess.DEVNULL,
                stderr = subprocess.DEVNULL
            ) != 0,
            **opts
        )
        self.autostop = autostop
        if autostart:
            self.start()
            pass
        pass
    
    def __del__(self):
        if getattr(self, 'autostop', None):
            self.stop()
            pass
        pass

    pass

class DockerSandboxProvider(SandboxProvider):
    
    def __init__(self, machine = None, image = 'sandbox', imagedir =
                 './environment', boxcwd = '/sandbox',
                 onstart = None, onstop = None, oncreate = None, onbuild = None,
                 **opts):
        super().__init__(boxcwd = boxcwd, **opts)
        self.imagename = image
        self.imagedir = os.path.realpath(imagedir)
        self.onbuild = onbuild
        # Automatically create a docker machine if needed; leave it up
        # even after tub exits (autostop = False).
        self.machine = machine or AutoDockerMachine(
            autostart = False, autostop = False,
            machine = codetub.NAME + '-sandbox-provider',
            onstart = onstart,
            onstop = onstop,
            oncreate = oncreate,
        )
        pass

    def lookup(self):
        matches = subprocess.check_output(
            ['docker', 'images', '-q', self.imagename],
            env = self.env
        ).decode().strip().split()
        assert len(matches) <= 1
        return matches[0] if 0 < len(matches) else None

    def build(self):
        self.imageid = self.lookup()
        if not self.imageid:
            if self.onbuild:
                self.onbuild(self)
                pass
            assert self.env
            subprocess.check_call(
                ['docker', 'build', '-t', self.imagename, self.imagedir],
                env = self.env
            )
            self.imageid = self.lookup()
            pass
        assert self.imageid
        return self

    def start(self):
        self.machine.start()
        self.build()
        return self

    def stop(self):
        self.machine.stop()
        return self

    def sandbox(self, boxid, stagedir = None, boxcwd = None, env = None,
                logfile = None, boxopts = None, timeout = None):
        return DockerSandbox(
            env = env if env != None else self.env,
            boxid = boxid,
            stagedir = stagedir or os.path.join(self.stagebase, boxid),
            image = self.imagename,
            boxcwd = boxcwd or self.boxcwd,
            logfile = logfile or self.logfile,
            boxopts = boxopts or self.boxopts,
            timeout = timeout or self.timeout
        )
    pass


