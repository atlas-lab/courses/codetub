import json
import os
import shlex
import shutil
import subprocess

class FailedProcess(Exception):
    """Represents a failed process and its termination status."""
    def __init__(self, status):
        super().__init__(' '.join(shlex.quote(w) for w in status.command))
        self.status = status
        pass
    
class TerminatedProcess(object):
    """Representation of process termination status."""

    def __init__(self, status, command, retcode, stdout, stderr, timeout = None):
        self.status = status
        self.command = command
        self.retcode = retcode
        self.stdout  = stdout
        self.stderr  = stderr
        self.timeout = timeout
        pass

    def succeeded(self):
        """Did the process terminate in success?"""
        return self.retcode == 0

    def expired(self):
        """Did the timeout expire?"""
        return self.timeout != None

    def __bool__(self):
        """Terminated processes are True in success and False in failure/timeout."""
        return self.succeeded()

    def show(self):
        if self.stdout:
            print(self.stdout.decode(errors = "replace"))
            pass
        if self.stderr:
            print(None if self.stderr == None else self.stderr.decode(errors = "replace"))
            pass
        return self

    def check(self):
        """Raise an exception now if the process did not exit in success."""
        if not self:
            self.show()
            raise FailedProcess(self)
            pass
        return self

    pass

STAGE_UPDIR = 'upload'
STAGE_WORKDIR = 'work'
STAGE_DOWNDIR = 'download'
STAGE_METADIR = 'meta'
STAGE_META_STATE_FILE = 'state.json'
STAGE_MODE = 0o0700
STAGE_UMASK = 0o0077
STAGE_LOGFILE = 'sandbox.out'

class Sandbox(object):
    """A Sandbox is a place to run some external, third-party, or untrusted
    code that is under evaluation (e.g., for grading) or for other
    testing or containment purposes.

    Every sandbox has a staging area and a sandboxed execution context.

    The staging area is a directory tree in the local filesystem,
    including:

        - an upload directory (.updir) for preparing files to upload
          to the sandbox execution context with the upload method.

        - a download directory (.downdir) for retrieving files from
          the sandbox execution context with the download method.

        - a working directory (.workdir) used as the working directory
          during interactions with the sandbox execution context.

    Preparation and external analysis processes can be run in a
    sandbox's staging area via the sandbox's stage_run method.

    The sandboxed execution context supports running processes at some
    level of isolation with the boxed_run method:

        - Unsafe Isolation: Sandbox processes run on a shared system
          with standard process isolation and possibly a restricted
          environment, but no other restrictions, keeping all other
          privileges of the real/effective user, including full
          filesystem access under the user's permissions and full
          network access. Sandbox context may likewise be (made)
          accessible to other processes and users on the same system.

        - Weak Isolation: Sandbox processes run on a system or
          container dedicated to sandboxes and separate from the
          controller system, but are isolated from other sandboxes
          only via process isolation and possibly a restricted
          environment, keeping all other privileges including full
          filesystem access to other sandbox contexts under the user's
          permissions and full network access. Sandbox context may
          likewise be (made) accessible to other sandboxes on the same
          system.

        - Strong Isolation: Sandbox processes run on a system or
          container dedicated to that sandbox alone, sharing
          filesystem and other privileges only within the sandbox and
          running without network access. Containers for separate
          sandboxes may still be co-scheduled to share resources (but
          not permissions) on a single system.

    The sandbox abstraction is originally designed for use with
    strongly isolated containers for testing untrusted code. To
    support compatibility testing of provided external code across
    multiple computing environments in a course that supports 2
    similar phsyical environments, 1 virtual environment, and a
    sandbox testing environment, we have placed other weaker types of
    sandboxes under the same interface.

    There are currently 4 concrete Sandbox types:

      - A LocalSandbox/Provider provides unsafe sandboxes that simply
        run sandbox processes directly on the local system using the
        staging area's working directory as the process working
        directory.

      - A RemoteSandbox/Provider provides unsafe sandboxes that all
        run their processes as a single shared user on a remote host
        via SSH using an initially user-private working directory in
        /tmp.

      - A VagrantSandbox/Provider provides weak sandboxes that all run
        processes as a single shared user on a dedicated virtual
        machine managed by Vagrant.

      - A DockerSandbox/Provider provides strong sandboxes that each
        run their process in a sandbox-specific container without
        network access.

    Use cases:

      - Use only strongly isolated sandboxes for executing untrusted
        code.

      - Use weakly or strongly isolated sandboxes for testing during
        development of trusted code that does sensitive operations
        that could hose the main system if they go wrong.

      - Use any (unsafe, weak, strong) sandbox for testing established
        or innocuous trusted code, e.g., for checking compatibility
        across multiple computing environments.

    """

    def __init__(self, arg, env = None, boxid = None, stagedir = None,
                 boxcwd = None, logfile = STAGE_LOGFILE, boxopts = [],
                 timeout = None):
        # single box-type-specific arg... hack for supporting simple
        # reconstruction across process boundaries.
        self.arg = arg
        # Environment for processes run in the staging area or the
        # execution context.
        self.env = env if env != None else os.environ
        
        # Identifier of this box.
        self.boxid = boxid

        # Staging area.
        self.stagedir = stagedir

        # Staging area parts.
        self.updir = os.path.join(stagedir, STAGE_UPDIR)
        self.downdir = os.path.join(stagedir, STAGE_DOWNDIR)
        self.workdir = os.path.join(stagedir, STAGE_WORKDIR)
        self.metadir = os.path.join(stagedir, STAGE_METADIR, self.__class__.__name__)
        self.dirs = [
            # self.stagedir, # excluded since it does not contain
                             # anything but subdirs
            self.updir,
            self.downdir,
            self.workdir,
            self.metadir,
        ]

        # Working directory within the box execution context.
        self.boxcwd = boxcwd
        self.boxopts = boxopts

        # Default log file.
        self.logfile = logfile

        # State passing across instances...
        self.statefile = os.path.join(self.metadir, STAGE_META_STATE_FILE)

        # Default timeout
        self.timeout = timeout

        pass


    def get_state(self):
        return None

    def set_state(self, state):
        return self
    
    def load_state(self):
        if os.path.exists(self.statefile):
            with open(self.statefile) as state:
                self.set_state(json.load(state))
                pass
            pass
        return self

    def dump_state(self):
        os.makedirs(self.metadir, exist_ok = True)
        with open(self.statefile, 'w') as state:
            json.dump(self.get_state(), state)
            pass
        return self

    def clear_state(self):
        if os.path.exists(self.statefile):
            os.remove(self.statefile)
            pass
        return self

    def isolated(self):
        """Is this sandbox implementation strongly isolated?"""
        return False

    def stage_exists(self):
        """Does the staging area exist?"""
        return any(os.path.exists(d) and 0 < len(os.listdir(d)) for d in self.dirs)

    def box_exists(self):
        """Does the execution context exist?"""
        return False

    def stage_create(self):
        """Create the staging area."""
        for d in self.dirs:
            os.makedirs(d, mode = STAGE_MODE, exist_ok = True)
            pass
        return self

    def box_create(self):
        """Create the execution context."""
        return self

    def create(self, box = True):
        """Create the staging area and (by default) execution context."""
        self.stage_create()
        if box:
            self.box_create()
        return self

    def stage_delete(self):
        """Delete the staging area."""
        if os.path.exists(self.stagedir):
            shutil.rmtree(self.stagedir)
            pass
        return self

    def box_delete(self):
        """Delete the execution context."""
        self.clear_state()
        return self

    def delete(self, stage = False, box = True):
        """Optionally delete the staging area (default: no) and the execution
        context (default: yes)."""
        if box:
            self.box_delete()
            pass
        if stage:
            self.stage_delete()
            pass
        return self

    def start(self):
        """Start the execution context."""
        return self

    def stop(self):
        """Stop the execution context."""
        return self

    def upload(self, path = '.'):
        """Upload from the staging area to the execution context."""
        return self

    def download(self, path = '.'):
        """Download from the execution context to the staging area."""
        return self

    def log(self, output = None, logfile = None, logmode = 'a'):
        """Log to a file in the staging area's working directory."""
        assert output != None
        with open(os.path.join(self.workdir, logfile or self.logfile), logmode) as f:
            f.write(output)
            pass
        pass

    def stage_run(self, cmd = None, logfile = None, logcomment = None,
                  logcmd = True, logmode = 'a', erase_boxcwd = True, **opts):
        """Run a command ON THE HOST (NOT in the sandbox execution context), using
        the staging area working directory, and return its termination status."""

        # Command must be a list or string.  Shell syntax NOT supported.
        assert type(cmd) == list or type(cmd) == str

        # The staging area working directory must exist.
        assert os.path.exists(self.workdir)

        # If command-logging is enabled, log the command.
        if logfile and logcmd:
            self.log(
                '{preface}\n$ {command}\n'.format(
                    preface = '\n' + logcomment if logcomment else '',
                    command = ' '.join((shlex.quote(w) if w not in ['<', '>'] else w)
                                       for w in (shlex.split(logcmd) if type(logcmd) == str
                                                 else logcmd if type(logcmd) != bool
                                                 else cmd))
                ),
                logfile = logfile,
                logmode = logmode
            )
            if logmode == 'w':
                # Having done an initial 'w'-mode log operation,
                # remaining operations should, nonetheless, be 'a'.
                logmode = 'a'
            pass
        
        try:
            # The actual working directory must exist.
            assert os.path.exists(opts.get('cwd', self.workdir))

            # Run the command with standard (but override-able) process
            # treatment.
            status = subprocess.run(
                cmd,
                stdout = opts.get('stdout', subprocess.PIPE),
                stderr = opts.get('stderr', subprocess.STDOUT),
                env = opts.get('env', self.env),
                cwd = opts.get('cwd', self.workdir),
                timeout = opts.get('timeout', self.timeout),
                **{k: v for k, v in opts.items()
                   if k not in ['stdout', 'stderr', 'env', 'cwd', 'timeout']}
            )
            # The process terminated on its own.
            result = TerminatedProcess(status, cmd, status.returncode,
                                       status.stdout, status.stderr)
        except subprocess.TimeoutExpired as e:
            # The process timed out.
            result = TerminatedProcess(e, cmd, None,
                                       e.stdout, e.stderr, timeout = e.timeout)
            pass

        # If logging is enabled, log the output from stdout and stderr.
        if logfile:
            if result.stdout:
                stdout = result.stdout.decode(errors = "replace")
                self.log(stdout.replace(self.boxcwd + '/.', '.').replace(self.boxcwd, '.') if erase_boxcwd else stdout,
                         logfile = logfile, logmode = logmode)
                pass
            if result.stderr:
                stderr = result.stderr.decode(errors = "replace")
                self.log(stderr.replace(self.boxcwd + '/.', '.').replace(self.boxcwd, '.') if erase_boxwd else stderr,
                         logfile = logfile, logmode = logmode)
                pass
            if result.expired():
                self.log('\n<< TIMEOUT after {timeout} seconds >>\n'.format(
                    timeout = result.timeout
                )
            )
            pass

        # Return the termination status.
        return result

    def boxed_run(self, cmd = None, **opts):
        """Run a process in the sandbox execution context and return its
        termination status."""
        return False
    pass

class SandboxProvider(object):
    def __init__(self, env = os.environ, stagebase = './sandboxes',
                 boxcwd = None, logfile = STAGE_LOGFILE, boxopts = [],
                 timeout = None):
        self.env = env
        self.stagebase = stagebase
        self.boxcwd = boxcwd
        self.logfile = logfile
        self.boxopts = boxopts
        self.timeout = timeout
        pass

    def start(self):
        return self

    def stop(self):
        return self
    
    def sandbox(self, boxid, stagedir = None, boxcwd = None, env = None,
                logfile = None, boxopts = None, timeout = None):
        return None
    
