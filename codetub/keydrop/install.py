# CodeTub Host Installer
# Copyright (c) 2017-2018, Benjamin P. Wood

import codetub
from codetub import admin
from codetub.cli import *
from codetub.keydrop import *
from codetub.util import *

import os
import pwd
import shutil

def install():
    parser = argparse.ArgumentParser(description = 'Install ACL/keys for a keydrop bot in the current user account.')
    parser.add_argument("--repouser", help = "repository host user",
                        default = 'git')
    parser.add_argument("--repohost", help = "repository host hostname",
                        default = 'localhost')
    parser.add_argument("--repoport", help = "repository host SSH port",
                        default = "22")
    parser.add_argument("--coderoot", help = "code root", required = True)
    parser.add_argument("--cc", help = "path to cc executable")
    parser.add_argument("--git", help = "path to git executable")
    parser.add_argument("--python", help = "path to python3 executable")

    opts = parser.parse_args()
    run = make_run(CHATTY)
    git = opts.git or 'git'
        
    # Unless explicitly changed, make private files.
    old_mask = os.umask(UMASK_U_RWX)
    
    # Make top-level directories.
    makedirs()
    
    # Keyscan the host and persist to known_hosts.
    with open(codetub.homepath('.ssh', 'known_hosts'),
              'a') as known_hosts:
        known_hosts.write(
            run(['ssh-keyscan', '-p', opts.repoport, opts.repohost],
                output = True)
        )
        pass

    # Clone the ADMIN_DIR
    run([git, 'clone', codetub.repourl(admin.REPO,
                                         user = opts.repouser,
                                         host = opts.repohost,
                                         port = opts.repoport,),
         codetub.homepath(ADMIN_DIR)])
    
    # Create user keydir if not present in the ACL working copy.
    os.makedirs(os.path.join(ADMIN_DIR, admin.USER_PUBKEYS), exist_ok=True)

    # Set git identity.
    run([git, '-C', codetub.homepath(ADMIN_DIR),
         'config', 'core.sshCommand',
         'ssh -i {key} -o IdentitiesOnly=yes -p {port}'.format(
             key = codetub.homepath(PRIVKEY), port = opts.repoport
         )])
    run([git, '-C', codetub.homepath(ADMIN_DIR),
         'config', 'user.name', codetub.NAME + ' keydrop bot'])
    run([git, '-C', codetub.homepath(ADMIN_DIR),
         'config', 'user.email',
         '{user}@{host}'.format(user = pwd.getpwuid(os.getuid()).pw_name,
                               host = opts.repohost)])

    # Build C wrapper.
    run([
        'make', '-C', os.path.join(opts.coderoot, KEYDROP_CODE_DIR),
        'WRAPPER=' + codetub.homepath(DROP_BOT_SETUID), 'install'
    ] + [
        '{var}={val}'.format(var = var, val = val)
        for var, val in [
                ('CC', opts.cc),
                ('GIT', opts.git),
                ('PYTHON', opts.python),
        ] if val
    ])
    
    pass

if __name__ == '__main__':
    install()
