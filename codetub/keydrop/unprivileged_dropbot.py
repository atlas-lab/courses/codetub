# Unprivileged dropbot: drop key from stdin to file, run
# dropbot_setuid, remove file.

import os
import pwd
import subprocess
import sys

# Arguments
drop_dir = sys.argv[1]
dropbot_setuid = sys.argv[2]
keyname = sys.argv[3]

raw_pubkey = input() + '\n'

keypath = os.path.join(drop_dir, keyname)
os.umask(0o0022)
with open(keypath, 'w') as pk:
    pk.write(raw_pubkey)
    pass

try:
    subprocess.check_output([dropbot_setuid],
                            stderr = subprocess.STDOUT)
except subprocess.CalledProcessError as e:
    print(e.output.decode(errors = "replace"))
    raise e
finally:
    if os.path.exists(keypath):
        os.remove(keypath)
