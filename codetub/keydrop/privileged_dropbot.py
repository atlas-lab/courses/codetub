# A WRAPPER EXECUTES THIS SCRIPT UNDER SETUID/SETGID PERMISSIONS AS
# THE OWNING USER/GROUP.

# argv: 0 <script> 1 <pythonpath> 2 <gitpath>

if __name__ == '__main__':
    import sys
    sys.path.insert(0, sys.argv[1])
    pass

from codetub import admin
from codetub import keydrop
from codetub.util import UMASK_U_RWX
import glob
import os
import pwd
import shutil
import subprocess
import sys

def import_user_keys(gitpath):
    # Require an empty environment.
    assert len(os.environ) == 0

    # Effective uid (as codetub user)
    drop_uid = os.geteuid()

    # Drop user.
    drop_username = pwd.getpwuid(drop_uid).pw_name
    drop_homedir = pwd.getpwuid(drop_uid).pw_dir

    # Real uid of client.
    client_uid = os.getuid()
    if client_uid == drop_uid:
        print('setuid wrapper seems to have failed:', os.getresuid(), os.getresgid())
        pass
    assert(client_uid != drop_uid)
    
    # Real username of client.
    client_username = pwd.getpwuid(client_uid).pw_name

    def be_codetub():
        os.setresuid(drop_uid, drop_uid, client_uid)
        pass
    def be_client():
        os.setresuid(client_uid, drop_uid, drop_uid)
        pass
        
    def _import_user_keys():

        # FIXME: early: log/email all attempts for auditing.
        # could drop a perm to ensure log not accessible after.
        
        # Run all operations under restrictive umask.
        old_umask = os.umask(UMASK_U_RWX)
        
        # Git command with explicit paths and ssh identity.
        git = [gitpath, '-C', os.path.join(drop_homedir, keydrop.ADMIN_DIR)]

        be_codetub()
        subprocess.check_call(git + ['pull', '--quiet', '--commit', '--no-edit'])
        be_client()

        imported = []
        denied = []
        for pubkey in glob.glob(os.path.join(
                os.path.join(drop_homedir, keydrop.DROP_DIR),
                admin.pubkey_name(owner = client_username, sha = '*')
        )):
            with open(pubkey) as pkfd:
                key_uid = os.fstat(pkfd.fileno()).st_uid
                pass
            # FIXME, also require membership in a safelist that
            # explicitly does NOT include faculty or admins, only
            # students?  I guess this could be auto-checked on
            # tempest (SPECIFIC, NOT GENERAL) by home dir location
            # or membership in 'faculty' group.
            if key_uid == client_uid:
                # Approve this well-formed key submission owned by
                # the executing client user.
                aclkeypath = os.path.join(
                    os.path.join(drop_homedir, keydrop.ADMIN_DIR),
                    admin.USER_PUBKEYS,
                    admin.unprivileged_name(
                        admin.pubkey_canonical_name(owner = client_username,
                                                    pubkey = pubkey)
                    )
                )
                be_codetub() # need permission to read the repo keys.
                if (not os.path.exists(aclkeypath)
                    or subprocess.call(['/usr/bin/diff',
                                        pubkey, aclkeypath]) != 0):
                    shutil.copy(pubkey, aclkeypath)
                    subprocess.check_call(git + ['add', aclkeypath])
                    imported.append((pubkey, os.path.basename(aclkeypath)))
                    pass
                be_client()
                os.remove(pubkey)
            else:
                # Deny and log this ill-formed key submission not
                # owned by the executing client user.
                print('WARNING: fraudulent key {key}.'.format(key = pubkey))
                denied.append((key_uid, os.path.basename(pubkey)))
                pass
            pass
        be_codetub()
        if imported or denied:
            # Commit the keydrop results, logging both successful
            # drops and fraudulent drop attempts.
            subprocess.check_call(git + [
                'commit', '-m',
                '{owner} keydrop: imported [{imported}]; denied [{denied}]'
                .format(
                    owner = client_username,
                    imported = ', '.join(
                        '{src} -> {dest}'.format(
                            src = src, dest = dest
                        ) for src, dest in imported
                    ),
                    denied = ', '.join(
                        '{key} by {user}'.format(
                            key = key, user = pwd.getpwuid(key_uid).pw_name
                        ) for key_uid, key in denied
                    )
                )
            ]) #, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
            push_failures = 0
            # Push to the repo host to activate.  Do not specify
            # push/pull URLs explicitly based on opts.repo____,
            # which are untrusted... Instead, use the configured
            # remotes that were recorded by an admin when
            # initially cloning this repo.  Leaking public keys
            # would not be a disaster, but just in case...
            while subprocess.call(git + ['push']) != 0:
                push_failures += 1
                if 5 < push_failures:
                    # This would imply competition with an admin
                    # pushing from another working copy of the acl
                    # repo.
                    raise('Too many push failures.')
                subprocess.check_call(git + ['pull', '--commit', '--no-edit'])
                pass
            pass
        pass

    return keydrop.with_keydrop_lock(drop_username, _import_user_keys)

if __name__ == '__main__':
    import_user_keys(sys.argv[2])
    
