#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#define _GNU_SOURCE
#include <unistd.h>

/**
 * Native wrapper to support setuid bit.  (Most systems wisely do not
 * respect setuid on #! interpreted executables.)
 *
 * setuid security considerations:
 *
 * 1. This executable must NOT use any dynamic linking/loading.  It
 *    must be statically linked, to resist attacks via LD_PRELOAD,
 *    LD_LIBRARY_PATH, etc.
 *
 * 2. The environment must be scrubbed before invoking the python
 *    script, to ensure that no roundabout environment attacks can
 *    influence what python, the script, or subprocesses will do.
 *
 * Options for scrubbing the environment:
 *
 * 1. env -i starts from an empty environment, BUT on some systems,
 *    it also drops setuid privileges.  That will not work.
 * 2. python options:
 *      -E ignores environment vars that affect python. (e.g. PYTHONPATH)
 *      -s ignores user site directory.
 *      -S ignores global site directory.
 *      -I is isolated mode = -E -s plus more?
 *    This seems like a good idea, but it lets various other
 *    environment variables pass through.  More is needed.
 * 3. Null out the global `environ` variable in C before execing:
 *    https://www.dwheeler.com/secure-programs/Secure-Programs-HOWTO/environment-variables.html
 * 
 * The latter two combined should be good.
 */
int main(int argc, char** argv) {
  // Clear the environment.
  clearenv();

  // Sanity check: fail here if setuid bit was not respected.
  uid_t r, e, s;
  getresuid(&r, &e, &s);
  assert(r != e && "setuid wrapper seems to have failed");

  // In the empty environment, exec python with minimal external influence.
  // Macros here are absolute paths determined at compile time.
  return execl(PYTHON,
               PYTHON, "-B", "-I", "-S", KEYDROP_PY, PYTHONPATH, GIT,
               NULL);
}
