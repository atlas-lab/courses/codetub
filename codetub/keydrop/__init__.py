# Key Drop directory structures.

import codetub
from codetub import admin
from codetub.util import with_file_lock
import os
import pwd

# Key for dropbot access to admin repo.
PRIVKEY = os.path.join('.ssh', 'id_rsa')
PUBKEY  = admin.pubkey(PRIVKEY)

# Name of dropbot key in the admin repo.
ACL_KEYNAME = admin.system_name('keydrop')

# Private items.
PRIV_DIR = 'private'
KEYDROP_LOCK  = os.path.join(PRIV_DIR, 'keydrop-lock')
ADMIN_DIR = os.path.join(PRIV_DIR, admin.REPO)

# Public items.
PUB_DIR = 'public'
BIN_DIR = os.path.join(PUB_DIR, 'bin')
DROP_DIR = os.path.join(PUB_DIR, 'keydrop')
DROP_BOT_SETUID = os.path.join(BIN_DIR, 'dropbot_setuid')

# Keydrop executables, relative to CodeTub installation.
KEYDROP_CODE_DIR = 'codetub/keydrop'
DROP_BOT = os.path.join(KEYDROP_CODE_DIR, 'unprivileged_dropbot.py')

# Directories and permissions.
DIRS = [
    (PRIV_DIR, 0o0700),
    (PUB_DIR,  0o0755),
    (BIN_DIR, 0o0755),
    (DROP_DIR, 0o1777),
]

# Create directories.
def makedirs():
    for directory, mode in DIRS:
        os.makedirs(directory, exist_ok = True)
        os.chmod(directory, mode)
        pass
    pass

# Run thunk while holding the keydrop lock.
def with_keydrop_lock(user, thunk):
    klock = os.path.join(pwd.getpwnam(user).pw_dir, KEYDROP_LOCK)
    with_file_lock(klock, thunk)

