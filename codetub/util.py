import fcntl
from functools import reduce
import glob
import json
import os
import shutil
import subprocess
import sys
import time

# assert (3, 5) <= sys.version_info

UMASK_U_RWX = 0o0077
UMASK_UG_RWX = 0o0007
MODE_U_RWX = 0o0700
MODE_U_RW = 0o0600

# Verbosity levels.
MODEST = 0
PROGRESS = 1
CHATTY = 2

def make_run(verbose = MODEST, pretend = False, **defaults):
    def run(cmd, output = False, interactive = False, fail = None, **kwargs):
        opts = dict(defaults)
        opts.update(kwargs)
        assert not (output and interactive)
        assert type(cmd) == list and all(type(x) == str for x in cmd)
        if pretend or verbose >= CHATTY:
            print(subprocess.list2cmdline(cmd), opts)
            pass
        if not pretend:
            if verbose <= PROGRESS:
                if not interactive and not output and not opts.get('stderr'):
                    # opts['stderr'] = subprocess.DEVNULL
                    opts['stderr'] = subprocess.STDOUT
                    pass
                if not interactive and not output and not opts.get('stdout'):
                    # opts['stdout'] = subprocess.DEVNULL
                    pass
                pass
            if 'comment' in opts:
                del opts['comment']
                pass
            try:
                if output and not interactive:
                    return subprocess.check_output(cmd, **opts).decode(errors = "replace")
                else:
                    assert not output
                    if 'stdout' in opts or interactive:
                        subprocess.check_call(cmd, **opts)
                    else:
                        subprocess.check_output(cmd, **opts)
                    return 0
            except subprocess.CalledProcessError as e:
                # print('ERROR running {} with options {}'.format(cmd, opts))
                if e.output:
                    print(e.output.decode(errors = "replace"))
                    pass
                if fail:
                    return fail()
                else:
                    raise e
        elif output:
            return ''
        else:
            return 0
    return run

def make_show(verbose = MODEST):
    def show(string, level = MODEST, alt = None):
        if verbose >= level:
            sys.stdout.write(string)
        elif alt:
            sys.stdout.write(alt)
            pass
        sys.stdout.flush()
        pass
    def showln(string = '', level = MODEST, alt = None):
        show(string, level, alt if alt else None)
        show('\n', level)
    return (show, showln)

def make_log(logfile, verbose = False, pretend = False):
    if not os.path.exists(directory):
        os.makedirs(directory)
    path = os.path.join(directory, logfile)
    
    ## Log something.
    def log_writeln(s = '', mode = 'a'):
        with open(path, mode) as f:
            f.write(s)
            f.write('\n')
            f.flush()
            pass
        if verbose >= CHATTY:
            print(str)
            pass

    ## Log and run a command.
    run = make_run(verbose, pretend)
    def log_run(command, interactive = False, **opts):
        with open(path, 'a') as f:
            ## Log the command to be run.
            f.write('\n')
            if opts.get('comment'):
                f.write("# %s\n" % opts['comment'])
                del opts['comment']
                pass
            s = "$ " + subprocess.list2cmdline(command)
            if opts.get('stdin'):
                s += " < %s" % os.path.basename(opts['stdin'])
                pass
            if opts.get('stdout'):
                s += " > %s" % os.path.basename(opts['stdout'])
                pass
            f.write("%s\n" % s)
            f.flush()
            if not interactive:
                # print('NOT interactive', command)
                ## Set up stdout, stderr
                if not opts.get('stdout') or opts['stdout'] == "-":
                    opts['stdout'] = f
                    pass
                if not opts.get('stderr') or opts['stderr'] == "-":
                    opts['stderr'] = f
                    pass
                pass
            else:
                # print('INTERACTIVE', command, opts)
                pass
            run(command, output=False, interactive=interactive, **opts)
            pass
        pass

    ## Log an error.
    errors = []
    def log_error(x=None):
        if x:
            errors.append(x)
    def log_errors():
        return errors

    ## Logger is:
    return (log_writeln, log_run, log_error, log_errors, path)

def make_timer(show, showln):
    def timer(message, thunk):
        show("{}... ".format(message))
        result, duration = timed(thunk)
        showln("done. [{:.1f}s]".format(duration))
        return result
    return timer

def timed(thunk):
    start = time.time()
    result = thunk()
    duration = time.time() - start
    return (result, duration)

def load_json(path):
    with open(path) as f:
        return json.load(f)
    
def store_json(blob, path):
    with open(path, 'w') as f:
        json.dump(blob, f, indent=4)
    
# Records
class Record(object):
    def __init__(self, **defaults):
        self.attrs = set()
        self._setattrs(defaults)
        pass
    def _setattrs(self, attrs):
        for k,v in attrs.items():
            setattr(self, k, v)
            self.attrs.add(k)
            pass
        pass
    def to_dict(self):
        return {k: getattr(self, k) for k in self.attrs}
    def to_json(self):
        return json.dumps(self.to_dict())
    def shadow(self, **attrs):
        fresh = self.__class__()
        fresh._setattrs(self.to_dict())
        fresh._setattrs(attrs)
        return fresh
    def update(self, **attrs):
        self._setattrs(attrs)
        return self
    def load(self, path):
        self._setattrs(load_json(path))
        return self
    def store(self, path):
        store_json(self.to_dict(), path)
        pass
    pass

# Configs as records
class Config(Record):
    def __init__(self, **defaults):
        super().__init__(**defaults)
        if 'configpath' in defaults and os.path.exists(defaults['configpath']):
            self.load(defaults['configpath'])
            pass
        pass
    def load(self, configpath):
        super().load(configpath)
        if not getattr(self, 'basepath', None):
            self.basepath = os.path.abspath(os.path.dirname(configpath) or '.')
            pass
        return self
    def store(self, configpath=None):
        super().store(configpath or self.configpath)
    pass

class IteratorHasNext:
    def __init__(self, it):
        self.it = it
        self.cached = None
        pass
    
    def has_next(self):
        if self.cached:
            status, item = self.cached
            return status
        else:
            try:
                self.cached = (True, self.it.next())
                return True
            except StopIteration:
                self.cached = (False, None)
                return False
            pass
        pass
    
    def next(self):
        if self.has_next():
            status, item = self.cached
            self.cached = None
            pass
        pass

    pass


## Expand a list of patterns given a base directory.
def expand(base, patterns, ignore = []):
    ignore_paths = [p for pat in ignore for p in glob.glob(src, pat)]
    def exp(acc, p):
        for path in glob.glob(p if p.startswith("/") else os.path.join(base, p)):
            if path not in acc and path not in ignore_paths:
                acc.append(path)
                pass
            pass
        return acc
    return reduce(exp, patterns, [])

def rm_r(src):
    if os.path.isdir(src):
        shutil.rmtree(src)
    else:
        os.remove(src)
        pass
    pass

def cp_r(src, dest):
    "Actually cp -a"
    if os.path.isdir(src):
        shutil.copytree(src, dest)
    else:
        shutil.copy2(src, dest)
        pass
    pass

def relcopy(src, dest, node, overwrite = False, underwrite = False):
    assert not (overwrite and underwrite)
    target = os.path.join(dest, os.path.relpath(node, start = src))
    if not target.startswith(dest):
        raise "Bad relcopy out of bounds"
    parent = os.path.dirname(target)
    if parent:
        os.makedirs(parent, exist_ok = True)
        pass
    # Overwrite if any of the following is true:
    # - explicit overwrite: overwrite=True
    # - default behavior:   overwrite=False and underwrite=False and source is newer
    if (os.path.exists(target)
        and (overwrite
             or (not underwrite
                 and os.path.getmtime(target) < os.path.getmtime(node)))):
        rm_r(target)
        pass
    if not os.path.exists(target):
        cp_r(node, target)
        pass
    pass
    
def with_file_lock(path, thunk):
    with open(path, 'w+') as lockfd:
        fcntl.flock(lockfd, fcntl.LOCK_EX)
        try:
            return thunk()
        finally:
            # Release the lock no matter what.
            fcntl.flock(lockfd, fcntl.LOCK_UN)
            pass
        pass
    pass

def findup(file, cwd = os.getcwd()):
    if file.startswith('/'):
        return file
    path = os.path.join(os.path.realpath(cwd), file)
    if os.path.exists(path):
        return path
    elif cwd == '/':
        return None
    else:
        return findup(file, os.path.dirname(cwd))
    pass

