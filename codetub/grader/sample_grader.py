#!/usr/bin/env python3

# Auto-generated sample grader.py script.

from codetub.grader.driver import main
from codetub.grader.gradetree import *

def auto(sandbox):
    """Perform automatic evaluation."""
    # Run untrusted user code in the sandbox. The sandbox is
    # prepopulated with the user code.

    # sandbox.boxed_run captures stdout/stderr as bytes in eponymous
    # attributes of the return value.  To log all output to a file,
    # use the logfile argument. GRADE_OUT is the standard output file
    # included in grade summaries by gradetree.
    ok = sandbox.boxed_run(['ls'], logfile = GRADE_OUT)

    # Return a grade, in this case predicated on the results of the
    # sandboxed execution.
    return make('Auto grade', points = (5 if ok else 0, 5))

def manual(sandbox):
    """Perform manual evaluation."""
    # sandbox.staged_run runs a command OUTSIDE the sandbox, in the
    # staging area working directory, perhaps useful to display some
    # file contents to the human grader.
    sandbox.stage_run(['ls', '-l'])
    # Return a grade.
    return make('Manual grade',
                # Prompt for notes, if any (ended by empty line).
                notes = prompt_notes(),
                # Prompt for point assignment, from the available
                # number of points.
                points = prompt_points(5))

if __name__ == '__main__':
    # Specify phases for grading.
    # A phase is a (well-named) function that takes a prepared Sandbox
    # argument and returns a gradetree grade structure.
    main([auto, manual],
         # Optionally list an file paths that should be included in
         # grading output.  By default, the standard output file,
         # GRADE_OUT, is included.
         rootlinks = [],
         # Optionally give any notes to appear in the top-level grade
         # summary header.
         rootnotes = [])
