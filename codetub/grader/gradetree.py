# Library for lightweight hierarchical grade records.
# Copyright (c) Benjamin P. Wood 2017-2018

import argparse
import json
import os
import sys
from functools import reduce

GRADE_DIR  = '.gradetree'
GRADE_JSON = os.path.join(GRADE_DIR, 'grade.json')
SUBGRADE_JSON_TEMPLATE = os.path.join(GRADE_DIR, 'grade.{}.json')
GRADE_OUT  = os.path.join(GRADE_DIR, 'grade.out')
GRADE_TEXT = 'grade.txt'

def path(name):
    return os.path.join(GRADE_DIR, name)
def relname(path):
    return path.replace(GRADE_DIR + '/', '')

def title(grade):
    """Return the title of the given grade item."""
    return grade['title']

def key(grade):
    """Return the key of the given grade item if present, else the title."""
    return grade.get('key', title(grade))

def notes(grade):
    """Return the list of notes on the given grade item."""
    return grade.get('notes', [])

def links(grade):
    """Return the list of links on the given grade item."""
    return grade.get('links', [])

def subgrades(grade):
    """Return the list of subgrades of the given grade item."""
    return grade.get('subgrades', [])

def points(grade):
    """Return the explicitly marked points of the given grade item."""
    return grade.get('points')

def total(grade):
    """Return the total points (awarded, possible) in the given grade item."""
    children = subgrades(grade)
    if children:
        return reduce(lambda acc, xy: (acc[0] + xy[0], acc[1] + xy[1]),
                      map(total, children), (0,0))
    else:
        pts = points(grade)
        if pts:
            return pts
        else:
            return (0, 0)

def valid(grade):
    """Return grade if the given grade item is well-formed, else None."""
    return grade if (
        ('title' in grade)
        # links : string list option
        and type(grade.get('links', [])) == list
        and all(type(l) == str for l in grade.get('links', []))
        # notes : string list option
        and type(grade.get('notes', [])) == list
        and all(type(n) == str for n in grade.get('notes', []))
        # points and subgrades are mutually exclusive
        and not ('points' in grade and 'subgrades' in grade)
        # points : (int * int) list option
        and ('points' not in grade or (
            type(grade['points']) in [tuple, list]
            and len(grade['points']) == 2
            and all(type(n) in [int, float] for n in grade['points'])
        ))
        # subgrades : grade list option
        and all(valid(p) for p in grade.get('subgrades', []))
        # key: str
        and type(grade.get('key', '') == str)
    ) else None
    
def make(title, links = [], notes = [], points = None, subgrades = [], key = None):
    """Return a new grade item composed of the given attributes."""
    return extend({}, title = title, links = links, notes = notes, points = points, subgrades = subgrades, key = key)
def extend(g, title = None, links = [], notes = [], points = None, subgrades = [], key = None):
    """Return a new grade item composed of the given attributes."""
    if title != None:
        g['title'] = title
    if links != None:
        g['links'] = links
    if notes:
        g['notes'] = notes
    if points:
        g['points'] = points
    if subgrades:
        g['subgrades'] = subgrades
    if key:
        g['key'] = key
    assert valid(g)
    return g

def prompt_rubric(title, levels, maxpoints = None, notespath = None):
    print(title + ':\n(Select an assessment bin.)')
    for k, v in levels.items():
        assert 'points' in v
        assert 'notes' in v
        assert type(v['notes']) == list and all(type(n) == str for n in v['notes'])
        print('  {} [{} points]: {}'.format(k, v['points'], '\n\t'.join(v['notes'])))
        pass
    choiceid = None
    while choiceid not in levels:
        choiceid = input('= ').strip()
        pass
    selected = levels[choiceid]
    return make(
        title,
        notes = selected['notes'] + ([] if not notespath
                                     else prompt_notes(cachepath = notespath)),
        points = (selected['points'],
                  maxpoints if maxpoints != None
                  else max(v['points'] for v in levels.values()))
    )

def prompt_points(available, bounds = True, default = None, prompt = None):
    assert 0 < available or not bounds
    while True:
        try:
            pts = float(input('{}Points{} __ / {}: '.format(
                prompt + ' ' if prompt else '',
                '' if not default else ' [{}]'.format(default),
                available if available != 0 else '0 (raw adjustment)'
            )))
            return (min(pts, available) if bounds else pts,
                    available)
        except:
            pass
        pass
    pass

def prompt_count(prompt, conv = int):
    while True:
        try:
            return conv(input(prompt))
        except:
            pass
        pass
    pass

def prompt_notes(cachepath = None, cache = None, prompt = None):
    cache = cache or []
    if cachepath:
        if os.path.exists(cachepath):
            with open(cachepath) as cf:
                cache = json.load(cf)
                pass
            pass
        pass
    notes = []
    print("--- NOTES {} {}".format("for" if prompt else "", prompt or ""))
    print("""Enter notes or:
    ?    to view note history
    #N   to insert note N from history
         (empty) to continue""")
    while True:
        note = input('{}Note: '.format(prompt + ' ' if prompt else '')).strip(' \t\n')
        if note == '':
            if cachepath:
                with open(cachepath, 'w') as cf:
                    json.dump(cache, cf)
                    pass
                pass
            return notes
        elif note == '?':
            print("Note cache:")
            for i,n in enumerate(cache):
                print("  #{:<3} {}".format(i, n))
                pass
            pass
            
        elif note.startswith('#'):
            try:
                n = cache[int(note[1:])]
                notes.append(n)
                print("  >> " + n)
            except:
                print("Invalid cache index")
                pass
            pass
        elif len(note) < 2:
            print("Ignoring suspiciously small note!")
            pass
        else:
            notes.append(note)
            if note not in cache:
                cache.append(note)
                pass
            pass
        pass
    pass

def prompt(title, points, notes = True):
    return make(title,
                notes = prompt_notes() if notes else None,
                points = prompt_points(points))
        
def prompt_adjustment():
    return make("Raw grade adjustment",
                notes = prompt_notes(prompt = "Raw Adjustment") if notes else None,
                points = prompt_points(0, bounds = False))

def load(jsonpath, orelse = lambda: None):
    """Return a grade item loaded from a JSON file at the given path
    or the result of calling the orelse thunk if the file does not exist."""
    if os.path.exists(jsonpath):
        with open(jsonpath) as f:
            g = json.load(f)
            pass
    else:
        g = orelse()
        pass
    assert valid(g)
    return g

def store(grade, jsonpath, overwrite = False):
    """Store the given grade item to a JSON file at the given path,
    overwriting an existing file only if the overwrite argument is true."""
    assert valid(grade)
    if not overwrite:
        # g = load(jsonpath)
        # assert all(k in grade for k in g)
        assert not os.path.exists(jsonpath)
        pass
    with open(jsonpath, 'w') as f:
        json.dump(grade, f, indent = 4)
        pass
    return grade

INDENT = '  '
"""Indent for children in text output."""

def text(grade, output = sys.stdout, root = '.'):
    """Format the given grade item and any files in its links
    as plain text, writing to the given output (default: stdout)."""
    def writeln(line=''):
        output.write('{}\n'.format(line))
        pass
    def writegrade(grade, depth = 1):
        award, possible = total(grade)
        writeln((INDENT * depth) + (
            title(grade) + ':' if 0 == possible and 0 == award
            else '{title}: {sign}{award}'.format(
                title = title(grade),
                sign = '+' if 0 < award else '',
                award = award
            ) if 0 == possible and 0 != award
            else '{title}: {award} / {possible}'.format(
                title = title(grade),
                award = award,
                possible = possible
            )
        ))
        for subnote in notes(grade):
            writeln(INDENT * (depth + 1) + subnote)
            pass
        sublinks = [relname(l)
                    for l in links(grade)
                    if os.path.exists(os.path.join(root, l))]
        if sublinks:
            writeln(INDENT * (depth + 1) + 'See more in section {} below.'.format(', '.join('"{}"'.format(l) for l in sublinks)))
            pass
        for subgrade in subgrades(grade):
            writegrade(subgrade, depth + 1)
            pass
        pass
    def writerule():
        writeln('+' + ('-' * 70) + '+')
        pass
    def writeframe(s):
        writeln('| {:<68} |'.format(s))
        pass
    def writebanner(lines):
        writerule()
        if type(lines) == list:
            for line in lines:
                writeframe(line)
                pass
            pass
        else:
            writeframe(lines)
            pass
        writerule()
        pass
    
    writebanner(['{title}'.format(title = title(grade))]
                + notes(grade))
    for p in subgrades(grade):
        writegrade(p)
        pass

    award, possible = total(grade)
    writebanner("TOTAL: {award:>3} / {possible:>3}".format(
        award = award,
        possible = possible
    ))

    def inline_linked_file(c):
        if os.path.exists(os.path.join(root, c)):
            writeln()
            writebanner(c.replace(GRADE_DIR + '/', ''))
            with open(os.path.join(root, c), 'rb') as cf:
                writeln(cf.read().decode(errors = "replace"))
                pass
            pass
        pass
    def write_linked_files(g, prev = None):
        if prev == None:
            prev = set()
        for c in links(g):
            if c not in prev:
                inline_linked_file(c)
                prev.add(c)
            pass
        for sg in subgrades(g):
            write_linked_files(sg, prev)
            pass
        pass
    write_linked_files(grade)
    pass

def report(grade, jsonpath = GRADE_JSON, textpath = GRADE_TEXT):
    store(grade, jsonpath, overwrite = True)
    with open(textpath, 'w') as textfile:
        text(grade, textfile)
        pass
    return grade
