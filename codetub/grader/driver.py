import argparse
import glob
import json
import sys

from codetub.grader.gradetree import *
import codetub.sandbox.strong as strong
import codetub.sandbox.weak as weak
import codetub.sandbox.unsafe as unsafe

SANDBOX_TYPES = {
    ty.__name__: ty for ty in [
        strong.DockerSandbox,
        weak.SharedVagrantSandbox,
        unsafe.SharedRemoteSandbox,
    ]
}

def seq(steps):
    def seq_tailrec(steps, acc):
        if 0 == len(steps):
            return acc
        else:
            thunk = steps[0]
            status = thunk()
            return seq_tailrec(steps[1:] if status else [], acc + [status])
        pass
    return seq_tailrec(steps, [])

NODEPS_PHASES = set()
def nodeps(f):
    NODEPS_PHASES.add(f)
    return f

NORUN_PHASES = set()
def norun(f):
    NODEPS_PHASES.add(f)
    NORUN_PHASES.add(f)
    return f

def record(roottitle, rootnotes, rootlinks, all_explicit_phases):
    all_phases = all_explicit_phases + [RAW_ADJUSTMENT_KEY]
    all_phase_files = [SUBGRADE_JSON_TEMPLATE.format(p) for p in all_phases]
    phase_files = [(p, f) for p, f in  zip(all_phases, all_phase_files) if os.path.exists(f)]
    return report(make(roottitle,
                       notes = rootnotes,
                       subgrades = [
                           extend(load(f), key = p) for p, f in phase_files
                       ] + [
                           # Legacy
                           load(f) for f in glob.glob(SUBGRADE_JSON_TEMPLATE.format('*'))
                           if f not in all_phase_files
                       ],
                       links = rootlinks))
    
def evaluate(sandbox, targets, deps, roottitle, rootnotes, rootlinks, all_explicit_phases,
             overwrite = False):
    # Create the grade directory.
    os.makedirs(GRADE_DIR, exist_ok = True)

    # Run each requested phase and store its results.
    for name, fun in targets:
        jsonfile = SUBGRADE_JSON_TEMPLATE.format(name)
        if overwrite or (
                fun not in NODEPS_PHASES
                and os.path.exists(jsonfile)
                and any(os.path.getmtime(jsonfile) < os.path.getmtime(os.path.join(sandbox.updir, f))
                        for f in deps if os.path.exists(f))
        ) or not os.path.exists(jsonfile):
            store(fun(sandbox), jsonfile, overwrite = True)
            pass
        pass

    # Create and store a root grade from all available phase results
    # (not just the targets run this time).
    return record(roottitle, rootnotes, rootlinks, all_explicit_phases)

# NB: cannot collide with any function name.
RAW_ADJUSTMENT_KEY = 'raw-adjustment'
def raw_adjust(sandbox):
    if os.path.exists(GRADE_TEXT):
        print("Existing evaluation: file://{}\n".format(os.path.abspath(GRADE_TEXT)))
        pass
    return prompt_adjustment()

def main(all_phase_funs, rootnotes = [], rootlinks = []):
    # Parse arguments.
    parser = argparse.ArgumentParser()

    parser.add_argument('phases', nargs = '*',
                        help = 'grading phases')
    parser.add_argument('--overwrite', action = 'store_true',
                        help = 'overwrite existing grade trees for the selected phases')

    # Other actions (besides running phases)
    action = parser.add_mutually_exclusive_group()
    action.add_argument('-d', '--dump-phases', dest = 'dump', action = 'store_true',
                        help = 'dump json of grader phases and exit without evaluation')
    action.add_argument('-r', '--report', dest = 'report', action = 'store_true',
                        help = 'generate reports and exit without new evaluation')
    action.add_argument('-e', '--erase', dest = 'erase', action = 'store_true',
                        help = 'erase json of given phases instead of evaluating')

    gradeargs = parser.add_argument_group('grade description arguments')
    # gradeargs.add_argument('-a', '--authors', help = 'author attribution string')
    gradeargs.add_argument('-t', '--title', help = 'title of graded work')

    sandboxargs = parser.add_argument_group('sandbox access arguments')
    sandboxargs.add_argument('--sandbox-boxtype', dest = 'boxtype',
                             help = 'sandbox type')
    sandboxargs.add_argument('--sandbox-boxarg', dest = 'boxarg',
                             help = 'provider-specific argument')
    sandboxargs.add_argument('--sandbox-boxid', dest = 'boxid',
                             help = 'identifier for sandbox')
    sandboxargs.add_argument('--sandbox-boxcwd', dest = 'boxcwd',
                             help = 'working directory in container')
    sandboxargs.add_argument('--sandbox-stagedir', dest = 'stagedir', default = '.',
                             help = 'sandbox io directory')
    sandboxargs.add_argument('--sandbox-timeout', dest = 'timeout', type = int,
                             help = 'sandbox task timeout')
    sandboxargs.add_argument('--dep', dest = 'deps', action = 'append', default = [],
                             help = 'existing gradefiles up-to-date only if newer than this file')
    
    opts = parser.parse_args()

    all_explicit_phases = [p.__name__ for p in all_phase_funs]
    if opts.dump:
        print(json.dumps(all_explicit_phases))
        return all_explicit_phases
    if opts.report:
        return report(opts.title, rootnotes, rootlinks, all_explicit_phases)

    # Map phase names to phase functions.
    phase_by_name = { p.__name__: p for p in all_phase_funs }
    # Built-in raw grade adjustment support.
    phase_by_name[RAW_ADJUSTMENT_KEY] = raw_adjust

    # If no phases specified, run all phases.
    phases = opts.phases or [p for p in phase_by_name.keys() if p != RAW_ADJUSTMENT_KEY]
    invalid = [name for name in phases
               if name not in phase_by_name]
    if 0 < len(invalid):
        parser.error("Invalid phase(s) {}. Choose from {}.".format(
            ', '.join(invalid),
            ', '.join(phase_by_name.keys())
        ))
        pass

    targets = [(name, phase_by_name[name])
               for name in phases]

    needs_sandbox = not all(p in NORUN_PHASES for n, p in targets)

    if opts.erase:
        for p in phases:
            phase_jsonpath = SUBGRADE_JSON_TEMPLATE.format(p)
            if os.path.exists(phase_jsonpath):
                os.remove(phase_jsonpath)
                pass
            pass
        record(opts.title, rootnotes, rootlinks)
        return

    # Evaluate the selected phases in the given sandbox with the given
    # grade attributes.
    sandbox = SANDBOX_TYPES[opts.boxtype](opts.boxarg,
                                          boxid = opts.boxid,
                                          stagedir = opts.stagedir,
                                          boxcwd = opts.boxcwd,
                                          timeout = opts.timeout)
    sandbox.load_state()
    try:
        return evaluate(sandbox,
                        targets,
                        opts.deps,
                        opts.title,
                        rootnotes,
                        rootlinks,
                        all_explicit_phases,
                        overwrite = opts.overwrite)
    finally:
        sandbox.dump_state()
        pass

