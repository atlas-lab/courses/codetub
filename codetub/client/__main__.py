# CodeTub client
# Copyright (c) 2017-2018, Benjamin P. Wood

import codetub
import codetub.client.tool

if __name__ == '__main__':
    codetub.client.tool.main(codetub.NAME)
