from codetub.client import *
import codetub.cli
from codetub.client.setup import *
from codetub.client.repos import *
from codetub.util import UMASK_U_RWX

TASKS = [
    # preserve 'setup' alias for compatibility with course init scripts
    (['auth', 'setup'], Auth, 
     'authenticate your account for CodeTub hosted repository access (once per account)'),
    (['id'], ID,
     'show your authenticated username for CodeTub hosted repository access'),
    (['list', 'repos'], List,
     'list the CodeTub hosted repositories you can access'),
    (['start', 'clone'], Start,
     'clone starter code to create a CodeTub hosted repository and a local working copy'),
    # (['survey'], Survey,
    #   'respond to any assignment survey questions'),
    (['sign', 'certify'], Sign,
      'sign your work and respond to assignment survey'),
]

DEFAULT = 'default'

def main(title, desc = None):
    def confid(opts, descriptor):
        if type(descriptor) == str and os.path.exists(descriptor):
            opts.config = load_json(descriptor)['confid']
        elif type(descriptor) == dict:
            opts.config = descriptor['confid']
            pass
        return opts.config
    codetub.cli.main(title, "{} client".format(title), TASKS,
                     config = DEFAULT,
                     load_config = lambda opts: load_config(confid(opts, desc)),
                     desc = desc,
                     umask = UMASK_U_RWX)
