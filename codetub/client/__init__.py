# CodeTub client
# Copyright (c) 2017-2018, Benjamin P. Wood
#
# Requirements:
# ssh-copy-id

import codetub
from codetub import admin
from codetub.util import *

import os

LOCAL_CONTEXT  = codetub.dotpath('client')
LOCAL_CONFIGS  = os.path.join(LOCAL_CONTEXT, 'configs')
LOCAL_KEYS = os.path.join(LOCAL_CONTEXT, 'keys')
LOCAL_PAIRINGS = os.path.join(LOCAL_CONTEXT, 'pairings.json')
LOCAL_CONFFILE = 'config.json'

LOCAL_SSH_KEYPATH_TEMPLATE = os.path.join(
    LOCAL_KEYS,
    codetub.NAME.lower() + '-client-{client}-{repouser}@{repohost}'
)

def local_privkey(conf):
    return LOCAL_SSH_KEYPATH_TEMPLATE.format(client = conf.client,
                                             repouser = conf.repouser,
                                             repohost = conf.repohost)

LOCAL_SSH_DIR = os.path.expanduser('~/.ssh')
LOCAL_SSH_CONFIG = os.path.join(LOCAL_SSH_DIR, 'config')
LOCAL_SSH_KNOWN_HOSTS = os.path.join(LOCAL_SSH_DIR, 'known_hosts')
LOCAL_SSH_CONFIG_TEMPLATE = """
# CodeTub ({{repouser}}@{{repohost}})
Match User {{repouser}} Host {{repohost}}
    IdentityFile {keyfile}
    IdentitiesOnly yes
    PasswordAuthentication no
    Port {{repoport}}
""".format(keyfile = LOCAL_SSH_KEYPATH_TEMPLATE)

CONFIG_VERSION = 0

def load_config(cid):
    config = os.path.join(LOCAL_CONFIGS, cid, LOCAL_CONFFILE)
    if os.path.exists(config):
        return Config().load(config)
    else:
        os.makedirs(os.path.dirname(config), mode = MODE_U_RWX, exist_ok = True)
        return None

TEAMLOG = '.team.json'

QUESTION_FILE = '.survey.json'
SURVEY_PROMPT_KEY = 'prompt'
SURVEY_TYPE_KEY = 'type'
ANSWER_PATH_TEMPLATE = '.{}.answers.json'
ANSWER_TYPES = [(str, 'a string'), (int, 'an integer'), (float, 'a decimal number')]
ANSWER_TYPE_BY_NAME = {ty.__name__: ty for ty, eng in ANSWER_TYPES}
ANSWER_TYPE_ENGLISH_BY_NAME = {ty.__name__: eng for ty, eng in ANSWER_TYPES}
SIGN_PATH_TEMPLATE = '.{}.sign.json'

def check_survey(survey):
    assert type(survey) == list
    assert all(SURVEY_PROMPT_KEY in q and type(q[SURVEY_PROMPT_KEY]) == str for q in survey)
    assert all(SURVEY_TYPE_KEY in q and q[SURVEY_TYPE_KEY] in ANSWER_TYPE_BY_NAME for q in survey)

               
