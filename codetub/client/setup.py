import codetub
from codetub import admin
from codetub.cli import *
from codetub.client import *
from codetub.util import (load_json, store_json, MODE_U_RWX, UMASK_U_RWX)

import os
import pwd
import shutil
import socket
import subprocess as sp

class Auth(Task):
    def __init__(self, parser):
        super().__init__(parser)
        defaults = parser.get_default('desc')
        course = parser.add_argument_group('course selection arguments')
        course.add_argument("-c", "--course", help = "course id",
                            default = defaults.get('course') if type(defaults) == dict else None)
        course.add_argument("-i", "--instance", help = "course instance",
                            required = True)
        identity = parser.add_argument_group('optional identity arguments')
        identity.add_argument("-n", "--name",
                              help = "use this human name without prompting later")
        identity.add_argument("-u", "--user",
                              help = "use this username without prompting later")
        advanced = parser.add_argument_group('optional advanced arguments')
        if not defaults:
            advanced.add_argument("-d", "--desc", metavar = "PATH",
                                  help = "configure access for the given course descriptor file")
            pass
        advanced.add_argument("--force", action = 'store_true',
                              help = "force replacement of existing configuration")
        pass
    def execute(self, oldconf, opts):
        old_umask = os.umask(UMASK_U_RWX)

        if not opts.desc:
            self.usage_error('the following arguments are required: -d/--desc')
            pass
        if oldconf and not opts.force:
            self.error('{} is already configured.'.format(opts.config))
            pass
        
        # Load initial config.
        conf = Config(
            basepath = os.path.join(LOCAL_CONFIGS, opts.config),
            configpath = os.path.join(LOCAL_CONFIGS, opts.config, LOCAL_CONFFILE)
        )
        if type(opts.desc) == str:
            conf = conf.load(opts.desc)
        elif type(opts.desc) == dict:
            conf = conf.shadow(**opts.desc)
        else:
            assert False
            pass
        the_course = opt.course if opts.course != None and opts.course != conf.course else conf.course
        the_instance = opt.instance if opts.instance != None and opts.instance != conf.instance else conf.instance

        conf = conf.shadow(
            basepath = os.path.join(LOCAL_CONFIGS, conf.confid),
            configpath = os.path.join(LOCAL_CONFIGS, conf.confid, LOCAL_CONFFILE),
            repobase = 'courses/{}/{}'.format(the_course, the_instance),
            course = the_course,
            instance = the_instance
        )

        # Determine user ID, with confirmation if it differs from current login.
        user = (
            opts.user
            or input(
                "Enter your {host} user ID: ".format(host = conf.repohost)
            ).strip()
        )
        login = pwd.getpwuid(os.getuid()).pw_name
        if user != login:
            self.showln('The username "{entry}" differs from your current login ("{current}").'.format(
                entry = user, current = login
            ))
            print('Please double check that you are logged into your own account.')
            if input(
                    'Is "{current}" your username on the computer your are using (yes/no)? '.format(
                        current = login
                    )
            ).strip().lower() not in ['y', 'yes']:
                self.error('Please log out and then log into your own account before proceeding.')
                pass
            if input('Is "{entry}" your {host} username (yes/no)? '.format(
                    entry = user, host = conf.repohost
            )).strip().lower() not in ['y', 'yes']:
                self.error('Please start again.')
                pass
            pass

        # Check and persist this repouser@repohost / clientuser
        # pairing.  The local one-client-identity-per-codetub
        # limitation can be overcome just as with the usual
        # ssh/repohost tricks: declare explicit host aliases in
        # ~/.ssh/config.
        pairings = (load_json(LOCAL_PAIRINGS) if os.path.exists(LOCAL_PAIRINGS)
                    else {})
        repouserhost = '{user}@{host}'.format(user = conf.repouser,
                                              host = conf.repohost)
        paired_user = pairings.get(repouserhost, None)
        if paired_user:
            if paired_user != user:
                self.error('This account is already configured for user {client} to access {host}.'.format(
                    client = paired_user, host = repouserhost
                ))
                pass
            pass
        else:
            pairings[repouserhost] = user
            os.makedirs(os.path.dirname(LOCAL_PAIRINGS), exist_ok = True)
            store_json(pairings, LOCAL_PAIRINGS)
            pass

        # Store username in config.
        conf = conf.shadow(client = user, user = user)

        # Set git identity.
        try:
            name = opts.name or str(self.run(['git', 'config',
                                              '--global', 'user.name'],
                                             output = True)).strip()
        except:
            name = ''
            pass
        if len(name) == 0 or len(name.split()) < 2:
            self.showln("Please enter the human name you prefer to use on your work.")
            name = str(input("Name: ")).strip()
            self.run(['git', 'config', '--global', 'user.name', name])
            pass
        try:
            email = str(self.run(['git', 'config', '--global', 'user.email'],
                                 output = True)).strip()
        except:
            email = ''
            pass
        if len(email) == 0:
            email = '{}@{}'.format(user, conf.drophost)
            self.run(['git', 'config', '--global', 'user.email', email])
            pass

        conf = conf.shadow(name = name, email = email)

        # Create a key for use from the local user context.
        self.showln('Generating your key. (This may take a moment.)')
        privkey = local_privkey(conf)
        if not os.path.exists(privkey):
            os.makedirs(os.path.dirname(privkey), exist_ok = True)
            os.makedirs(os.path.dirname(LOCAL_SSH_KNOWN_HOSTS), exist_ok = True, mode = MODE_U_RWX)
            try:
                # Scan the repohost keys and persist to known_hosts.
                with open(LOCAL_SSH_KNOWN_HOSTS, 'a') as known_hosts:
                    known_hosts.write(self.run(
                        ['ssh-keyscan', '-p', conf.repoport, conf.repohost],
                        output=True, stderr=sp.DEVNULL
                    ))
            except:
                self.error('{host} lookup failed.'.format(conf.repohost))
                pass

            self.run([
                'ssh-keygen', '-q', '-t', 'rsa', '-b', '4096',
                '-N', '', '-C', user, '-f', privkey
            ], output = True, fail = lambda: self.error('Key generation failed'))
            pass

        self.showln('Registering your key. (This may take a moment.)')
        self.showln('Please enter your password for {user}@{host} if prompted.'
                    .format(user = user, host = conf.repohost))
        
        # Copy the pubkey to the drop host for keydrop.
        pubkey = admin.pubkey(privkey)
        pubkeyname = admin.pubkey_canonical_name(owner = user,
                                                 pubkey = pubkey)
        with open(pubkey, 'rb') as pk:
            # Run keydrop on the repo host.
            self.run(
                [
                    'ssh', '-p', conf.dropport,
                    '-o', 'PubkeyAuthentication=no',
                    '-o', 'PasswordAuthentication=yes',
                    # login
                    '{user}@{host}'.format(user = user, host = conf.drophost),
                    # command
                    (conf.droppython or 'python3'), '-B', conf.dropbot,
                    # command args
                    conf.dropdir, conf.dropbot_setuid, pubkeyname
                ],
                stdin = pk,
                interactive = True,
                fail = lambda: self.error('Key registration failed.')
            )
            pass

        # Save the ssh config.
        with open(LOCAL_SSH_CONFIG, 'a') as sshconfig:
            sshconfig.write(LOCAL_SSH_CONFIG_TEMPLATE.format(
                repouser = conf.repouser,
                repohost = conf.repohost,
                repoport = conf.repoport,
                client = user
            ))
            pass

        # Save the codetub config.
        os.makedirs(os.path.dirname(conf.configpath), exist_ok = True)
        conf.store()
        
        self.showln('Setup is complete.')
        pass
    pass


class ID(Task):
    def execute(self, conf, opts):
        if not conf:
            self.error('{} is not configured. Did you run the auth step for your account?'.format(opts.config))
            pass
        print(conf.client)
