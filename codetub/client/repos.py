import codetub
from codetub import admin
from codetub.cli import Task
from codetub.client import *
from codetub.util import *

import datetime
import os
import pwd
import re

class List(Task):
    def __init__(self, parser):
        super().__init__(parser)
        filters = parser.add_argument_group('optional filter arguments')
        filters.add_argument("-p", "--project", default = [],
                             nargs='+', metavar = 'PROJ', dest = 'project',
                             help="list only repos for the given project ID(s)")
        filters.add_argument("-t", "--team", metavar='USER', default = [],
                             nargs='+',
                             help="list only repos shared with all given teammates")
    def execute(self, conf, opts):
        if not conf:
            self.error('{} is not configured'.format(opts.config))
            pass
        info = self.run([
            'ssh', '{user}@{host}'.format(user = conf.repouser,
                                          host = conf.repohost),
            'info'
        ], output = True).split('\n')

        # Show custom header.
        self.showln("{client}'s repositories hosted by {user}@{host}:".format(
            client = conf.client,
            user = conf.repouser,
            host = conf.repohost
        ))

        # Filter remaining lines.
        for line in info[1:]:
            if '' == line or (all([
                    '*' not in line,
                    not opts.project or any(line.endswith('/' + p)
                                            for p in opts.project),
                    not opts.team or all(u in x.split('/')
                                         for x in line.split('-')
                                         for u in opts.team)
            ]) and conf.repobase in line):
                self.showln(line)
                pass
            pass
        pass

class Start(Task):
    def __init__(self, parser):
        super().__init__(parser)
        clone = parser.add_argument_group('project selection arguments')
        clone.add_argument("project", metavar = 'PROJ',
                           help = "required ID of project to start")
        clone.add_argument("destination", nargs = '?', metavar = 'DEST',
                           help = "optional destination directory for local working copy [default: PROJ]")
        teamargs = parser.add_argument_group('optional team selection arguments')
        pairing = teamargs.add_mutually_exclusive_group()
        pairing.add_argument("-e", "--existing", action='store_true',
                             help="clone the existing repository for this project without prompting later")
        pairing.add_argument("-s", "--solo", action='store_true',
                             help="start a solo project without prompting later")
        pairing.add_argument("-t", "--team", nargs = '*', metavar = 'USER',
                             dest = 'team',
                             help="start a team project for the given teammate usernames without prompting later")
        pairing.add_argument("--no-check", action = "store_true",
                             dest = "nocheck",
                             help = "do not check team member usernames")
    def execute(self, conf, opts):
        if not conf:
            self.error("'{}' is not configured. Please run '{} auth' first.".format(
                opts.config, self.task_parser.prog.split()[0]
            ))
            pass
        dest = opts.destination or opts.project

        # Are we logged in as a user with the same ID as our ID on the drop/auth host?
        # This approximates: are we logged in to the repo host / shared system?
        login_is_id = pwd.getpwuid(os.getuid()).pw_name == conf.user
        # FIXME: needs a better check for whether user lookup is available.
        lookup_userids = not opts.nocheck and login_is_id

        info = self.run([
            'ssh', '{user}@{host}'.format(user = conf.repouser,
                                          host = conf.repohost),
            'info'
        ], output = True).split()

        if os.path.join(conf.repobase, admin.STARTERS, opts.project) not in info:
            self.error("Could not find a starter repo called '{}'. Check for typos.".format(opts.project))
            pass

        # Check for existing local repo.
        if os.path.exists(dest):
            self.error("'{}' already exists locally.".format(dest))
            pass

        # Check for existing hosted repo if solo/team not specified.
        if not opts.solo and not opts.team:
            submit_pattern = r'^.*' + os.path.join(conf.repobase, admin.SUBMISSIONS,
                                                   r'([\w\d\-_]+)', opts.project) + r'$'
            hosted_teams = [
                team for team in [
                    re.match(submit_pattern, line).group(1).split('-')
                    for line in self.run([
                            'ssh', '{user}@{host}'.format(user = conf.repouser,
                                                          host = conf.repohost),
                            'info'
                    ], output = True).split('\n')
                    if re.match(submit_pattern, line)
                ]
                if conf.client in team
            ]
            def with_fullname(userid):
                if lookup_userids:
                    try:
                        fullname = pwd.getpwnam(userid).pw_gecos or "<unlisted>"
                    except KeyError:
                        fullname = "<unknown>"
                        pass
                    return '{} ({})'.format(userid, fullname)
                else:
                    return userid
                pass
            if 1 == len(hosted_teams):
                self.showln("You already have a hosted '{project}' repository for {team}.".format(
                    project = opts.project,
                    team = (('team {' + ', '.join(map(with_fullname, hosted_teams[0])) + '}')
                            if 1 < len(hosted_teams[0])
                            else 'just ' + hosted_teams[0][0])
                ))
                if opts.existing or input('Use this repository (yes/no)? ').strip().lower() in ['y', 'yes']:
                    opts.team = hosted_teams[0]
                    pass
                pass
            elif 1 < len(hosted_teams):
                self.showln("You already have hosted '{}' repositories for multiple teams:".format(opts.project))
                for i, hosted_team in enumerate(hosted_teams):
                    self.showln('  [{}]  {}'.format(
                        i, ', '.join(map(with_fullname, hosted_team))
                    ))
                    pass
                self.showln('Enter the number of an existing team to use that repository, or')
                self.showln('leave the input blank to continue and create a new repository.')
                selection = input('Selection (0-{}): '.format(len(hosted_teams) - 1)).strip()
                try:
                    index = int(selection)
                    if index in range(len(hosted_teams)):
                        opts.team = hosted_teams[index]
                        pass
                    pass
                except:
                    self.showln('No team selected.')
                    pass
                pass
            elif opts.existing:
                self.showln("You do not currently have access to any {} repository.".format(
                    opts.project
                ))
                pass
            pass

        # Form the team based on command line options, or prompt otherwise.
        users = [conf.client]
        if not opts.solo:
            users.extend(opts.team or str(input(
                """Enter all teammate usernames except {}, separated by spaces.
Teammate usernames: """.format(conf.client)
            )).split())
            pass
        team = list(sorted(set(users)))

        # Unless solo, check team membership.
        if 1 < len(team):
            max_userid_len = max(map(len, team))
            unconfirmed_users = 0
            invalid_users = 0
            print("Proposed team:")
            for userid in team:
                if re.match(r'^\w+$', userid):
                    if lookup_userids:
                        # Look up the username.
                        try:
                            user = pwd.getpwnam(userid)
                            symbol = '+'
                            lookup = "= " + (user.pw_gecos or "<unlisted>")
                        except KeyError:
                            symbol = '?'
                            lookup = "= ERROR: unknown user"
                            unconfirmed_users += 1
                            pass
                        pass
                    else:
                        symbol = '+'
                        lookup = ""
                        pass
                    pass
                else:
                    symbol = '!'
                    lookup = "= ERROR: invalid username"
                    invalid_users += 1
                    pass
                print(("  {} {:" + str(max_userid_len) + "} {}").format(symbol, userid, lookup))
                pass
            if 0 < unconfirmed_users:
                print("ERROR: {} username{} unknown.".format(
                    unconfirmed_users,
                    " is" if unconfirmed_users == 1 else "s are"
                ))
                pass
            if 0 < invalid_users:
                print("ERROR: {} username{} invalid.".format(
                    invalid_users,
                    " is" if invalid_users == 1 else "s are"
                ))
                pass
            
            # Confirm that team is correct.
            if 0 < invalid_users or 0 < unconfirmed_users or input('Is this team correct (yes/no)? ').strip().lower() not in ['y', 'yes']:
                self.error("Please check the team member usernames and try this command again.")
                pass
            pass

        # Form team ID.
        teamid = '-'.join(team)
        self.showln("Preparing hosted '{project}' repository for team '{team}'.".format(
            project = opts.project, team = teamid, dest = dest
        ))
        
        # Starter and submission URLs.
        # FIXME define these centrally.
        starterurl = codetub.repourl(conf.repobase, admin.STARTERS, opts.project,
                                     user = conf.repouser,
                                     host = conf.repohost,
                                     port = conf.repoport)
        submiturl  = codetub.repourl(conf.repobase, admin.SUBMISSIONS,
                                     teamid, opts.project,
                                     user = conf.repouser,
                                     host = conf.repohost,
                                     port = conf.repoport)
        
        # Clone submission repo to create it on host.  It's empty.
        self.run(['git', 'clone', submiturl, dest])

        # Add starter repo as a remote.
        self.run(['git', '-C', dest,
                  'remote', 'add', 'starter', starterurl])

        # Check or create teah record.
        teamlog = os.path.join(dest, TEAMLOG)
        if not os.path.exists(teamlog):
            # Pull from starter repo.
            self.run(['git', '-C', dest, 'pull', 'starter', codetub.DEFAULT_BRANCH])

            # Record team init.
            store_json([{
                'event': 'start',
                'by': conf.client,
                'team': team,
                'utctimestamp': datetime.datetime.utcnow().isoformat(),
            }], teamlog)
            self.run(['git', '-C', dest, 'add', os.path.relpath(teamlog, dest)])
            self.run(['git', '-C', dest, 'commit', '-m',
                      '{} started team {}'.format(conf.client, teamid)])

            # Fix up branch names to use standard DEFAULT_BRANCH.
            self.run(['git', '-C', dest, 'branch', '-M', codetub.DEFAULT_BRANCH])
            # Push to the submission repo.
            self.run(['git', '-C', dest, 'push', '--set-upstream', 'origin', codetub.DEFAULT_BRANCH])
            # Make that the remote HEAD.
            self.run(['ssh', '-p', conf.repoport, '{}@{}'.format(conf.repouser, conf.repohost),
                      'symbolic-ref',
                      '/'.join([conf.repobase, admin.SUBMISSIONS, teamid, opts.project]),
                      'HEAD',
                      'refs/heads/{}'.format(codetub.DEFAULT_BRANCH)])

            # Grant permissions to teammates.
            # Do this after the push to avoid deadlock.
            if 1 < len(users):
                for u in users:
                    if u != conf.client:
                        self.run([
                            'ssh',
                            '{user}@{host}'.format(user = conf.repouser,
                                                   host = conf.repohost),
                            'perms', os.path.join(conf.repobase,
                                                  admin.SUBMISSIONS,
                                                  teamid, opts.project),
                            '+', 'WRITERS', u
                        ])
                        pass
                    pass
                pass
            pass
        
        # Show the submission URL.
        self.showln("Hosted repository URL: {}".format(submiturl))
        self.showln("Local working copy:    {}".format(dest))
        pass

class Submit(Task):
    # Honor code, time report as hooks.
    # tag?
    # Catchall commit, push.
    def __init__(self, parser):
        super().__init__(parser)
        parser.add_argument("cwd", metavar = 'PATH', default = '.',
                            help = "optional path to working copy [default: .]")
        pass
    def execute(self, conf, opts):
        status = self.run(['git', '-C', opts.cwd, 'status'], output = True)
        if "nothing to commit" not in status:
            self.showln("(!!)  This local repository contains changes that have not been committed.")
            self.showln("      Try to resolve this issue with git add, git commit, git push.")
            self.error("Your work is NOT fully submitted.")
            pass
        if "Your branch is up to date with 'origin/{}'".format(codetub.DEFAULT_BRANCH) not in status:
            self.showln("(!!)  This local repository contains commits that have not been pushed.")
            self.showln("      Try to resolve this issue with git push.")
            self.error("Your work is NOT fully submitted.")
            pass
        self.showln('(OK)  All tracked files in this local repository have been submitted.')
        self.showln('      Run the same check in all clones.')
        
        self.run(['git', 'pull'], output = True)
        # FIXME
        self.error("NOT IMPLEMENTED")
        pass

    pass

def ask(q, ty, default = None):
    parse = ANSWER_TYPE_BY_NAME[ty]
    while True:
        raw = input('\n' + q + (' ' if not default
                                else ' [default: {}] '.format(default)))
        if len(raw) == 0:
            return default
        try:
            return parse(raw)
        except:
            print('Please enter {} or leave blank to {}.'.format(
                ty,
                'accept the default' if default else 'skip'
            ))
            pass
        pass
    pass

class Survey(Task):
    def __init__(self, parser, lazy = False):
        super().__init__(parser)
        self.lazy = lazy
        
    def execute(self, conf, opts):
        gitdir = findup('.git')
        if not gitdir:
            self.error('This command must be run inside the local working copy of a repository.')
            pass
        root = os.path.dirname(gitdir)
        qfile = os.path.join(root, QUESTION_FILE)
        afile = os.path.join(root, ANSWER_PATH_TEMPLATE.format(conf.client))
        if os.path.exists(qfile):
            if not os.path.exists(afile) or not self.lazy:
                print('\nPlease answer the following questions.')
                questions = load_json(qfile)
                store_json({
                    'user': conf.client,
                    'answers': [
                        ask(q[SURVEY_PROMPT_KEY], q[SURVEY_TYPE_KEY], default = default)
                        for q, default in zip(questions,
                                              load_json(afile)['answers']
                                              if os.path.exists(afile)
                                              else (None for q in questions))
                    ]
                }, afile)
                if 'nothing to commit' not in self.run(['git', 'status', afile], output = True):
                    self.run(['git', 'add', afile], output = True)
                    self.run(['git', 'commit', '-m', 'responses by {}'.format(conf.client),
                              afile], output = True)
                    pass
                pass
            pass
            self.showln('''
Your responses have been committed to your local repository.
They will be submitted next time you git push.''')
        else:
            self.showln("There are no survey questions for this assignment.")
            pass
        pass
    pass

class Sign(Survey):
    def __init__(self, parser):
        super().__init__(parser, lazy = False)
        pass
    
    def execute(self, conf, opts):
        super().execute(conf, opts)
        gitdir = findup('.git')
        if not gitdir:
            self.error('This command must be run inside the local working copy of a repository.')
            pass
        root = os.path.dirname(gitdir)
        self.showln('''
Please type your name to certify that the work in this repository was
completed in accordance with the relevant policies for this assignment.
''')
        sfile = os.path.join(root, SIGN_PATH_TEMPLATE.format(conf.client))
        store_json({
            'user' : conf.client,
            'signature' : input('Your name: '),
            'utctimestamp' : datetime.datetime.utcnow().isoformat()
        }, sfile)
        if 'nothing to commit' not in self.run(['git', 'status', sfile], output = True):
            self.run(['git', 'add', sfile], output = True)
            self.run(['git', 'commit', '-m', 'signature by {}'.format(conf.client),
                      sfile], output = True)
            pass
        self.showln('''
Your signature (and any other responses) have been committed to a file
in your local repository.

TO SUBMIT YOUR WORK:
Please check that all relevant changes from your work are committed,
then git push to submit your signature (and any other local commits).
''')
        self.run(['git', 'status'])
        

class Leave(Task):
    # Split/leave a team.  Commit to teamlogs, do cloning, re-remoting.
    pass
class Join(Task):
    # Fuse into a team.  Commit to teamlogs, do re-remoting, merge?
    pass

    
