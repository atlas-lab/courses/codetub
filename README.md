# CodeTub: Submit That Assignment Code (incrementally)

## Overview

[CodeTub](https://gitlab.com/atlas-lab/courses/codetub) is a workflow
and toolbox to manage code coursework in version control repositories.

### Tools

The CodeTub toolbox provides three **human-facing CLI tools** for
three roles:

- *System administrator:* The `tubadmin` tool controls repository
  hosting (especially for courses) using [Gitolite](http://gitolite.com).
- *Course manager:* The `tub` tool distributes, collects, evaluates,
  and reports grades/feedback for student code in hosted version
  control repositories.
- *Student programmer:* The `tubclient` tool configures individual and
  team assignment repositories. Standard version control commands
  record and submit work in a simple [workflow introduced
  here](https://cs.wellesley.edu/~cs240/docs/common/git/).

These three tools are somewhat loosely coupled and may be used all
together or in subsets.

Behind the scenes, these tools interact with two host services
configured by `tubadmin`.

- The **repo host** uses [Gitolite](http://gitolite.com) to host
  repositories.
- The **keydrop bot** acts as an autonomous
  [Gitolite](http://gitolite.com) administrator for authorizing user
  SSH keys for use with [Gitolite](http://gitolite.com).

Both host services require dedicated user accounts. They may run on
the same or separate hosts. The keydrop bot works only on a host where
all `tubclient` users have individual shell accounts with SSH
access. The services are invoked on demand via SSH. Neither uses a
daemon.

### Subsystems

CodeTub includes several subsystems that are ready (or nearly ready)
for independent use in other contexts:

- Simple access control policies for [gitolite](http://gitolite.com)
  support [git](https://git-scm.com) access by faculty and students to
  starter repositories and individual or team submission repositories.
- The `codetub.keydrop` library implements a "bot" that allows users
  with shell accounts on a machine to upload public keys for
  authentication with a [gitolite](http://gitolite.com) host.
- The `codetub.cli` library provides common infrastructure for
  command-line tools that use config files and support multiple tasks.
- The `codetub.grader` library manages simple hierarchical
  grades/rubrics and provides an optional driver for grading scripts
  that use sandboxes.
- The `codetub.sandbox` library executes untrusted student code in
  sandboxes [docker](https://docs.docker.com/), providing an interface
  for interacting with the code that runs in the sandbox
  environment. It is also useful for testing code in other
  environments such as a course virtual appliance (manged by
  [Vagrant](https://www.vagrantup.com/)) or remote SSH hosts.

## Install

### Dependencies
   
All tools and hosts (`tubadmin`, `tubclient`, `tub`, repo host,
keydrop bot) require these #*base dependencies**:

- a POSIX system
  - all human-facing tools work on GNU/Linux and macOS
- [Python](https://python.org) >= 3.5
- [Git](https://git-scm.com) >= 2.0
- SSH

#### Host Dependencies

The repo host (managed remotely by `tubadmin`) requires:

- base dependencies
- [Gitolite](http://gitolite.com) >= 3.6.7
- dedicated user account

#### Keydrop Dependencies

The keydrop bot host (configured remotely by `tubadmin`) requires:

- base dependencies
- C compiler
- GNU `make`
- dedicated user account
- the keydrop bot may depend on GNU/Linux for `setuid` and `environ`
  behavior.  It has not been tested on macOS or other POSIX systems.

#### Manager Dependencies

The course manager (`tub`) requires:

- The `tqdm` pip package: `pip install tqdm`.
- base dependencies
- [docker](https://docs.docker.com/) engine >= 18.0, for running
  evaluation in strong sandboxes
- [docker-machine](https://docs.docker.com/machine/overview/) >= 0.15,
  if not running on a Linux host or not using the GUI Docker for Mac /
  Docker for Windows
- [vagrant](https://vagrantup.com) >= 2.0, for running the demo or
  testing in course appliances
- [VirtualBox](https://www.virtualbox.org/) >= 5.2, for running the
  demo or using docker-machine

Some listed version requirements may be conservatively high.

### Installation

For all three tools and roles:

0.  Install [dependencies](#dependencies).
    - Admins: Add `'symbolic-ref'` to the list of Gitolite commands to `ENABLE` in `.gitolite.rc`.
1.  `git clone ssh://git@gitlab.com/atlas-lab/courses/codetub`.
2.  Add `codetub/bin` to your `PATH`.

Administrators should see additional [documentation](#documentation) on
[installing and managing a CodeTub repository host and keydrop
bot](./docs/admin.org).

## Source Structure

- `bin`: command-line tool wrappers to go on `PATH`
- `c`: C code for keydrop bot setuid wrapper
- `codetub`: Python CodeTub implementation
- `demo`: automated demo (and integration test) of installation,
  setup, and workflow, using a virtual machine as CodeTub repository
  host, keydrop bot, and user host.
  - **Currently STALE**
- `docs`: documentation
- `LICENSE.txt`: source code and documentation license
- `README.org`: this file
- `TODO.org`: ideas, TODOs, issues

## Documentation

- For Students: [CodeTub Client Documentation](./docs/client.org)
- For Faculty: [CodeTub Manager Documentation](./docs/manager.org)
- For Admins: [CodeTub Admin and Host Documentation](./docs/admin.org)
- For Developers: [CodeTub Design and
  Implementation](./docs/internals/overview.org)

## License

Copyright (c) 2017 - 2018, Benjamin P. Wood, Wellesley College

Licensed under a BSD 3-Clause License ([LICENSE.txt](./LICENSE.txt)).

## History

[Ben Wood](https://cs.wellesley.edu/~bpw/) developed a Mercurial-based
predecessor to CodeTub using third-party hosting starting in 2014-2015
for [CS 240](https://cs.wellesley.edu/~cs240/) and [CS
251](https://cs.wellesley.edu/~cs251/fall15/) at Wellesley
College. Ben developed a Mercurial-based prototype of the current
CodeTub course manager for [CS 240 in Spring
2017](https://cs.wellesley.edu/~cs240/s17/) and the full locally
hosted, sandboxed, Git-based system for [CS 240 Fall
2018](https://cs.wellesley.edu/~cs240/f18/). Several semesters worth
of unsuspecting students new to version control have tested the
workflows. The author has no comment on code quality or usefulness to
others.

